#include <sev/lag/vector.hpp>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

// -- Instantiation
template class sev::lag::Vector< float, 1 >;
template class sev::lag::Vector< float, 2 >;
template class sev::lag::Vector< float, 3 >;
template class sev::lag::Vector< float, 4 >;

template class sev::lag::Vector< double, 1 >;
template class sev::lag::Vector< double, 2 >;
template class sev::lag::Vector< double, 3 >;
template class sev::lag::Vector< double, 4 >;

// -- Tests

template < std::size_t DIM >
void
test_features ()
{
  {
    std::ostringstream ostr;
    ostr << "--- Test features " << DIM << " ---\n";
    std::cout << ostr.str ();
  }

  using Vec = sev::lag::Vector< double, DIM >;
  if ( std::is_trivial< Vec >::value ) {
    throw std::runtime_error ( "is_trivial" );
  }
  if ( !std::is_trivially_copyable< Vec >::value ) {
    throw std::runtime_error ( "!is_trivially_copyable" );
  }
  if ( !std::is_trivially_destructible< Vec >::value ) {
    throw std::runtime_error ( "!is_trivially_destructible" );
  }
  if ( !std::is_standard_layout< Vec >::value ) {
    throw std::runtime_error ( "!is_standard_layout" );
  }
  if ( !std::is_copy_constructible< Vec >::value ) {
    throw std::runtime_error ( "!is_copy_constructible" );
  }
  if ( !std::is_move_constructible< Vec >::value ) {
    throw std::runtime_error ( "!is_move_constructible" );
  }
}

template < std::size_t DIM >
void
test_default_init ()
{
  {
    std::ostringstream ostr;
    ostr << "--- Test default init " << DIM << " ---\n";
    std::cout << ostr.str ();
  }

  using Vec = sev::lag::Vector< double, DIM >;
  Vec vec_default;
  if ( vec_default != Vec ( sev::lag::init::zero ) ) {
    std::ostringstream ostr;
    ostr << "vec_default " << vec_default << " is not zero initialized!";
    throw std::runtime_error ( ostr.str () );
  }
}

template < std::size_t DIM >
void
test_fill_init ()
{
  {
    std::ostringstream ostr;
    ostr << "--- Test fill init " << DIM << " ---\n";
    std::cout << ostr.str ();
  }

  using Vec = sev::lag::Vector< double, DIM >;
  Vec vec_zero ( sev::lag::init::zero );
  Vec vec_one ( sev::lag::init::one );
  Vec vec_fill_twelve ( sev::lag::init::Fill< double > ( 12.0 ) );

  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( vec_zero[ ii ] != 0.0 ) {
      std::ostringstream ostr;
      ostr << "vec_zero[ " << ii << "] should be " << 0.0 << " but is "
           << vec_zero[ ii ];
      throw std::runtime_error ( ostr.str () );
    };
  }

  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( vec_one[ ii ] != 1.0 ) {
      std::ostringstream ostr;
      ostr << "vec_one[ " << ii << "] should be " << 1.0 << " but is "
           << vec_one[ ii ];
      throw std::runtime_error ( ostr.str () );
    };
  }

  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( vec_fill_twelve[ ii ] != 12.0 ) {
      std::ostringstream ostr;
      ostr << "vec_fill_twelve[ " << ii << "] should be " << 12.0 << " but is "
           << vec_fill_twelve[ ii ];
      throw std::runtime_error ( ostr.str () );
    };
  }
}

template < std::size_t DIM >
void
test_array_init ()
{
  {
    std::ostringstream ostr;
    ostr << "--- Test array init " << DIM << " ---\n";
    std::cout << ostr.str ();
  }

  double array[ DIM ];
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    array[ ii ] = double ( ii );
  }

  using Vec = sev::lag::Vector< double, DIM >;
  Vec vec_array ( array );

  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( vec_array[ ii ] != array[ ii ] ) {
      std::ostringstream ostr;
      ostr << "vec_array[ " << ii << "] should be " << array[ ii ] << " but is "
           << vec_array[ ii ];
      throw std::runtime_error ( ostr.str () );
    };
  }
}

// -- Main

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  test_features< 1 > ();
  test_features< 2 > ();
  test_features< 3 > ();
  test_features< 4 > ();

  test_default_init< 1 > ();
  test_default_init< 2 > ();
  test_default_init< 3 > ();
  test_default_init< 4 > ();

  test_fill_init< 1 > ();
  test_fill_init< 2 > ();
  test_fill_init< 3 > ();
  test_fill_init< 4 > ();

  test_array_init< 1 > ();
  test_array_init< 2 > ();
  test_array_init< 3 > ();
  test_array_init< 4 > ();

  return 0;
}

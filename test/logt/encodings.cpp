
#include <sev/logt/context.hpp>
#include <sev/logt/server.hpp>
#include <sev/logt/sinks/ostream_file.hpp>
#include <sev/logt/sinks/ostream_stdout.hpp>
#include <iostream>

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  sev::logt::sinks::OStream_File std_sink_file;
  std_sink_file.open ( "encodings.txt", true );
  sev::logt::sinks::OStream_StdOut std_sink_out;

  sev::logt::Server manifold;
  manifold.sink_register ( &std_sink_file );
  manifold.sink_register ( &std_sink_out );

  sev::logt::Context log_context_a ( manifold, "A" );
  sev::logt::Context log_context_c ( log_context_a, std::string ( "C" ) );
  sev::logt::Context log_context_null ( manifold, nullptr );

  std::cout << "-- Begin\n";

  log_context_a.str ( "Context A" );
  log_context_c.str ( "Context C" );
  log_context_null.str ( "context null" );

  log_context_a.str ( "ASCII text" );
  log_context_a.str ( "ASCII text with two lines\nLine two" );
  log_context_a.str ( "ASCII text with two lines and two endl\nLine two\n" );
  log_context_a.str ( std::string ( "std::string" ) );
  log_context_a.str ( std::u16string ( u"std::u16string" ) );
  log_context_a.str ( std::u32string ( U"std::u32string" ) );

  log_context_a.str ( "Umlauts char     äöüÄÖÜÐ" );
  log_context_a.str ( u"Umlauts char16_t äöüÄÖÜÐ" );
  log_context_a.str ( U"Umlauts char32_t äöüÄÖÜÐ" );

  log_context_a.cat ( sev::logt::FL_INFO, "cat single " );
  log_context_a.cat ( sev::logt::FL_INFO, 1 );
  log_context_a.cat ( sev::logt::FL_INFO, -1 );
  log_context_a.cat ( sev::logt::FL_INFO, 1.1f );
  log_context_a.cat ( sev::logt::FL_INFO, 1.1 );

  log_context_a.cat ( sev::logt::FL_INFO,
                      "cat ",
                      " 1 ",
                      1,
                      " 1.1 ",
                      1.1f,
                      " 1.2 ",
                      1.2,
                      " -2 ",
                      -2,
                      std::string ( " string " ) );

  std::cout << "-- Done\n";

  return 0;
}

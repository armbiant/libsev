
#include <sev/assert.hpp>
#include <sev/mem/list/se/list.hpp>
#include <algorithm>
#include <array>
#include <cstdint>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <string>
#include <vector>

class Entry : public sev::mem::list::se::Item
{
  public:
  Entry ( std::size_t index_n )
  : index ( index_n )
  , some_text (
        "A long string that hopefully gets allocated in dynamic memory" )
  {
  }

  std::size_t index;
  std::string some_text;
};

void
test_integrity ( const std::list< Entry > & entries_n,
                 const sev::mem::list::se::List< Entry > & list_n,
                 std::size_t size_n )
{
  // std::cout << "    test_integrity: size: " << size_n << "\n";

  // Test emptyness
  if ( size_n == 0 ) {
    if ( !list_n.is_empty () ) {
      throw std::runtime_error ( "List should be empty" );
    }
  } else {
    if ( list_n.is_empty () ) {
      throw std::runtime_error ( "List should not be empty" );
    }
  }

  // Test front() back() equality
  if ( size_n != 0 ) {
    if ( size_n == 1 ) {
      if ( list_n.front () != list_n.back () ) {
        throw std::runtime_error ( "front() back() missmatch" );
      }
    } else {
      if ( list_n.front () == list_n.back () ) {
        throw std::runtime_error ( "front() back() match" );
      }
    }
  }

  // Test iteration
  {
    auto eit = entries_n.begin ();
    auto lit = list_n.begin ();
    std::size_t index = 0;
    for ( ; lit != list_n.end (); ++lit, ++eit, ++index ) {
      if ( index >= size_n ) {
        throw std::runtime_error ( "Iteration went too far." );
      }
      if ( &( *lit ) != &( *eit ) ) {
        throw std::runtime_error ( "Bad iterator reference." );
      }
    }
  }

  // Test std::distance
  {
    std::size_t iter_count = std::distance ( list_n.begin (), list_n.end () );
    if ( iter_count != size_n ) {
      throw std::runtime_error ( "Bad std::distance " +
                                 std::to_string ( iter_count ) + " should be " +
                                 std::to_string ( size_n ) );
    }
  }

  // Test count()
  {
    std::size_t list_count = list_n.count ();
    if ( list_count != size_n ) {
      throw std::runtime_error ( "Bad list size " +
                                 std::to_string ( list_count ) + " should be " +
                                 std::to_string ( size_n ) );
    }
  }
}

void
test_access ( const std::list< Entry > & entries_n,
              const sev::mem::list::se::List< Entry > & list_n )
{
  // std::cout << "    test_access: size: " << entries_n.size() << "\n";

  auto eit = entries_n.begin ();
  auto lit = list_n.begin ();
  std::size_t index = 0;
  for ( ; lit != list_n.end (); ++lit, ++eit, ++index ) {
    if ( lit->index != index ) {
      throw std::runtime_error ( "Bad index. Expected " +
                                 std::to_string ( index ) + " but got " +
                                 std::to_string ( lit->index ) );
    }
  }
}

void
test_fill ( std::list< Entry > & entries_n,
            sev::mem::list::se::List< Entry > & list_n,
            std::size_t size_n,
            bool push_front_n )
{
  // std::cout << "  test_fill: " << size_n << "\n";

  if ( !entries_n.empty () ) {
    throw std::runtime_error ( "Entries should be empty" );
  }
  if ( !list_n.is_empty () ) {
    throw std::runtime_error ( "List should be empty" );
  }

  for ( std::size_t ii = 0; ii != size_n; ++ii ) {
    if ( push_front_n ) {
      entries_n.emplace_back ( size_n - ii - 1 );
    } else {
      entries_n.emplace_back ( ii );
    }
  }

  for ( auto & entry : entries_n ) {
    if ( push_front_n ) {
      list_n.push_front ( &entry );
    } else {
      list_n.push_back ( &entry );
    }
  }

  if ( push_front_n ) {
    entries_n.reverse ();
  }

  test_integrity ( entries_n, list_n, size_n );
  test_access ( entries_n, list_n );
}

void
test_pop ( std::list< Entry > & entries_n,
           sev::mem::list::se::List< Entry > & list_n,
           std::size_t size_n )
{
  // std::cout << "  test_pop: " << size_n << "\n";

  if ( size_n == 0 ) {
    return;
  }

  entries_n.pop_front ();
  list_n.pop_front ();

  test_integrity ( entries_n, list_n, size_n - 1 );
}

void
test_extraction ()
{
  std::size_t num = 5;

  std::list< Entry > items;
  sev::mem::list::se::List< Entry > list;

  auto fillList = [ num, &items, &list ] () {
    list.clear ();
    items.clear ();

    for ( std::size_t ii = 0; ii != num; ++ii ) {
      items.emplace_back ( ii );
    }
    for ( auto & item : items ) {
      list.push_back ( &item );
    }
  };

  // Extract front items
  fillList ();
  for ( std::size_t ii = 0; ii != num; ++ii ) {
    Entry * entry = list.extract_if (
        [ ii ] ( const Entry & ent ) { return ent.index == ii; } );
    if ( entry == nullptr ) {
      throw std::runtime_error ( "Entry * should be valid" );
    }
    auto iit = items.begin ();
    std::advance ( iit, ii );
    if ( entry != &( *iit ) ) {
      throw std::runtime_error ( "Extract item missmatch" );
    }
  }
  if ( !list.is_empty () ) {
    throw std::runtime_error ( "Items should be empty" );
  }

  // Extract back items
  fillList ();
  for ( std::size_t ii = 0; ii != num; ++ii ) {
    std::size_t iir = num - 1 - ii;
    Entry * entry = list.extract_if (
        [ iir ] ( const Entry & ent ) { return ent.index == iir; } );
    if ( entry == nullptr ) {
      throw std::runtime_error ( "Entry * should be valid" );
    }
    auto iit = items.begin ();
    std::advance ( iit, iir );
    if ( entry != &( *iit ) ) {
      throw std::runtime_error ( "Extract item missmatch" );
    }
  }
  if ( !list.is_empty () ) {
    throw std::runtime_error ( "Items should be empty" );
  }

  // Extract second item
  fillList ();
  for ( std::size_t ii = 1; ii != num; ++ii ) {
    Entry * entry = list.extract_if (
        [ ii ] ( const Entry & ent ) { return ent.index == ii; } );
    if ( entry == nullptr ) {
      throw std::runtime_error ( "Entry * should be valid" );
    }
    auto iit = items.begin ();
    std::advance ( iit, ii );
    if ( entry != &( *iit ) ) {
      throw std::runtime_error ( "Extract item missmatch" );
    }
  }
  list.pop_front ();
  if ( !list.is_empty () ) {
    throw std::runtime_error ( "Items should be empty" );
  }
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  std::size_t repeats = 2;

  // Test push and pop
  {
    std::size_t fill_size_max = 8;

    std::list< Entry > entries;
    sev::mem::list::se::List< Entry > list;
    for ( std::size_t rr = 0; rr != repeats; ++rr ) {
      for ( std::size_t ii = 0; ii != fill_size_max; ++ii ) {
        std::cout << "test_size: " << ii << "\n";
        for ( bool push_front : { true, false } ) {
          test_fill ( entries, list, ii, push_front );
          for ( std::size_t jj = 0; jj != ii; ++jj ) {
            test_pop ( entries, list, ii - jj );
          }
        }
      }
    }
  }

  // Test splicing
  {
    std::size_t splice_size_max = 4;

    std::list< Entry > entries;
    sev::mem::list::se::List< Entry > list;

    for ( std::size_t rr = 0; rr != repeats; ++rr ) {
      for ( std::size_t ii = 0; ii != splice_size_max; ++ii ) {
        for ( bool push_front : { true, false } ) {
          for ( std::size_t app = 0; app != splice_size_max; ++app ) {
            std::cout << "test_splice: " << ii << " " << app << "\n";
            // Fill destination
            test_fill ( entries, list, ii, push_front );
            // Fill appendix
            std::list< Entry > entries_app;
            sev::mem::list::se::List< Entry > list_app;
            test_fill ( entries_app, list_app, app, push_front );

            // Splice
            entries.splice ( entries.end (), entries_app );
            list.splice_back ( list_app );

            // Test integrity
            test_integrity ( entries, list, ii + app );

            // Clear before repeating
            list.clear ();
            entries.clear ();
            // Test inegrity after clearing
            test_integrity ( entries, list, 0 );
          }
        }
      }
    }
  }

  // Test extraction
  {
    test_extraction ();
  }

  return 0;
}

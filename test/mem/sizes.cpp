
#include <cstdint>
#include <iostream>

struct Bool_1
{
  bool b0;
};

struct Bool_2
{
  bool b0;
  bool b1;
};

struct IBool_1
{
  int i0;
  bool b0;
};

struct IBool_2
{
  int i0;
  bool b0;
  bool b1;
};

struct PBool_1
{
  void * p0;
  bool b0;
};

struct PBool_2
{
  void * p0;
  bool b0;
  bool b1;
};

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  std::cout << "sizeof ( Bool_1 ): " << sizeof ( Bool_1 ) << "\n";
  std::cout << "sizeof ( Bool_2 ): " << sizeof ( Bool_2 ) << "\n";
  std::cout << "sizeof ( IBool_1 ): " << sizeof ( IBool_1 ) << "\n";
  std::cout << "sizeof ( IBool_2 ): " << sizeof ( IBool_2 ) << "\n";
  std::cout << "sizeof ( PBool_1 ): " << sizeof ( PBool_1 ) << "\n";
  std::cout << "sizeof ( PBool_2 ): " << sizeof ( PBool_2 ) << "\n";

  return 0;
}

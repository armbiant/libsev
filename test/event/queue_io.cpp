
#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/queue_io/queue.hpp>
#include <sev/string/utility.hpp>
#include <functional>
#include <future>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

using Flag_Function = std::function< void () >;
using Bit_Accu = sev::event::bit_accus_async::Callback;

static std::uint_fast32_t const flag_in = 1 << 0;
static std::uint_fast32_t const flag_out = 1 << 1;

template < std::size_t N >
struct Endpoint_N
{
  bool any_bits = false;
  Flag_Function bits_func;
  Bit_Accu bit_accu;
  sev::event::queue_io::Connection_N< N > con;
};

template < std::size_t N >
void
test_single_event ( std::array< Endpoint_N< N >, 2 > & endpoints_n,
                    std::size_t ep_a_index_n,
                    std::size_t channel_n )
{
  std::cout << sev::string::cat ( "test_single_event < ", N, " >()\n" );

  auto event = std::make_unique< sev::event::Event > ();
  // Push event into one end
  {
    auto & ep_a = endpoints_n[ ep_a_index_n ];
    // -- Push event
    ep_a.con.out ( channel_n ).push ( event.get () );

    // Test if any_bits flag was set
    if ( ep_a.any_bits ) {
      ep_a.any_bits = false;
    } else {
      throw std::runtime_error ( "After push() ep_a.any_bits should be set\n" );
    }

    // Test if the flag_out bit was set
    std::uint_fast32_t bits = ep_a.bit_accu.fetch_and_clear ();
    if ( bits != flag_out ) {
      throw std::runtime_error ( sev::string::cat (
          "After push() ep_a.bit_accu.fetch_bits() should be ",
          flag_out,
          " but is ",
          bits,
          "\n" ) );
    }

    // Feed connection
    ep_a.con.feed_queue ( bits, flag_in, flag_out );
    if ( bits != 0 ) {
      throw std::runtime_error ( sev::string::cat (
          "After feed_queue() (ep_a) bits should be 0 but is ", bits, "\n" ) );
    }

    // Test if any_bits flag was set
    if ( ep_a.any_bits ) {
      throw std::runtime_error (
          "After feed_queue () ep_a.any_bits should not be set\n" );
    }
    // Test if any bit was set
    bits = ep_a.bit_accu.fetch_and_clear ();
    if ( bits != 0 ) {
      throw std::runtime_error ( sev::string::cat (
          "After feed_queue () ep_a.bit_accu.fetch_bits() should be 0 but is ",
          bits,
          "\n" ) );
    }
  }

  // Read from the other end
  {
    std::size_t ep_b_index = ( ( ep_a_index_n + 1 ) % 2 );
    auto & ep_b = endpoints_n[ ep_b_index ];
    // Test if any_bits flag was set
    if ( ep_b.any_bits ) {
      ep_b.any_bits = false;
    } else {
      throw std::runtime_error ( "ep_b.any_bits should be set\n" );
    }
    // Test if any bit was set
    std::uint_fast32_t bits = ep_b.bit_accu.fetch_and_clear ();
    if ( bits != flag_in ) {
      throw std::runtime_error ( sev::string::cat (
          "After push() ep_b.bit_accu.fetch_bits() should be ",
          flag_in,
          " but is ",
          bits,
          "\n" ) );
    }

    // Feed connection
    ep_b.con.feed_queue ( bits, flag_in, flag_out );
    if ( bits != 0 ) {
      throw std::runtime_error ( sev::string::cat (
          "After feed_queue() (ep_b) bits should be 0 but is ", bits, "\n" ) );
    }
    // Test if the list is empty
    auto cin = ep_b.con.in ( channel_n );
    if ( !cin.available () ) {
      throw std::runtime_error (
          "ep_b.con.list_in ( channel_n ) should not be empty\n" );
    }
    cin.accept_one ();
  }
}

template < std::size_t N >
void
test_multiple_events ( std::array< Endpoint_N< N >, 2 > & endpoints_n,
                       std::size_t ep_a_index_n,
                       std::size_t channel_n )
{
  std::cout << sev::string::cat ( "test_multiple_events < ", N, " >()\n" );
  std::size_t ep_b_index = ( ( ep_a_index_n + 1 ) % 2 );

  auto & ep_a = endpoints_n[ ep_a_index_n ];
  auto & ep_b = endpoints_n[ ep_b_index ];

  auto event_a = std::make_unique< sev::event::Event > ();
  auto event_b = std::make_unique< sev::event::Event > ();

  ep_a.con.push ( channel_n, event_a.get () );
  ep_b.con.push ( channel_n, event_b.get () );
  std::uint_fast32_t bits_a = ep_a.bit_accu.fetch_and_clear ();
  if ( bits_a != flag_out ) {
    throw std::runtime_error (
        sev::string::cat ( "After push() ep_a.bit_accu.fetch_bits() should be ",
                           flag_out,
                           " but is ",
                           bits_a,
                           "\n" ) );
  }

  // Feed event_a into the queue
  ep_a.con.feed_queue ();

  // Test if any_bits flag was set
  if ( !ep_b.any_bits ) {
    throw std::runtime_error (
        "After ep_a.con.feed_queue () ep_b.any_bits should be set\n" );
  }
  // Test if any bit was set
  std::uint_fast32_t bits_b = ep_b.bit_accu.fetch_and_clear ();
  if ( bits_b != ( flag_in | flag_out ) ) {
    throw std::runtime_error ( sev::string::cat (
        "After ep_a.con.feed_queue () ep_b.bit_accu.fetch_bits() "
        "should be ",
        ( flag_in | flag_out ),
        " but is ",
        bits_b,
        "\n" ) );
  }

  // Feed event_a into ep_b and event_b into the queue
  ep_b.con.feed_queue ( bits_b, flag_in, flag_out );

  // Test if any bit was set
  bits_a = ep_a.bit_accu.fetch_and_clear ();
  if ( bits_a != flag_in ) {
    throw std::runtime_error ( sev::string::cat (
        "After ep_b.con.feed_queue() ep_a.bit_accu.fetch_bits() should be ",
        flag_in,
        " but is ",
        bits_a,
        "\n" ) );
  }

  // Test if the list is empty
  if ( ep_a.con.in ( channel_n ).available () ) {
    throw std::runtime_error ( sev::string::cat (
        "ep_a.con.in ( ", channel_n, " ) should be empty\n" ) );
  }
  if ( !ep_b.con.in ( channel_n ).available () ) {
    throw std::runtime_error ( sev::string::cat (
        "ep_b.con.in ( ", channel_n, " ) should not be empty\n" ) );
  }

  // Read from the other end
  ep_a.con.feed_queue ();

  // Test if the list is empty
  if ( !ep_a.con.in ( channel_n ).available () ) {
    throw std::runtime_error ( sev::string::cat (
        "ep_a.con.in ( ", channel_n, ") should not be empty\n" ) );
  }

  // Pop events from the lists
  ep_a.con.in ( channel_n ).accept_one ();
  ep_b.con.in ( channel_n ).accept_one ();
}

void
test_channels ()
{
  std::cout << sev::string::cat ( "test_channels\n" );

  auto event_a = std::make_unique< sev::event::Event > ();
  auto event_b = std::make_unique< sev::event::Event > ();

  sev::event::queue_io::Connection_2 con_a;
  sev::event::queue_io::Connection_2 con_b;

  {
    auto queue = sev::event::queue_io::Reference_2::created ();
    con_a.connect ( queue.link_a () );
    con_b.connect ( queue.link_b () );
  }

  if ( !con_a.is_connected () ) {
    throw std::runtime_error ( "con_a not connected" );
  }
  if ( !con_b.is_connected () ) {
    throw std::runtime_error ( "con_b not connected" );
  }

  con_a.out ().push ( event_a.get () );
  con_b.out ().push ( event_b.get () );

  con_a.feed_queue ();
  con_b.feed_queue ();
  con_a.feed_queue ();

  if ( !con_a.in ().available () ) {
    throw std::runtime_error ( "No event in con_a" );
  }
  if ( con_a.in ().front () != event_b.get () ) {
    throw std::runtime_error ( "Bad event con_a" );
  }

  if ( !con_b.in ().available () ) {
    throw std::runtime_error ( "No event in con_b" );
  }
  if ( con_b.in ().front () != event_a.get () ) {
    throw std::runtime_error ( "Bad event con_b" );
  }

  con_a.in ().accept_all ();
  con_b.in ().accept_all ();

  con_a.feed_queue ();
  con_b.feed_queue ();
  con_a.feed_queue ();

  if ( !con_a.out ().returning () ) {
    throw std::runtime_error ( "No returning event in con_a" );
  }
  con_a.out ().release_all ( [ &event_a ] ( sev::event::Event * event_n ) {
    if ( event_n != event_a.get () ) {
      throw std::runtime_error ( "Wrong event returned in con_a" );
    }
  } );

  if ( !con_b.out ().returning () ) {
    throw std::runtime_error ( "No returning event in con_b" );
  }
  con_b.out ().release_all ( [ &event_b ] ( sev::event::Event * event_n ) {
    if ( event_n != event_b.get () ) {
      throw std::runtime_error ( "Wrong event returned in con_b" );
    }
  } );
}

template < std::size_t N >
void
test_queue ()
{
  using Endpoint = Endpoint_N< N >;
  using Endpoints = std::array< Endpoint, 2 >;

  auto make_endpoints = [] () -> std::unique_ptr< Endpoints > {
    auto res = std::make_unique< Endpoints > ();
    Endpoints & endpoints = *res;

    auto queue = sev::event::queue_io::Reference_N< N >::created ();
    for ( std::size_t ii = 0; ii != 2; ++ii ) {
      auto & ep = endpoints[ ii ];
      ep.bits_func = [ &ep ] () { ep.any_bits = true; };
      ep.bit_accu.set_callback ( ep.bits_func );
      ep.con.set_incoming_notifier (
          [ &ep ] () { ep.bit_accu.set ( flag_in ); } );
      ep.con.set_push_notifier ( [ &ep ] () { ep.bit_accu.set ( flag_out ); } );
      ep.con.connect ( queue.link ( ii ) );
    }

    // -- Tests

    // Test incoming flags
    if ( endpoints[ 0 ].any_bits || endpoints[ 1 ].any_bits ) {
      throw std::runtime_error ( "There shouldn't be bits in the endpoints\n" );
    }
    return res;
  };

  // Send event into one end and pick it from the other end
  for ( std::size_t cc = 0; cc != 2; ++cc ) {
    for ( std::size_t ii = 0; ii != ( N / 2 ); ++ii ) {
      test_single_event< N > ( *make_endpoints (), cc, ii );
    }
  }
  for ( std::size_t cc = 0; cc != 2; ++cc ) {
    for ( std::size_t ii = 0; ii != ( N / 2 ); ++ii ) {
      test_multiple_events< N > ( *make_endpoints (), cc, ii );
    }
  }
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  test_queue< 1 > ();
  test_queue< 2 > ();
  test_channels ();

  return 0;
}

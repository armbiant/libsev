/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "ListModel.hpp"
#include <sev/thread/tracker.hpp>
#include <sev/utility.hpp>

namespace sevq::thread::statistics
{

ListModel::ListModel () {}

ListModel::ListModel ( QObject * parent_n, sev::thread::Tracker * tracker_n )
: QAbstractListModel ( parent_n )
, _tracker ( tracker_n )
{
}

ListModel::~ListModel () = default;

QHash< int, QByteArray >
ListModel::roleNames () const
{
  auto res = QAbstractListModel::roleNames ();
  res.insert ( ExtraRole::Thread, "thread" );
  res.insert ( ExtraRole::ThreadId, "threadId" );
  res.insert ( ExtraRole::ThreadName, "threadName" );
  return res;
}

int
ListModel::rowCount ( const QModelIndex & parent_n ) const
{
  if ( parent_n.isValid () ) {
    return 0;
  }
  return _threads.size ();
}

QVariant
ListModel::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( index_n.parent ().isValid () || ( index_n.column () != 0 ) ||
       ( index_n.row () < 0 ) ||
       ( static_cast< std::size_t > ( index_n.row () ) >= _threads.size () ) ) {
    return QVariant ();
  }

  auto & thread = *_threads[ index_n.row () ];
  switch ( role_n ) {
  case ExtraRole::Thread:
    return QVariant::fromValue ( static_cast< QObject * > ( &thread ) );
  case ExtraRole::ThreadId:
    return QVariant::fromValue ( thread.id () );
  case ExtraRole::ThreadName:
    return QVariant::fromValue ( thread.name () );
  default:
    break;
  }

  return QVariant ();
}

sevq::thread::statistics::Thread *
ListModel::thread_by_id ( std::uint_fast32_t id_n ) const
{
  auto it_end = _threads.cend ();
  auto it = std::find_if (
      _threads.cbegin (), it_end, [ id_n ] ( const auto & thread_n ) {
        return thread_n->id () == id_n;
      } );
  if ( it != it_end ) {
    return it->get ();
  }
  return nullptr;
}

void
ListModel::update ()
{
  if ( _tracker == nullptr ) {
    return;
  }
  {
    // Acquire
    auto stats = _tracker->statistics_acquire ();
    // Rebuild on demand
    if ( sev::change ( _list_changes, stats->list_changes () ) ) {
      rebuild ( *stats );
    }
    // Update
    update ( *stats );
    // Release
    _tracker->statistics_release ( std::move ( stats ) );
  }
}

void
ListModel::rebuild ( const sev::thread::statistics::Threads & stats_n )
{
  // -- Removes
  for ( auto it = _threads.cbegin (); it != _threads.cend (); ) {
    if ( stats_n.thread_by_id ( it->get ()->id () ) != nullptr ) {
      ++it;
      continue;
    }
    // Remove row
    auto index = std::distance ( _threads.cbegin (), it );
    beginRemoveRows ( QModelIndex (), index, index );
    it = _threads.erase ( it );
    endRemoveRows ();
  }

  // -- Inserts
  for ( const auto & thread : stats_n.threads () ) {
    if ( thread_by_id ( thread.id () ) != nullptr ) {
      continue;
    }
    // Insert row
    auto index = _threads.size ();
    beginInsertRows ( QModelIndex (), index, index );
    _threads.emplace_back (
        std::make_unique< sevq::thread::statistics::Thread > ( this, thread ) );
    endInsertRows ();
  }
}

void
ListModel::update ( const sev::thread::statistics::Threads & stats_n )
{
  const std::size_t num = _threads.size ();
  for ( std::size_t ii = 0; ii != num; ++ii ) {
    _threads[ ii ]->update ( stats_n.thread ( ii ) );
  }
}

} // namespace sevq::thread::statistics

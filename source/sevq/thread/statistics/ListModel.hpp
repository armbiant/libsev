/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/thread/statistics/threads.hpp>
#include <sevq/thread/statistics/Thread.hpp>
#include <QAbstractListModel>
#include <QList>
#include <memory>

namespace sev::thread
{
class Tracker;
}

namespace sevq::thread::statistics
{

class ListModel : public QAbstractListModel
{
  Q_OBJECT

  // -- Types

  enum ExtraRole
  {
    Thread = Qt::UserRole,
    ThreadId,
    ThreadName
  };

  public:
  // -- Construction

  ListModel ();

  ListModel ( QObject * parent_n, sev::thread::Tracker * tracker_n );

  ~ListModel ();

  // -- Model interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role_n = Qt::DisplayRole ) const override;

  // -- Updating

  Q_SLOT void
  update ();

  // -- Utility

  sevq::thread::statistics::Thread *
  thread_by_id ( std::uint_fast32_t id_n ) const;

  private:
  void
  rebuild ( const sev::thread::statistics::Threads & stats_n );

  void
  update ( const sev::thread::statistics::Threads & stats_n );

  private:
  // -- Attributes
  sev::thread::Tracker * _tracker = nullptr;
  std::uint_fast32_t _list_changes = 0;
  std::vector< std::unique_ptr< sevq::thread::statistics::Thread > > _threads;
};

} // namespace sevq::thread::statistics

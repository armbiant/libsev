/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "Thread.hpp"
#include <sev/utility.hpp>

namespace sevq::thread::statistics
{

Thread::Thread ( QObject * parent_n,
                 const sev::thread::statistics::Thread & thread_n )
: QObject ( parent_n )
, _id ( thread_n.id () )
, _name ( QString::fromStdString ( thread_n.name () ) )
{
  update ( thread_n );
}

void
Thread::update ( const sev::thread::statistics::Thread & thread_n )
{
  if ( sev::change ( _running, thread_n.is_running () ) ) {
    emit runningChanged ();
  }
  if ( sev::change ( _load, thread_n.frame_load () ) ) {
    emit loadChanged ();
  }
}

} // namespace sevq::thread::statistics

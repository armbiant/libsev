/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/thread/statistics/thread.hpp>
#include <QObject>
#include <QString>
#include <cstdint>

namespace sevq::thread::statistics
{

class Thread : public QObject
{
  Q_OBJECT

  public:
  // -- Properties

  Q_PROPERTY ( int id READ idInt CONSTANT )
  Q_PROPERTY ( QString name READ name CONSTANT )
  Q_PROPERTY ( bool running READ running NOTIFY runningChanged )
  Q_PROPERTY ( double load READ load NOTIFY loadChanged )

  // -- Construction and setup

  Thread ( QObject * parent_n,
           const sev::thread::statistics::Thread & thread_n );

  void
  update ( const sev::thread::statistics::Thread & thread_n );

  // -- Id

  std::uint_fast32_t
  id () const
  {
    return _id;
  }

  int
  idInt () const
  {
    return static_cast< int > ( _id );
  }

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  // -- isRunning

  bool
  running () const
  {
    return _running;
  }

  Q_SIGNAL void
  runningChanged ();

  // -- Load

  double
  load () const
  {
    return _load;
  }

  Q_SIGNAL void
  loadChanged ();

  private:
  // -- Attributes
  std::uint_fast32_t _id = 0;
  QString _name;
  bool _running = false;
  double _load = 0.0;
};

} // namespace sevq::thread::statistics

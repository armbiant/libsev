/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>
#include <cstdint>
#include <iterator>
#include <stdexcept>

namespace sev::mem
{

/// @brief View into an array of items
///
template < class T >
class View
{
  // -- Types
  public:
  using value_type = T;
  using iterator = T *;
  using const_iterator = const T *;
  using reverse_iterator = std::reverse_iterator< iterator >;
  using const_reverse_iterator = std::reverse_iterator< const_iterator >;
  using size_type = std::size_t;

  // -- Constructors

  /// @brief Zero initializes pointer and size
  ///
  constexpr View () = default;

  /// @brief Setup pointer and size
  ///
  constexpr View ( T * items_n, size_type size_n )
  : _items ( items_n )
  , _size ( size_n )
  {
  }

  // -- General

  /// @brief Clears item pointer and size ot zero
  ///
  void
  clear ()
  {
    _items = nullptr;
    _size = 0;
  }

  /// @brief Checks if pointer and size are not zero
  ///
  bool constexpr is_valid () const
  {
    return ( ( _items != nullptr ) && ( _size != 0 ) );
  }

  /// @brief Checks if the size is not zero
  ///
  bool constexpr is_empty () const { return ( _size == 0 ); }

  /// @brief Sets item pointer and number of items
  ///
  void
  reset ( T * items_n, size_type size_n )
  {
    _items = items_n;
    _size = size_n;
  }

  /// @brief Forwards the items() pointer and reduces the size().
  ///
  void
  drop_front ( size_type num_n )
  {
    _items += num_n;
    _size -= num_n;
  }

  /// @brief Moves the items() pointer backwards and increases the size().
  ///
  void
  add_front ( size_type num_n )
  {
    _items -= num_n;
    _size += num_n;
  }

  /// @brief Reduces the size().
  ///
  void
  drop_back ( size_type num_n )
  {
    _size -= num_n;
  }

  /// @brief Increases the size().
  ///
  void
  add_back ( size_type num_n )
  {
    _size += num_n;
  }

  // -- Reference accessors

  /// @brief Pointer to item array
  ///
  constexpr T *
  items () const
  {
    return _items;
  }

  void
  set_items ( T * items_n )
  {
    _items = items_n;
  }

  constexpr T *
  data () const
  {
    return _items;
  }

  /// @brief Number of items
  ///
  size_type constexpr size () const { return _size; }

  void
  set_size ( size_type size_n )
  {
    _size = size_n;
  }

  // -- Byte cast

  /// @brief Returns sizeof ( T )*size()
  ///
  constexpr size_type
  byte_size () const
  {
    return ( _size * sizeof ( T ) );
  }

  /// @brief Casted data pointer
  ///
  constexpr std::uint8_t *
  bytes () const
  {
    return reinterpret_cast< std::uint8_t * > ( _items );
  }

  // -- Item access

  /// @brief Item accessor
  constexpr T &
  get ( size_type index_n ) const
  {
    return _items[ index_n ];
  }

  /// @brief Item accessor with range check
  constexpr T &
  at ( size_type index_n ) const
  {
    if ( index_n >= _size ) {
      throw std::out_of_range ( "View index out of range." );
    }
    return _items[ index_n ];
  }

  constexpr T &
  front () const
  {
    return _items[ 0 ];
  }

  constexpr T &
  back () const
  {
    return _items[ _size - 1 ];
  }

  // -- Iteration

  iterator
  begin () const
  {
    return _items;
  }

  iterator
  cbegin () const
  {
    return _items;
  }

  iterator
  end () const
  {
    return ( _items + _size );
  }

  iterator
  cend () const
  {
    return ( _items + _size );
  }

  reverse_iterator
  rbegin () const
  {
    return reverse_iterator ( end () );
  }

  reverse_iterator
  crbegin () const
  {
    return reverse_iterator ( end () );
  }

  reverse_iterator
  rend () const
  {
    return reverse_iterator ( begin () );
  }

  reverse_iterator
  crend () const
  {
    return reverse_iterator ( begin () );
  }

  // -- Subscript operators

  constexpr T &
  operator[] ( size_type index_n ) const
  {
    return _items[ index_n ];
  }

  // -- Comparison operators

  constexpr bool
  operator== ( const View & view_n ) const
  {
    return ( _items == view_n._items ) && ( _size == view_n._size );
  }

  constexpr bool
  operator!= ( const View & view_n ) const
  {
    return ( _items != view_n._items ) || ( _size != view_n._size );
  }

  private:
  // -- Attributes
  T * _items = nullptr;
  size_type _size = 0;
};

} // namespace sev::mem

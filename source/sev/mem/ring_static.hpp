/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/mem/ring_spans.hpp>
#include <sev/mem/ring_spans_read.hpp>
#include <sev/mem/view.hpp>
#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <new>
#include <type_traits>
#include <utility>

namespace sev::mem
{

/// @brief Static sized ring buffer
///
template < class T, std::size_t CAP >
class Ring_Static
{
  public:
  // -- Types
  using size_type = std::size_t;

  using Span = sev::mem::View< T >;
  using Spans = sev::mem::Ring_Spans< T >;
  using Span_Const = sev::mem::View< const T >;
  using Spans_Const = sev::mem::Ring_Spans_Read< const T >;

  // -- Constructors

  /// @brief Doesn't allocate anything
  Ring_Static ();

  /// @brief Copy constructor
  Ring_Static ( const sev::mem::Ring_Static< T, CAP > & buffer_n );

  /// @brief Move constructor
  Ring_Static ( sev::mem::Ring_Static< T, CAP > && buffer_n );

  ~Ring_Static ();

  void
  clear ();

  void
  fill ( const T & value_n );

  constexpr static size_type
  capacity ()
  {
    return CAP;
  }

  size_type
  size () const
  {
    return _size;
  }

  size_type
  capacity_free () const
  {
    return ( CAP - _size );
  }

  bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Same as is_empty()
  bool
  empty () const
  {
    return ( _size == 0 );
  }

  bool
  is_full () const
  {
    return ( _size == CAP );
  }

  T &
  item ( size_type index_n )
  {
    return items_pointer ()[ ( _begin + index_n ) % CAP ];
  }

  const T &
  item ( size_type index_n ) const
  {
    return items_pointer ()[ ( _begin + index_n ) % CAP ];
  }

  T &
  front ()
  {
    return items_pointer ()[ _begin ];
  }

  const T &
  front () const
  {
    return items_pointer ()[ _begin ];
  }

  T &
  back ()
  {
    return items_pointer ()[ ( _begin + _size - 1 ) % CAP ];
  }

  const T &
  back () const
  {
    return items_pointer ()[ ( _begin + _size - 1 ) % CAP ];
  }

  // -- Back pushing

  /// @brief Trusts in the buffer not being full
  void
  push_back_not_full ( const T & item_n );

  /// @brief Trusts in the buffer not being full
  void
  push_back_not_full ( T && item_n );

  /// @brief Trusts in the buffer not being full
  template < typename... Args >
  void
  emplace_back_not_full ( Args &&... args_n );

  /// @brief Trusts in the buffer to have enough capacity
  template < class Input_Iter >
  void
  append_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  /// @brief Drops the first item on demand
  void
  push_back_dropping ( const T & item_n );

  /// @brief Drops the first item on demand
  void
  push_back_dropping ( T && item_n );

  /// @brief Drops the first item on demand
  template < typename... Args >
  void
  emplace_back_dropping ( Args &&... args_n );

  // -- Back popping

  /// @brief Trusts in the buffer not being empty
  void
  pop_back_not_empty ();

  /// @brief Trusts in the buffer not being empty
  void
  pop_back_not_empty ( T * output_n );

  // -- Front pushing

  /// @brief Trusts in the buffer not being full
  void
  push_front_not_full ( const T & item_n );

  /// @brief Trusts in the buffer not being full
  void
  push_front_not_full ( T && item_n );

  /// @brief Trusts in the buffer not being full
  template < typename... Args >
  void
  emplace_front_not_full ( Args &&... args_n );

  /// @brief Trusts in the buffer not being full
  template < class Input_Iter >
  void
  prepend_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  /// @brief Drops the last item on demand
  void
  push_front_dropping ( const T & item_n );

  /// @brief Drops the last item on demand
  void
  push_front_dropping ( T && item_n );

  /// @brief Drops the last item on demand
  template < typename... Args >
  void
  emplace_front_dropping ( Args &&... args_n );

  // -- Front popping

  /// @brief Trusts in the buffer not being empty
  void
  pop_front_not_empty ();

  /// @brief Trusts in the buffer not being empty
  void
  pop_front_not_empty ( T * output_n );

  // -- Spans

  Span
  span_front ();

  Span
  span_back ();

  Span_Const
  span_front () const;

  Span_Const
  span_back () const;

  Span_Const
  span_const_front () const;

  Span_Const
  span_const_back () const;

  Spans
  spans ();

  Spans_Const
  spans () const;

  Spans_Const
  spans_const () const;

  Spans
  spans_free ();

  // -- Manual size manipulation

  void
  increment_size_count_front ( size_type delta_n );

  void
  increment_size_count_back ( size_type delta_n );

  void
  decrement_size_count_front ( size_type delta_n );

  void
  decrement_size_count_back ( size_type delta_n );

  // -- Search and replace

  std::size_t
  index_of ( const T & match_value_n ) const;

  size_type
  index_of ( const T & match_value_n, size_type begin_index_n ) const;

  void
  remove_at ( std::size_t index_n );

  /// @return True if an item was removed
  bool
  remove_first_match ( const T & match_value_n );

  /// @return True if an item was removed
  bool
  remove_last_match ( const T & match_value_n );

  /// @return Number of items removed
  size_type
  remove_all_matches ( const T & match_value_n );

  // -- Item access operators

  T &
  operator[] ( size_type index_n )
  {
    return item ( index_n );
  }

  const T &
  operator[] ( size_type index_n ) const
  {
    return item ( index_n );
  }

  // -- Assignment operators

  sev::mem::Ring_Static< T, CAP > &
  operator= ( const sev::mem::Ring_Static< T, CAP > & buffer_n );

  sev::mem::Ring_Static< T, CAP > &
  operator= ( sev::mem::Ring_Static< T, CAP > && buffer_n );

  // Private methods
  private:
  T *
  items_pointer ();

  const T *
  items_pointer () const;

  T *
  alloc_front_not_full ();

  void
  destruct_reverse ();

  private:
  // -- Attributes
  size_type _begin = 0;
  size_type _size = 0;
  alignas ( alignof ( T ) ) std::uint8_t _items[ sizeof ( T ) * CAP ];
};

template < class T, std::size_t CAP >
inline T *
Ring_Static< T, CAP >::items_pointer ()
{
  return reinterpret_cast< T * > ( &_items[ 0 ] );
}

template < class T, std::size_t CAP >
inline const T *
Ring_Static< T, CAP >::items_pointer () const
{
  return reinterpret_cast< const T * > ( &_items[ 0 ] );
}

template < class T, std::size_t CAP >
inline Ring_Static< T, CAP >::Ring_Static ()
{
}

template < class T, std::size_t CAP >
Ring_Static< T, CAP >::Ring_Static (
    const sev::mem::Ring_Static< T, CAP > & buffer_n )
{
  Spans_Const cspans = buffer_n.spans_const ();
  T * it_dst = items_pointer ();
  for ( auto & cspan : cspans ) {
    const T * it_src_end ( cspan.end () );
    const T * it_src_cur ( cspan.begin () );
    while ( it_src_cur != it_src_end ) {
      new ( it_dst ) T ( *it_src_cur );
      ++it_dst;
      ++it_src_cur;
    }
  }
  _size = cspans.spans_size ();
}

template < class T, std::size_t CAP >
Ring_Static< T, CAP >::Ring_Static (
    sev::mem::Ring_Static< T, CAP > && buffer_n )
{
  {
    Spans cspans = buffer_n.spans ();
    T * it_dst = items_pointer ();
    for ( auto & cspan : cspans ) {
      const T * it_src_end ( cspan.end () );
      const T * it_src_cur ( cspan.begin () );
      while ( it_src_cur != it_src_end ) {
        new ( it_dst ) T ( std::move ( *it_src_cur ) );
        ++it_dst;
        ++it_src_cur;
      }
    }
    _size = cspans.spans_size ();
  }
  buffer_n.clear ();
}

template < class T, std::size_t CAP >
inline Ring_Static< T, CAP >::~Ring_Static ()
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse ();
  }
}

template < class T, std::size_t CAP >
mem::Ring_Static< T, CAP > &
Ring_Static< T, CAP >::operator= (
    const sev::mem::Ring_Static< T, CAP > & buffer_n )
{
  clear ();
  {
    Spans_Const cspans = buffer_n.spans_const ();
    T * it_dst = items_pointer ();
    for ( auto & cspan : cspans ) {
      const T * it_src_end ( cspan.end () );
      const T * it_src_cur ( cspan.begin () );
      while ( it_src_cur != it_src_end ) {
        new ( it_dst ) T ( *it_src_cur );
        ++it_dst;
        ++it_src_cur;
      }
    }
    _size = cspans.spans_size ();
  }
  return *this;
}

template < class T, std::size_t CAP >
mem::Ring_Static< T, CAP > &
Ring_Static< T, CAP >::operator= ( sev::mem::Ring_Static< T, CAP > && buffer_n )
{
  clear ();
  {
    Spans cspans = buffer_n.spans ();
    T * it_dst = items_pointer ();
    for ( auto & cspan : cspans ) {
      const T * it_src_end ( cspan.end () );
      const T * it_src_cur ( cspan.begin () );
      while ( it_src_cur != it_src_end ) {
        new ( it_dst ) T ( std::move ( *it_src_cur ) );
        ++it_dst;
        ++it_src_cur;
      }
    }
    _size = cspans.spans_size ();
  }
  buffer_n.clear ();
  return *this;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::clear ()
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse ();
  }
  _begin = 0;
  _size = 0;
}

template < class T, std::size_t CAP >
void
Ring_Static< T, CAP >::destruct_reverse ()
{
  if ( _size == 0 ) {
    return;
  }

  const Spans cspans = spans ();
  for ( const Span * cspan : { &cspans[ 1 ], &cspans[ 0 ] } ) {
    const typename Span::const_iterator it_begin ( cspan->begin () );
    typename Span::const_iterator it ( cspan->end () );
    while ( it != it_begin ) {
      --it;
      it->~T ();
    }
  }
}

template < class T, std::size_t CAP >
void
Ring_Static< T, CAP >::fill ( const T & value_n )
{
  spans ().fill ( value_n );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Span
Ring_Static< T, CAP >::span_front ()
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span ( &front (), tsize );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Span
Ring_Static< T, CAP >::span_back ()
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span ( &item ( tsize ), size () - tsize );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Span_Const
Ring_Static< T, CAP >::span_front () const
{
  return span_const_front ();
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Span_Const
Ring_Static< T, CAP >::span_back () const
{
  return span_const_back ();
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Span_Const
Ring_Static< T, CAP >::span_const_front () const
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span_Const ( &front (), tsize );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Span_Const
Ring_Static< T, CAP >::span_const_back () const
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span_Const ( &item ( tsize ), size () - tsize );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Spans
Ring_Static< T, CAP >::spans ()
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Spans ( &front (), tsize, &item ( tsize ), size () - tsize );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Spans_Const
Ring_Static< T, CAP >::spans () const
{
  return spans_const ();
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Spans_Const
Ring_Static< T, CAP >::spans_const () const
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Spans_Const ( &front (), tsize, &item ( tsize ), size () - tsize );
}

template < class T, std::size_t CAP >
inline typename Ring_Static< T, CAP >::Spans
Ring_Static< T, CAP >::spans_free ()
{
  const size_type sfree = capacity_free ();
  const size_type index_begin = ( ( _begin + size () ) % capacity () );
  const size_type tsize = std::min ( ( capacity () - index_begin ), sfree );
  return Spans ( &( items_pointer ()[ index_begin ] ),
                 tsize,
                 &( items_pointer ()[ ( index_begin + tsize ) % capacity () ] ),
                 ( sfree - tsize ) );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::increment_size_count_front ( size_type delta_n )
{
  _size += delta_n;
  DEBUG_ASSERT ( _size <= CAP );
  // Decrement the begin index with bottom looping
  _begin += CAP;
  _begin -= delta_n;
  _begin %= CAP;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::increment_size_count_back ( size_type delta_n )
{
  _size += delta_n;
  DEBUG_ASSERT ( _size <= CAP );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::decrement_size_count_front ( size_type delta_n )
{
  DEBUG_ASSERT ( delta_n <= _size );
  _size -= delta_n;
  // Decrement the begin index with bottom looping
  _begin += delta_n;
  _begin %= CAP;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::decrement_size_count_back ( size_type delta_n )
{
  DEBUG_ASSERT ( delta_n <= _size );
  _size -= delta_n;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_back_not_full ( const T & item_n )
{
  new ( ( items_pointer () + ( ( _begin + _size ) % CAP ) ) ) T ( item_n );
  ++_size;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_back_not_full ( T && item_n )
{
  new ( ( items_pointer () + ( ( _begin + _size ) % CAP ) ) )
      T ( std::move ( item_n ) );
  ++_size;
}

template < class T, std::size_t CAP >
template < typename... Args >
inline void
Ring_Static< T, CAP >::emplace_back_not_full ( Args &&... args_n )
{
  new ( ( items_pointer () + ( ( _begin + _size ) % CAP ) ) )
      T ( std::forward< Args > ( args_n )... );
  ++_size;
}

template < class T, std::size_t CAP >
template < class Input_Iter >
void
Ring_Static< T, CAP >::append_copy_not_full ( Input_Iter begin_n,
                                              Input_Iter end_n )
{
  increment_size_count_back (
      spans_free ().append_copy_not_full ( begin_n, end_n ) );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_back_dropping ( const T & item_n )
{
  if ( is_full () ) {
    pop_front_not_empty ();
  }
  push_back_not_full ( item_n );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_back_dropping ( T && item_n )
{
  if ( is_full () ) {
    pop_front_not_empty ();
  }
  push_back_not_full ( std::move ( item_n ) );
}

template < class T, std::size_t CAP >
template < typename... Args >
inline void
Ring_Static< T, CAP >::emplace_back_dropping ( Args &&... args_n )
{
  if ( is_full () ) {
    pop_front_not_empty ();
  }
  emplace_back_not_full ( std::forward< Args > ( args_n )... );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::pop_back_not_empty ()
{
  back ().~T ();
  --_size;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::pop_back_not_empty ( T * output_n )
{
  T & bitem ( back () );
  *output_n = std::move ( bitem );
  bitem.~T ();
  --_size;
}

template < class T, std::size_t CAP >
inline T *
Ring_Static< T, CAP >::alloc_front_not_full ()
{
  DEBUG_ASSERT ( !is_full () );
  ++_size;
  {
    // Decrement the begin index with bottom looping
    _begin += CAP;
    --_begin;
    _begin %= CAP;
  }
  return &front ();
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_front_not_full ( const T & item_n )
{
  new ( alloc_front_not_full () ) T ( item_n );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_front_not_full ( T && item_n )
{
  new ( alloc_front_not_full () ) T ( item_n );
}

template < class T, std::size_t CAP >
template < typename... Args >
inline void
Ring_Static< T, CAP >::emplace_front_not_full ( Args &&... args_n )
{
  new ( alloc_front_not_full () ) T ( std::forward< Args > ( args_n )... );
}

template < class T, std::size_t CAP >
template < class Input_Iter >
void
Ring_Static< T, CAP >::prepend_copy_not_full ( Input_Iter begin_n,
                                               Input_Iter end_n )
{
  increment_size_count_front (
      spans_free ().prepend_copy_not_full ( begin_n, end_n ) );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_front_dropping ( const T & item_n )
{
  if ( is_full () ) {
    pop_back_not_empty ();
  }
  push_front_not_full ( item_n );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::push_front_dropping ( T && item_n )
{
  if ( is_full () ) {
    pop_back_not_empty ();
  }
  push_front_not_full ( std::move ( item_n ) );
}

template < class T, std::size_t CAP >
template < typename... Args >
inline void
Ring_Static< T, CAP >::emplace_front_dropping ( Args &&... args_n )
{
  if ( is_full () ) {
    pop_back_not_empty ();
  }
  emplace_front_not_full ( std::forward< Args > ( args_n )... );
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::pop_front_not_empty ()
{
  front ().~T ();
  {
    // Increment begin index with end looping
    ++_begin;
    _begin %= CAP;
  }
  --_size;
}

template < class T, std::size_t CAP >
inline void
Ring_Static< T, CAP >::pop_front_not_empty ( T * output_n )
{
  T & fitem ( front () );
  *output_n = std::move ( fitem );
  fitem.~T ();
  {
    // Increment begin index with end looping
    ++_begin;
    _begin %= CAP;
  }
  --_size;
}

template < class T, std::size_t CAP >
typename Ring_Static< T, CAP >::size_type
Ring_Static< T, CAP >::index_of ( const T & match_value_n ) const
{
  return spans_const ().index_of ( match_value_n );
}

template < class T, std::size_t CAP >
typename Ring_Static< T, CAP >::size_type
Ring_Static< T, CAP >::index_of ( const T & match_value_n,
                                  size_type begin_index_n ) const
{
  return spans_const ().index_of ( match_value_n, begin_index_n );
}

template < class T, std::size_t CAP >
void
Ring_Static< T, CAP >::remove_at ( std::size_t index_n )
{
  Spans cspans = spans ();
  const int mdir = cspans.remove_at ( index_n );
  if ( mdir == 0 ) {
    return;
  }

  if ( mdir < 0 ) {
    decrement_size_count_back ( 1 );
  } else {
    decrement_size_count_front ( 1 );
  }
}

template < class T, std::size_t CAP >
bool
Ring_Static< T, CAP >::remove_first_match ( const T & match_value_n )
{
  Spans cspans = spans ();
  const int mdir = cspans.remove_first_match ( match_value_n );
  if ( mdir == 0 ) {
    return false;
  }

  if ( mdir < 0 ) {
    decrement_size_count_back ( 1 );
  } else {
    decrement_size_count_front ( 1 );
  }
  return true;
}

template < class T, std::size_t CAP >
bool
Ring_Static< T, CAP >::remove_last_match ( const T & match_value_n )
{
  Spans cspans ( spans () );
  const int mdir ( cspans.remove_last_match ( match_value_n ) );
  if ( mdir == 0 ) {
    return false;
  }

  if ( mdir < 0 ) {
    decrement_size_count_back ( 1 );
  } else {
    decrement_size_count_front ( 1 );
  }
  return true;
}

template < class T, std::size_t CAP >
typename Ring_Static< T, CAP >::size_type
Ring_Static< T, CAP >::remove_all_matches ( const T & match_value_n )
{
  if ( is_empty () ) {
    return 0;
  }

  // Init with search start index
  size_type num_found ( 0 );
  size_type index ( 0 );
  do {
    index = index_of ( match_value_n, index );
    if ( index == _size ) {
      break;
    }
    remove_at ( index );
    ++num_found;
  } while ( !is_empty () );

  return num_found;
}

} // namespace sev::mem

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/mem/list/se/item.hpp>
#include <cstddef>
#include <utility>

namespace sev::mem::list::se
{

/// @brief Singely linked list of items
///
class Basic_List
{
  public:
  // -- Construction

  Basic_List ()
  : _front ( &_back )
  , _back ( &_front )
  {
  }

  Basic_List ( const Basic_List & list_n ) = delete;

  Basic_List ( Basic_List && list_n )
  : Basic_List ()
  {
    if ( list_n.is_empty () ) {
      return;
    }
    _front._link = list_n._front.link ();
    _back._link = list_n._back.link ();
    list_n.reset ();
  }

  ~Basic_List () = default;

  // -- Setup

  void
  reset ()
  {
    _front._link = &_back;
    _back._link = &_front;
  }

  // -- First item accessor

  Item *
  front () const
  {
    return _front.link ();
  }

  Item *
  back () const
  {
    return _back.link ();
  }

  bool
  is_empty () const
  {
    return _front.link () == &_back;
  }

  /// @brief Counts the items in the list
  std::size_t
  count () const;

  // -- Push

  void
  push_back ( Item * item_n )
  {
    DEBUG_ASSERT ( !item_n->links () );
    item_n->_link = &_back;
    _back.link ()->_link = item_n;
    _back._link = item_n;
  }

  void
  push_front ( Item * item_n )
  {
    DEBUG_ASSERT ( !item_n->links () );
    item_n->_link = _front.link ();
    if ( item_n->_link == &_back ) {
      _back._link = item_n;
    }
    _front._link = item_n;
  }

  // -- Pop

  /// @brief Assumes !is_empty()
  Item *
  pop_front_not_empty ()
  {
    Item * res = _front.link ();
    extract_item ( _front, *res );
    return res;
  }

  // -- Extract

  void
  extract_item ( Item & prev_n, Item & item_n )
  {
    prev_n._link = item_n._link;
    if ( prev_n._link == &_back ) {
      _back._link = &prev_n;
    }
    item_n._link = nullptr;
  }

  // -- Splice

  /// @brief Appends the items from @a src_n list to this and clears @a src_n.
  ///
  /// @a src_n must not be empty
  void
  splice_back_not_empty ( Basic_List & src_n )
  {
    // Update the last next pointer
    _back.link ()->_link = src_n._front.link ();
    // Update the last item back() pointer
    src_n._back.link ()->_link = &_back;
    _back._link = src_n._back.link ();
    // Clear source list
    src_n.reset ();
  }

  /// @brief Appends the items from @a src_n list to this and clears @a src_n
  void
  splice_back ( Basic_List & src_n )
  {
    if ( !src_n.is_empty () ) {
      splice_back_not_empty ( src_n );
    }
  }

  // -- Swap

  void
  swap ( Basic_List & list_n );

  // -- Subscript operators

  Item *
  operator[] ( std::size_t index_n )
  {
    Item * item = _front.link ();
    for ( ; index_n != 0; --index_n ) {
      item = item->link ();
    }
    return item;
  }

  const Item *
  operator[] ( std::size_t index_n ) const
  {
    const Item * item = _front.link ();
    for ( ; index_n != 0; --index_n ) {
      item = item->link ();
    }
    return item;
  }

  // -- Assignment

  void
  assign_move ( Basic_List && list_n )
  {
    if ( list_n.is_empty () ) {
      reset ();
      return;
    }
    _front._link = list_n._front.link ();
    _back._link = list_n._back.link ();
    list_n.reset ();
  }

  // -- Assignment operators

  Basic_List &
  operator= ( const Basic_List & list_n ) = delete;

  Basic_List &
  operator= ( Basic_List && list_n )
  {
    assign_move ( std::move ( list_n ) );
    return *this;
  }

  protected:
  // -- Attributes
  Item _front;
  Item _back;
};

} // namespace sev::mem::list::se

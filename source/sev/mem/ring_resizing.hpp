/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/math/numbers.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <utility>

namespace sev::mem
{

/// @brief Ring_Fixed that increases its capacity if it's full on a push
/// and decreases its capacity if its free size grows over a settable limit.
///
template < class T >
class Ring_Resizing : public Ring_Fixed< T >
{
  public:
  // -- Types
  using size_type = typename Ring_Fixed< T >::size_type;
  static const size_type capacity_block_size_default = 8;
  static const size_type capacity_shrink_limit_default =
      ( capacity_block_size_default * 3 );

  // -- Construction

  /// @brief Doesn't allocate anything
  Ring_Resizing ();

  /// @brief Allocates a chunk of memory
  Ring_Resizing ( size_type capacity_n );

  /// @brief Delete copy constructor
  ///
  Ring_Resizing ( const Ring_Resizing< T > & buffer_n );

  /// @brief Move constructor
  ///
  Ring_Resizing ( Ring_Resizing< T > && buffer_n );

  // -- Automatic capacity increase and decrease

  size_type
  capacity_block_size () const
  {
    return _capacity_block_size;
  }

  void
  set_capacity_block_size ( size_type value_n );

  size_type
  capacity_shrink_limit () const
  {
    return _capacity_shrink_limit;
  }

  void
  set_capacity_shrink_limit ( size_type value_n );

  void
  increase_capacity_auto ();

  void
  decrease_capacity_auto ();

  // -- Back pushing

  /// @brief Pushes an item at the back
  ///
  void
  push_back ( const T & item_n );

  /// @brief Pushes an item at the back
  ///
  void
  push_back ( T && item_n );

  /// @brief Emplaces an item at the back
  ///
  template < typename... Args >
  void
  emplace_back ( Args &&... args_n );

  // -- Back popping

  bool
  pop_back ();

  bool
  pop_back ( T * output_n );

  // -- Front pushing

  /// @brief Pushes an item at the front
  ///
  void
  push_front ( const T & item_n );

  /// @brief Pushes an item at the front
  ///
  void
  push_front ( T && item_n );

  /// @brief Emplaces an item at the front
  ///
  template < typename... Args >
  void
  emplace_front ( Args &&... args_n );

  // -- Front popping

  bool
  pop_front ();

  bool
  pop_front ( T * output_n );

  // -- Operators

  Ring_Resizing< T > &
  operator= ( const Ring_Resizing< T > & buffer_n );

  Ring_Resizing< T > &
  operator= ( Ring_Resizing< T > && buffer_n );

  private:
  // -- Utility

  void
  increase_capacity ();

  void
  decrease_capacity ();

  bool
  decrease_capacity_test ();

  private:
  // -- Attributes
  size_type _capacity_block_size = 0;
  size_type _capacity_shrink_limit = 0;
};

template < class T >
Ring_Resizing< T >::Ring_Resizing ()
: _capacity_block_size ( capacity_block_size_default )
, _capacity_shrink_limit ( capacity_shrink_limit_default )
{
}

template < class T >
Ring_Resizing< T >::Ring_Resizing ( size_type capacity_n )
: Ring_Fixed< T > ( capacity_n )
, _capacity_block_size ( capacity_block_size_default )
, _capacity_shrink_limit ( capacity_shrink_limit_default )
{
}

template < class T >
Ring_Resizing< T >::Ring_Resizing ( const Ring_Resizing< T > & buffer_n )
: Ring_Fixed< T > ( buffer_n )
, _capacity_block_size ( buffer_n._capacity_block_size )
, _capacity_shrink_limit ( buffer_n._capacity_shrink_limit )
{
}

template < class T >
Ring_Resizing< T >::Ring_Resizing ( Ring_Resizing< T > && buffer_n )
: Ring_Fixed< T > ( std::move ( buffer_n ) )
, _capacity_block_size ( buffer_n._capacity_block_size )
, _capacity_shrink_limit ( buffer_n._capacity_shrink_limit )
{
}

template < class T >
Ring_Resizing< T > &
Ring_Resizing< T >::operator= ( const Ring_Resizing< T > & buffer_n )
{
  Ring_Fixed< T >::copy_assign ( buffer_n );
  _capacity_block_size = buffer_n._capacity_block_size;
  _capacity_shrink_limit = buffer_n._capacity_shrink_limit;
  return *this;
}

template < class T >
inline Ring_Resizing< T > &
Ring_Resizing< T >::operator= ( Ring_Resizing< T > && buffer_n )
{
  Ring_Fixed< T >::move_assign ( std::move ( buffer_n ) );
  _capacity_block_size = buffer_n._capacity_block_size;
  _capacity_shrink_limit = buffer_n._capacity_shrink_limit;
  return *this;
}

template < class T >
void
Ring_Resizing< T >::set_capacity_block_size ( size_type value_n )
{
  _capacity_block_size = value_n;
  math::assign_larger ( _capacity_block_size, size_type ( 1 ) );
}

template < class T >
void
Ring_Resizing< T >::set_capacity_shrink_limit ( size_type value_n )
{
  _capacity_shrink_limit = value_n;
  math::assign_larger ( _capacity_shrink_limit, size_type ( 1 ) );
}

template < class T >
inline void
Ring_Resizing< T >::increase_capacity_auto ()
{
  if ( Ring_Fixed< T >::is_full () ) {
    increase_capacity ();
  }
}

template < class T >
void
Ring_Resizing< T >::increase_capacity ()
{
  size_type new_size ( Ring_Fixed< T >::capacity () );
  new_size += _capacity_block_size;
  Ring_Fixed< T >::set_capacity ( new_size );
}

template < class T >
void
Ring_Resizing< T >::decrease_capacity ()
{
  size_type new_capa ( Ring_Fixed< T >::size () );
  // Align to block size
  new_capa += ( _capacity_block_size - 1 );
  new_capa /= _capacity_block_size;
  new_capa *= _capacity_block_size;
  Ring_Fixed< T >::set_capacity ( new_capa );
}

template < class T >
inline bool
Ring_Resizing< T >::decrease_capacity_test ()
{
  return ( Ring_Fixed< T >::capacity_free () > _capacity_shrink_limit );
}

template < class T >
inline void
Ring_Resizing< T >::decrease_capacity_auto ()
{
  if ( decrease_capacity_test () ) {
    decrease_capacity ();
  }
}

template < class T >
inline void
Ring_Resizing< T >::push_back ( const T & item_n )
{
  increase_capacity_auto ();
  Ring_Fixed< T >::push_back_not_full ( item_n );
}

template < class T >
inline void
Ring_Resizing< T >::push_back ( T && item_n )
{
  increase_capacity_auto ();
  Ring_Fixed< T >::push_back_not_full ( std::move ( item_n ) );
}

template < class T >
template < typename... Args >
inline void
Ring_Resizing< T >::emplace_back ( Args &&... args_n )
{
  increase_capacity_auto ();
  Ring_Fixed< T >::emplace_back_not_full ( std::forward< Args > ( args_n )... );
}

template < class T >
inline bool
Ring_Resizing< T >::pop_back ()
{
  if ( Ring_Fixed< T >::is_empty () ) {
    return false;
  }

  Ring_Fixed< T >::pop_back_not_empty ();
  decrease_capacity_auto ();
  return true;
}

template < class T >
inline bool
Ring_Resizing< T >::pop_back ( T * output_n )
{
  if ( Ring_Fixed< T >::is_empty () ) {
    return false;
  }

  Ring_Fixed< T >::pop_back_not_empty ( output_n );
  decrease_capacity_auto ();
  return true;
}

template < class T >
inline void
Ring_Resizing< T >::push_front ( const T & item_n )
{
  increase_capacity_auto ();
  Ring_Fixed< T >::push_front_not_full ( item_n );
}

template < class T >
inline void
Ring_Resizing< T >::push_front ( T && item_n )
{
  increase_capacity_auto ();
  Ring_Fixed< T >::push_front_not_full ( std::move ( item_n ) );
}

template < class T >
template < typename... Args >
inline void
Ring_Resizing< T >::emplace_front ( Args &&... args_n )
{
  increase_capacity_auto ();
  Ring_Fixed< T >::emplace_front_not_full (
      std::forward< Args > ( args_n )... );
}

template < class T >
inline bool
Ring_Resizing< T >::pop_front ()
{
  if ( Ring_Fixed< T >::is_empty () ) {
    return false;
  }

  Ring_Fixed< T >::pop_front_not_empty ();
  decrease_capacity_auto ();
  return true;
}

template < class T >
inline bool
Ring_Resizing< T >::pop_front ( T * output_n )
{
  if ( Ring_Fixed< T >::is_empty () ) {
    return false;
  }

  Ring_Fixed< T >::pop_front_not_empty ( output_n );
  decrease_capacity_auto ();
  return true;
}

} // namespace sev::mem

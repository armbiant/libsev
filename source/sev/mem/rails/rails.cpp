/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "rails.hpp"
#include <sev/assert.hpp>
#include <sev/mem/align.hpp>
#include <sev/mem/rails/chunk.hpp>
#include <sev/mem/rails/portal.hpp>
#include <sev/mem/rails/rail.hpp>
#include <sev/mem/view.hpp>
#include <algorithm>
#include <stdexcept>

namespace sev::mem::rails
{

/// @brief A rail with only one data item requires
///        this amount of space for meta information
static constexpr std::size_t meta_size_minimal =
    sizeof ( sev::mem::rails::Rail ) +      //
    2 * sizeof ( sev::mem::rails::Chunk ) + //
    2 * sizeof ( sev::mem::rails::Portal );

} // namespace sev::mem::rails

namespace sev::mem::rails
{

Rails::Rails ()
{
  static_assert ( sev::mem::is_aligned ( sizeof ( Rail ) ) );
  static_assert ( sev::mem::is_aligned ( sizeof ( Chunk ) ) );
  static_assert ( sev::mem::is_aligned ( sizeof ( Portal ) ) );

  set_rail_capacity ( _rail_capacity_default );
}

Rails::Rails ( const Rails & rails_n )
{
  assign_copy ( rails_n );
}

Rails::Rails ( Rails && rails_n )
: _chunk_front ( rails_n._chunk_front )
, _chunk_back ( rails_n._chunk_back )
, _rail_front ( std::move ( rails_n._rail_front ) )
, _rail_back ( std::move ( rails_n._rail_back ) )
, _size ( rails_n._size )
, _rails ( rails_n._rails )
, _rail_capacity ( rails_n._rail_capacity )
, _pool_capacity ( rails_n._pool_capacity )
, _pool_size ( rails_n._pool_capacity )
, _pool_top ( std::move ( rails_n._rail_back ) )
{
  // Reset source
  rails_n._chunk_front = nullptr;
  rails_n._chunk_back = nullptr;
  rails_n._rail_front = nullptr;
  rails_n._rail_back = nullptr;
  rails_n._size = 0;
  rails_n._rails = 0;
  rails_n._rail_capacity = 0;
  rails_n._pool_capacity = 0;
  rails_n._pool_size = 0;
  rails_n._pool_top = nullptr;
}

Rails::~Rails ()
{
  // Destroy rails
  auto destroy_rails = [ this ] ( Rail * rail_n ) {
    while ( rail_n != nullptr ) {
      Rail * rail_prev = rail_n->links ().prev;
      Rail::disconnect ( rail_prev, rail_n );
      this->destroy_rail ( rail_n );
      rail_n = rail_prev;
    }
  };

  destroy_rails ( _rail_back );
  destroy_rails ( _pool_top );
}

void
Rails::assign_copy ( const Rails & rails_n )
{
  // Clear
  if ( rails_total () != 0 ) {
    set_pool_capacity ( 0 );
    clear ();
  }

  // Setup capacities
  set_rail_capacity ( rails_n.rail_capacity () );
  set_pool_capacity ( rails_n.pool_capacity () );

  for ( auto dref : rails_n ) {
    // Raw copy to allocated chunk
    std::byte * chunk = allocate_back ( dref.size () );
    std::copy_n ( dref.items (), dref.size (), chunk );
  }
}

void
Rails::assign_move ( Rails && rails_n )
{
  if ( &rails_n == this ) {
    return;
  }

  // Clear
  if ( rails_total () != 0 ) {
    set_pool_capacity ( 0 );
    clear ();
  }

  // Move
  _chunk_front = rails_n._chunk_front;
  _chunk_back = rails_n._chunk_back;
  _rail_front = rails_n._rail_front;
  _rail_back = rails_n._rail_back;
  _size = rails_n._size;
  _rails = rails_n._rails;
  _rail_capacity = rails_n._rail_capacity;
  _pool_capacity = rails_n._pool_capacity;
  _pool_size = rails_n._pool_size;
  _pool_top = rails_n._pool_top;

  // Reset source
  rails_n._chunk_front = nullptr;
  rails_n._chunk_back = nullptr;
  rails_n._rail_front = nullptr;
  rails_n._rail_back = nullptr;
  rails_n._size = 0;
  rails_n._rails = 0;
  rails_n._rail_capacity = 0;
  rails_n._pool_capacity = 0;
  rails_n._pool_size = 0;
  rails_n._pool_top = nullptr;
}

void
Rails::clear ()
{
  if ( _size == 0 ) {
    return;
  }

  // Reset state
  _chunk_front = nullptr;
  _chunk_back = nullptr;
  _size = 0;

  // Release rails
  DEBUG_ASSERT ( _rail_front != nullptr );
  DEBUG_ASSERT ( _rail_back != nullptr );
  {
    Rail * rail_back = _rail_back;
    while ( rail_back != nullptr ) {
      Rail * rail_release = rail_back;
      rail_back = rail_back->links ().prev;
      Rail::disconnect ( rail_back, rail_release );
      release_rail ( rail_release );
    }
  }
  _rail_front = nullptr;
  _rail_back = nullptr;
}

void
Rails::set_rail_capacity ( size_type size_n )
{
  _rail_capacity =
      sev::mem::aligned_up ( std::max ( size_n, _rail_capacity_minimum ) );
}

std::byte *
Rails::allocate_front ( size_type size_n )
{
  if ( size_n > _rail_capacity ) {
    throw std::length_error ( "Allocation size larger than rail capacity." );
  }

  // Alocate block aligned size
  return allocate_block_front ( sev::mem::aligned_up (
      std::max ( size_n, sev::mem::align_block_size ) ) );
}

std::byte *
Rails::allocate_block_front ( size_type block_size_n )
{
  // Checks
  DEBUG_ASSERT ( block_size_n != 0 );
  DEBUG_ASSERT ( block_size_n <= rail_capacity () );
  DEBUG_ASSERT ( sev::mem::is_aligned ( block_size_n ) );

  // This amount of memory is needed
  const size_type rsize = ( block_size_n + sizeof ( Chunk ) );
  DEBUG_ASSERT ( sev::mem::is_aligned ( rsize ) );

  // Check the available size in the current front chunk
  size_type size_free = ( _chunk_front != nullptr )
                            ? _chunk_front->portal_front ()->size_free
                            : 0;
  // Allocate a new rail on demand
  if ( size_free < rsize ) {
    new_rail_front ();
    size_free = _chunk_front->portal_front ()->size_free;
  }

  // Checks
  DEBUG_ASSERT ( sev::mem::is_aligned ( size_free ) );
  DEBUG_ASSERT ( size_free >= rsize );
  DEBUG_ASSERT ( _chunk_front->portal_front ()->chunk_next == _chunk_front );

  // Enough space available. Use rail.
  _chunk_front->size_front = block_size_n;
  // New end chunk
  Chunk * chunk_new = reinterpret_cast< Chunk * > (
      reinterpret_cast< std::byte * > ( _chunk_front ) - rsize );
  chunk_new->size_front = 0;
  chunk_new->size_back = block_size_n;
  // New end portal
  Portal * portal = chunk_new->portal_front ();
  portal->size_free = size_free - rsize;
  portal->chunk_next = chunk_new;
  // Update rail last chunk pointer
  _chunk_front = chunk_new;
  // Increment size statistics
  ++_size;

  DEBUG_ASSERT ( ( reinterpret_cast< std::byte * > ( chunk_new ) -
                   sizeof ( Portal ) ) >= _rail_front->arena_begin () );

  // Return data chunk pointer
  return reinterpret_cast< std::byte * > ( _chunk_front->data_back () );
}

std::byte *
Rails::allocate_back ( size_type size_n )
{
  if ( size_n > _rail_capacity ) {
    throw std::length_error ( "Allocation size larger than rail capacity." );
  }

  // Alocate block aligned size
  return allocate_block_back ( sev::mem::aligned_up (
      std::max ( size_n, sev::mem::align_block_size ) ) );
}

std::byte *
Rails::allocate_block_back ( size_type block_size_n )
{
  // Checks
  DEBUG_ASSERT ( block_size_n != 0 );
  DEBUG_ASSERT ( block_size_n <= rail_capacity () );
  DEBUG_ASSERT ( sev::mem::is_aligned ( block_size_n ) );

  // This amount of memory is needed
  const size_type rsize = ( block_size_n + sizeof ( Chunk ) );
  DEBUG_ASSERT ( sev::mem::is_aligned ( rsize ) );

  // Check the available size in the current back chunk
  size_type size_free =
      ( _chunk_back != nullptr ) ? _chunk_back->portal_back ()->size_free : 0;
  // Allocate a new rail on demand
  if ( size_free < rsize ) {
    new_rail_back ();
    size_free = _chunk_back->portal_back ()->size_free;
  }

  // Checks
  DEBUG_ASSERT ( sev::mem::is_aligned ( size_free ) );
  DEBUG_ASSERT ( size_free >= rsize );
  DEBUG_ASSERT ( _chunk_back->portal_back ()->chunk_next == _chunk_back );

  // Enough space available. Use rail.
  _chunk_back->size_back = block_size_n;
  // New end chunk
  Chunk * const chunk_data = _chunk_back;
  Chunk * chunk_new = reinterpret_cast< Chunk * > (
      reinterpret_cast< std::byte * > ( _chunk_back ) + rsize );
  chunk_new->size_front = block_size_n;
  chunk_new->size_back = 0;
  // New end portal
  Portal * portal = chunk_new->portal_back ();
  portal->size_free = size_free - rsize;
  portal->chunk_next = chunk_new;
  // Update rail last chunk pointer
  _chunk_back = chunk_new;
  // Increment size statistics
  ++_size;

  DEBUG_ASSERT ( ( reinterpret_cast< std::byte * > ( chunk_new ) +
                   sizeof ( Chunk ) + sizeof ( Portal ) ) <=
                 _rail_back->arena_end () );

  // Return data chunk pointer
  return reinterpret_cast< std::byte * > ( chunk_data->data_back () );
}

void
Rails::push_front ( Const_Chunk_View view_n )
{
  auto * chunk = allocate_front ( view_n.size () );
  std::copy ( view_n.begin (), view_n.end (), chunk );
}

void
Rails::push_back ( Const_Chunk_View view_n )
{
  auto * chunk = allocate_back ( view_n.size () );
  std::copy ( view_n.begin (), view_n.end (), chunk );
}

void
Rails::pop_front ()
{
  if ( is_empty () ) {
    return;
  }

  const Portal portal_prev = *_chunk_front->portal_front ();
  Chunk * chunk = _chunk_front->chunk_next ();
  if ( chunk->size_back != 0 ) {
    // Chunk is not the last one in the rail.
    // Write new portal
    Portal & portal = *chunk->portal_front ();
    portal.size_free =
        portal_prev.size_free + chunk->size_front + sizeof ( Chunk );
    portal.chunk_next = portal_prev.chunk_next;
    chunk->size_front = 0;
    _chunk_front = chunk;
    --_size;
    return;
  }

  // Chunk is the last one in the rail
  if ( !_rail_front->links ().next ) {
    // This is the last remaining rail
    // Clear rail chain
    {
      Rail * rail_release = _rail_front;
      _rail_front = nullptr;
      _rail_back = nullptr;
      release_rail ( rail_release );
    }
    // Reset end chunks
    _chunk_front = nullptr;
    _chunk_back = nullptr;
    // Update size
    DEBUG_ASSERT ( _size == 1 );
    --_size;
  } else {
    // There is another rail afterwards
    // Update end portals and chunks
    Chunk * chunk_next = chunk->portal_back ()->chunk_next;
    chunk_next->portal_front ()->chunk_next = chunk_next;
    _chunk_front = chunk_next;
    // Update rail chain
    {
      Rail * rail_release = _rail_front;
      _rail_front = _rail_front->links ().next;
      Rail::disconnect ( rail_release, _rail_front );
      release_rail ( rail_release );
    }
    // Update size
    DEBUG_ASSERT ( _size > 1 );
    --_size;
  }
}

void
Rails::pop_back ()
{
  if ( is_empty () ) {
    return;
  }

  const Portal portal_prev = *_chunk_back->portal_back ();
  Chunk * chunk = _chunk_back->chunk_prev ();
  if ( chunk->size_front != 0 ) {
    // This is not the last chunk in the rail
    // Write new portal
    Portal & portal = *chunk->portal_back ();
    portal.size_free =
        portal_prev.size_free + chunk->size_front + sizeof ( Chunk );
    portal.chunk_next = portal_prev.chunk_next;
    chunk->size_back = 0;
    _chunk_back = chunk;
    --_size;
    return;
  }

  // Chunk is the first one in the rail
  if ( !_rail_back->links ().prev ) {
    // This is the last rail
    // Clear rail chain
    {
      Rail * rail_release = _rail_back;
      _rail_back = nullptr;
      _rail_front = nullptr;
      release_rail ( rail_release );
    }
    // Reset end chunks
    _chunk_front = nullptr;
    _chunk_back = nullptr;
    // Update size
    DEBUG_ASSERT ( _size == 1 );
    --_size;
  } else {
    // There is another rail before
    // Update end portals and chunks
    Chunk * chunk_next = chunk->portal_front ()->chunk_next;
    chunk_next->portal_back ()->chunk_next = chunk_next;
    _chunk_back = chunk_next;
    // Update rail chain
    {
      Rail * rail_release = _rail_back;
      _rail_back = _rail_back->links ().prev;
      Rail::disconnect ( _rail_back, rail_release );
      release_rail ( rail_release );
    }
    // Update size
    DEBUG_ASSERT ( _size > 1 );
    --_size;
  }
}

/// @brief Allocates a new rail and prepends it to the rail chain
/// @arg capacity_n The rail must be capable to allocate a chunk of this size
void
Rails::new_rail_front ()
{
  // New rail and initial chunk
  Rail * rail_new = acquire_rail ();
  Chunk * chunk_new = rail_new->reset_back_chunk ();

  if ( _rail_front == nullptr ) {
    // First allocated rail
    DEBUG_ASSERT ( _chunk_front == nullptr );
    DEBUG_ASSERT ( _chunk_back == nullptr );
    DEBUG_ASSERT ( _rail_back == nullptr );

    _rail_front = rail_new;
    _rail_back = rail_new;
    _chunk_front = chunk_new;
    _chunk_back = chunk_new;
  } else {
    // This is not the first allocated rail
    DEBUG_ASSERT ( _rail_front->links ().prev == nullptr );
    DEBUG_ASSERT ( _chunk_front != nullptr );
    DEBUG_ASSERT ( _chunk_front->size_front == 0 );
    DEBUG_ASSERT ( _chunk_front->size_back != 0 );
    DEBUG_ASSERT ( _chunk_front->portal_front ()->chunk_next == _chunk_front );

    // Update portals
    _chunk_front->portal_front ()->chunk_next = chunk_new;
    chunk_new->portal_back ()->chunk_next = _chunk_front;
    // Update first chunk reference
    _chunk_front = chunk_new;

    // Update rails chain
    Rail::connect ( rail_new, _rail_front );
    _rail_front = rail_new;
  }
}

/// @brief Allocates a new rail and appends it to the rail chain
/// @arg capacity_n The rail must be capable to allocate a chunk of this size
void
Rails::new_rail_back ()
{
  // New rail and initial chunk
  Rail * rail_new = acquire_rail ();
  Chunk * chunk_new = rail_new->reset_front_chunk ();

  // -- Update last rail and rail pointer(s)
  if ( _rail_back == nullptr ) {
    // First allocated rail
    DEBUG_ASSERT ( _chunk_front == nullptr );
    DEBUG_ASSERT ( _chunk_back == nullptr );
    DEBUG_ASSERT ( _rail_front == nullptr );

    _rail_front = rail_new;
    _rail_back = rail_new;
    _chunk_front = chunk_new;
    _chunk_back = chunk_new;
  } else {
    // This is not the first allocated rail
    DEBUG_ASSERT ( _rail_back->links ().next == nullptr );
    DEBUG_ASSERT ( _chunk_back != nullptr );
    DEBUG_ASSERT ( _chunk_back->size_front != 0 );
    DEBUG_ASSERT ( _chunk_back->size_back == 0 );
    DEBUG_ASSERT ( _chunk_back->portal_back ()->chunk_next == _chunk_back );

    // Update portals
    _chunk_back->portal_back ()->chunk_next = chunk_new;
    chunk_new->portal_front ()->chunk_next = _chunk_back;
    // Update last chunk reference
    _chunk_back = chunk_new;

    // Update rails chain
    Rail::connect ( _rail_back, rail_new );
    _rail_back = rail_new;
  }
}

/// @brief Allocates a new rail
///
/// @arg capacity_n The rail must be capable to allocate a single chunk of this
/// size
Rail *
Rails::acquire_rail ()
{
  ++_rails;
  // Take tile from the pool or create one
  if ( _pool_size != 0 ) {
    return pool_pop ();
  }
  return create_rail ();
}

void
Rails::release_rail ( Rail * rail_n )
{
  DEBUG_ASSERT ( rail_n != nullptr );
  DEBUG_ASSERT ( rail_n->links ().prev == nullptr );
  DEBUG_ASSERT ( rail_n->links ().next == nullptr );
  DEBUG_ASSERT ( _rails != 0 );

  // Push rail to pool or destroy
  if ( _pool_size < _pool_capacity ) {
    pool_push ( rail_n );
  } else {
    destroy_rail ( rail_n );
  }

  // Update statistics
  --_rails;
}

Rail *
Rails::create_rail ()
{
  // Calculate rail size
  size_type buffer_size = meta_size_minimal + _rail_capacity;
  // Allocate
  std::byte * buffer = new std::byte[ buffer_size ];
  // Placement new
  return new ( buffer ) Rail ( buffer, buffer_size );
}

void
Rails::destroy_rail ( Rail * rail_n )
{
  DEBUG_ASSERT ( rail_n != nullptr );
  std::byte * buffer = rail_n->buffer ();
  rail_n->~Rail ();
  delete[] buffer;
}

void
Rails::set_pool_capacity ( size_type capacity_n )
{
  if ( _pool_capacity == capacity_n ) {
    return;
  }

  // Change pool capacity
  _pool_capacity = capacity_n;

  // Create missing rails
  while ( rails_total () < _pool_capacity ) {
    pool_push ( create_rail () );
  }
  // Remove excess rails from the pool
  while ( ( _pool_size != 0 ) && ( rails_total () > _pool_capacity ) ) {
    destroy_rail ( pool_pop () );
  }
}

void
Rails::pool_push ( Rail * rail_n )
{
  ++_pool_size;
  if ( _pool_top ) {
    Rail::connect ( _pool_top, rail_n );
  }
  _pool_top = std::move ( rail_n );
}

Rail *
Rails::pool_pop ()
{
  DEBUG_ASSERT ( _pool_top != nullptr );
  DEBUG_ASSERT ( _pool_size != 0 );
  --_pool_size;
  Rail * res = _pool_top;
  _pool_top = _pool_top->links ().prev;
  Rail::disconnect ( _pool_top, res );
  return res;
}

} // namespace sev::mem::rails

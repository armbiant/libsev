/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "iterator.hpp"

namespace sev::mem::rails
{

template < bool CONST >
void
Iterator_Forward_< CONST >::increment ()
{
  // Jump to next chunk
  if ( _chunk->size_back != 0 ) {
    _chunk = _chunk->chunk_next ();
  }
  // Check if this is an end chunk
  if ( _chunk->size_back == 0 ) {
    // Jump to next rail front chunk
    _chunk = _chunk->portal_back ()->chunk_next;
  }
}

template < bool CONST >
void
Iterator_Forward_< CONST >::increment ( std::size_t delta_n )
{
  for ( ; delta_n != 0; --delta_n ) {
    increment ();
  }
}

template < bool CONST >
void
Iterator_Forward_< CONST >::decrement ()
{
  if ( _chunk->size_front != 0 ) {
    // There is a chunk before
    _chunk = _chunk->chunk_prev ();
  } else {
    // Jump to previous rail back chunk
    _chunk = _chunk->portal_front ()->chunk_next;
    // Jump to previous rail second to last chunk
    if ( _chunk->size_front != 0 ) {
      _chunk = _chunk->chunk_prev ();
    }
  }
}

template < bool CONST >
void
Iterator_Forward_< CONST >::decrement ( std::size_t delta_n )
{
  for ( ; delta_n != 0; --delta_n ) {
    decrement ();
  }
}

template < bool CONST >
void
Iterator_Reverse_< CONST >::increment ()
{
  // Jump to previous chunk
  if ( _chunk->size_front != 0 ) {
    _chunk = _chunk->chunk_prev ();
  }
  // Check if this is a front end chunk
  if ( _chunk->size_front == 0 ) {
    // Jump to previous rail end chunk
    _chunk = _chunk->portal_front ()->chunk_next;
  }
}

template < bool CONST >
void
Iterator_Reverse_< CONST >::increment ( std::size_t delta_n )
{
  for ( ; delta_n != 0; --delta_n ) {
    increment ();
  }
}

template < bool CONST >
void
Iterator_Reverse_< CONST >::decrement ()
{
  if ( _chunk->size_back != 0 ) {
    // There is a chunk before
    _chunk = _chunk->chunk_next ();
  } else {
    // Jump to next rail front chunk
    _chunk = _chunk->portal_back ()->chunk_next;
    // Jump to next rail second chunk
    if ( _chunk->size_back != 0 ) {
      _chunk = _chunk->chunk_next ();
    }
  }
}

template < bool CONST >
void
Iterator_Reverse_< CONST >::decrement ( std::size_t delta_n )
{
  for ( ; delta_n != 0; --delta_n ) {
    decrement ();
  }
}

// -- Instantiation
template class Iterator_Forward_< false >;
template class Iterator_Forward_< true >;
template class Iterator_Reverse_< false >;
template class Iterator_Reverse_< true >;

} // namespace sev::mem::rails

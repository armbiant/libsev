/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "rail.hpp"
#include <sev/assert.hpp>
#include <sev/mem/align.hpp>
#include <sev/mem/rails/chunk.hpp>
#include <sev/mem/rails/portal.hpp>
#include <new>

namespace sev::mem::rails
{

/// @brief On rail initialization this amount of space
///        is used by meta information
static constexpr std::size_t meta_size_initial =
    sizeof ( sev::mem::rails::Rail ) +  //
    sizeof ( sev::mem::rails::Chunk ) + //
    2 * sizeof ( sev::mem::rails::Portal );

} // namespace sev::mem::rails

namespace sev::mem::rails
{

Rail::Rail ( std::byte * buffer_n, std::size_t buffer_size_n )
: _buffer ( buffer_n )
, _buffer_size ( buffer_size_n )
{
  DEBUG_ASSERT ( sev::mem::is_aligned ( arena_begin () ) );
  DEBUG_ASSERT ( sev::mem::is_aligned ( arena_end () ) );
}

Rail::~Rail ()
{
  DEBUG_ASSERT ( links ().next == nullptr );
  DEBUG_ASSERT ( links ().prev == nullptr );
}

inline std::size_t
Rail::initial_free_size () const
{
  // Compute free usable size
  const std::size_t free_size = ( _buffer_size - meta_size_initial );
  // Check free size calculation
  DEBUG_ASSERT ( sev::mem::is_aligned ( free_size ) );
  return free_size;
}

Chunk *
Rail::reset_front_chunk ()
{
  // Memory addresses
  std::byte * const ptr_front = arena_begin ();
  std::byte * const ptr_chunk = ptr_front + sizeof ( Portal );
  std::byte * const ptr_back =
      ptr_front + ( sizeof ( Portal ) + sizeof ( Chunk ) );

  Chunk * const chunk = reinterpret_cast< Chunk * > ( ptr_chunk );
  new ( ptr_front ) Portal ( 0, chunk );
  new ( ptr_chunk ) Chunk ( 0, 0 );
  new ( ptr_back ) Portal ( initial_free_size (), chunk );
  return chunk;
}

Chunk *
Rail::reset_back_chunk ()
{
  // Memory addresses
  std::byte * const ptr_front =
      arena_end () - ( sizeof ( Portal ) * 2 + sizeof ( Chunk ) );
  std::byte * const ptr_chunk = ptr_front + sizeof ( Portal );
  std::byte * const ptr_back =
      ptr_front + ( sizeof ( Portal ) + sizeof ( Chunk ) );

  Chunk * const chunk = reinterpret_cast< Chunk * > ( ptr_chunk );
  new ( ptr_front ) Portal ( initial_free_size (), chunk );
  new ( ptr_chunk ) Chunk ( 0, 0 );
  new ( ptr_back ) Portal ( 0, chunk );
  return chunk;
}

} // namespace sev::mem::rails

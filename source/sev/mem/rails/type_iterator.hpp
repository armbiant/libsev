/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/rails/iterator.hpp>

namespace sev::mem::rails
{

// -- Forward declaration
template < typename T >
class Type_Rails;

/// @brief Forward iterator base class
///
template < typename T, bool REVERSE, bool CONST >
class Type_Iterator_
{
  public:
  // -- Types
  using Iter = Type_Iterator_< T, REVERSE, CONST >;
  using rails_iterator = typename std::conditional<
      REVERSE,
      sev::mem::rails::Iterator_Reverse_< CONST >,
      sev::mem::rails::Iterator_Forward_< CONST > >::type;
  using value_type = typename std::conditional< CONST, const T, T >::type;
  using pointer = value_type *;
  using reference = value_type &;
  using iterator_category = std::bidirectional_iterator_tag;
  using difference_type = std::ptrdiff_t;

  // -- Construction

  /// @brief Constructs an invalid iterator
  constexpr Type_Iterator_ () = default;

  private:
  friend class sev::mem::rails::Type_Rails< T >;

  constexpr Type_Iterator_ ( rails_iterator it_n )
  : _it ( std::move ( it_n ) )
  {
  }

  public:
  // -- Setup

  /// @brief Invalidates the iterator
  ///
  void
  reset ()
  {
    _it.reset ();
  }

  // -- Chunk

  constexpr bool
  is_valid () const
  {
    return _it.is_valid ();
  }

  // -- Item access

  pointer
  item () const
  {
    return reinterpret_cast< pointer > ( _it.data () );
  }

  // -- Cast operators

  pointer
  operator-> () const
  {
    return item ();
  }

  reference
  operator* () const
  {
    return *item ();
  }

  // -- Increment operators

  Iter &
  operator++ ()
  {
    increment ();
    return *this;
  }

  Iter
  operator++ ( int )
  {
    Iter res ( *this );
    increment ();
    return res;
  }

  Iter &
  operator+= ( std::size_t delta_n )
  {
    increment ( delta_n );
    return *this;
  }

  // -- Decrement operators

  Iter &
  operator-- ()
  {
    decrement ();
    return *this;
  }

  Iter
  operator-- ( int )
  {
    Iter res ( *this );
    decrement ();
    return res;
  }

  Iter &
  operator-= ( std::size_t delta_n )
  {
    decrement ( delta_n );
    return *this;
  }

  // -- Comparison operators

  bool
  operator== ( const Iter & it_n ) const
  {
    return ( _it == it_n._it );
  }

  bool
  operator!= ( const Iter & it_n ) const
  {
    return ( _it != it_n._it );
  }

  // -- Utility

  void
  increment ()
  {
    _it.increment ();
  }

  void
  increment ( std::size_t delta_n )
  {
    _it.increment ( delta_n );
  }

  void
  decrement ()
  {
    _it.decrement ();
  }

  void
  decrement ( std::size_t delta_n )
  {
    _it.decrement ( delta_n );
  }

  protected:
  // -- Attributes
  rails_iterator _it;
};

// -- Specializations

template < typename T >
using Type_Iterator = Type_Iterator_< T, false, false >;

template < typename T >
using Type_Const_Iterator = Type_Iterator_< T, false, true >;

template < typename T >
using Type_Reverse_Iterator = Type_Iterator_< T, true, false >;

template < typename T >
using Type_Const_Reverse_Iterator = Type_Iterator_< T, true, true >;

} // namespace sev::mem::rails

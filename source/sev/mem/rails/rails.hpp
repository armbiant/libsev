/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/align.hpp>
#include <sev/mem/rails/chunk.hpp>
#include <sev/mem/rails/iterator.hpp>
#include <sev/mem/view.hpp>
#include <cstddef>
#include <new>
#include <type_traits>
#include <utility>

namespace sev::mem::rails
{

// -- Forward declaration
class Rail;

/// @brief Double ended sequence container for raw data chunks of random length
///
class Rails
{
  public:
  // -- Types
  // - Iterators
  using size_type = std::size_t;
  using iterator = Iterator;
  using reverse_iterator = Reverse_Iterator;
  using const_iterator = Const_Iterator;
  using const_reverse_iterator = Const_Reverse_Iterator;
  // - Data reference
  using Chunk_View = sev::mem::View< std::byte >;
  using Const_Chunk_View = sev::mem::View< const std::byte >;

  // -- Default sizes
  static constexpr size_type _rail_capacity_minimum = 256;
  static constexpr size_type _rail_capacity_default = 4096;

  // -- Construction

  Rails ();

  /// @brief Copy constructor
  Rails ( const Rails & rails_n );

  /// @brief Move constructor
  Rails ( Rails && rails_n );

  ~Rails ();

  // -- Assignment

  void
  assign_copy ( const Rails & rails_n );

  void
  assign_move ( Rails && rails_n );

  Rails &
  operator= ( const Rails & rails_n )
  {
    assign_copy ( rails_n );
    return *this;
  }

  Rails &
  operator= ( Rails && rails_n )
  {
    assign_move ( std::move ( rails_n ) );
    return *this;
  }

  // -- Setup

  /// @brief Clears and deallocates all resources
  ///
  void
  clear ();

  // -- Rail default capacity

  /// @brief A new rails must be capable of allocating a single
  ///        chunk of this size.
  std::size_t
  rail_capacity () const
  {
    return _rail_capacity;
  }

  /// @brief Sets the default rail size for new rails
  ///
  void
  set_rail_capacity ( size_type size_n );

  // -- Allocated chunk statistics

  /// @brief Total number of allocated chunks
  ///
  size_type
  size () const
  {
    return _size;
  }

  /// @brief ( size() == 0 )
  ///
  bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Same as is_empty()
  bool
  empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Number of rails in use
  ///
  size_type
  rails () const
  {
    return _rails;
  }

  // -- Front chunk access

  std::byte *
  front ()
  {
    return _chunk_front->data_back ();
  }

  const std::byte *
  front () const
  {
    return _chunk_front->data_back ();
  }

  Chunk_View
  front_view ()
  {
    return Chunk_View ( _chunk_front->data_back (), _chunk_front->size_back );
  }

  Const_Chunk_View
  front_view () const
  {
    return Const_Chunk_View ( _chunk_front->data_back (),
                              _chunk_front->size_back );
  }

  // -- Back chunk access

  std::byte *
  back ()
  {
    return _chunk_back->data_front ();
  }

  const std::byte *
  back () const
  {
    return _chunk_back->data_front ();
  }

  Chunk_View
  back_view ()
  {
    return Chunk_View ( _chunk_back->data_front (), _chunk_back->size_front );
  }

  Const_Chunk_View
  back_view () const
  {
    return Const_Chunk_View ( _chunk_back->data_front (),
                              _chunk_back->size_front );
  }

  // -- Chunk allocation and removal at the front

  /// @brief Allocates a new chunk at the front that is large enough to hold
  ///        at least @a size_n bytes.
  ///
  /// @a must be lesser or equal to rail_capacity().
  /// The allocated chunk may by larger actually to
  /// comply with the alignment requirements.
  ///
  /// @return Non nullptr data chunk pointer.
  std::byte *
  allocate_front ( size_type size_n );

  /// @brief Allocates a new chunk of @a block_size_n bytes at the front.
  ///
  /// @a block_size_n must be not 0, block size aligned and lesser or
  /// equal to rail_capacity().
  ///
  /// @return Non nullptr data chunk pointer.
  std::byte *
  allocate_block_front ( std::size_t block_size_n );

  /// @brief Allocate and construct a new instance of type T
  ///
  /// T must be trivially copyable and trivially destructible.
  template < typename T, typename... Args >
  T &
  emplace_front ( Args &&... args_n )
  {
    static_assert ( std::is_trivially_copyable< T >::value );
    static_assert ( std::is_trivially_destructible< T >::value );
    static_assert ( !std::is_polymorphic< T >::value );

    auto * raw = allocate_block_front ( sev::mem::aligned_up ( sizeof ( T ) ) );
    try {
      // Construct class
      return *new ( raw ) T ( std::forward< Args > ( args_n )... );
    }
    catch ( ... ) {
      // Release allocated chunk and rethrow
      pop_front ();
      throw;
    }
  }

  /// @brief Allocate a chunk and copy the given data to it
  void
  push_front ( Const_Chunk_View view_n );

  /// @brief Pop the chunk at the front.
  void
  pop_front ();

  // -- Chunk allocation and removal at the back

  /// @brief Allocates a new chunk at the end that is large enough to hold
  ///        at least @a size_n bytes.
  ///
  /// @a must be lesser or equal to rail_capacity().
  /// The allocated chunk may by larger actually to
  /// comply with the alignment requirements.
  ///
  /// @return Non nullptr data chunk pointer.
  std::byte *
  allocate_back ( std::size_t size_n );

  /// @brief Allocates a new chunk of @a block_size_n bytes at the end.
  ///
  /// @a block_size_n must be not 0, block size aligned and lesser or
  /// equal to rail_capacity().
  ///
  /// @return Non nullptr data chunk pointer.
  std::byte *
  allocate_block_back ( std::size_t block_size_n );

  /// @brief Allocate and construct a new instance of type T
  ///
  /// T must be trivially copyable and trivially destructible.
  template < typename T, typename... Args >
  T &
  emplace_back ( Args &&... args_n )
  {
    static_assert ( std::is_trivially_copyable< T >::value );
    static_assert ( std::is_trivially_destructible< T >::value );
    static_assert ( !std::is_polymorphic< T >::value );

    auto * raw = allocate_block_back ( sev::mem::aligned_up ( sizeof ( T ) ) );
    try {
      // Construct class
      return *new ( raw ) T ( std::forward< Args > ( args_n )... );
    }
    catch ( ... ) {
      // Release allocated chunk and rethrow
      pop_back ();
      throw;
    }
  }

  /// @brief Allocate a chunk and copy the given data to it
  void
  push_back ( Const_Chunk_View view_n );

  /// @brief Pop the chunk at the back.
  void
  pop_back ();

  // -- Iteration

  iterator
  begin ()
  {
    return iterator ( _chunk_front );
  }

  const_iterator
  begin () const
  {
    return cbegin ();
  }

  const_iterator
  cbegin () const
  {
    return const_iterator ( _chunk_front );
  }

  iterator
  end ()
  {
    return iterator ( _chunk_back );
  }

  const_iterator
  end () const
  {
    return cend ();
  }

  const_iterator
  cend () const
  {
    return const_iterator ( _chunk_back );
  }

  reverse_iterator
  rbegin ()
  {
    return reverse_iterator ( _chunk_back );
  }

  const_reverse_iterator
  rbegin () const
  {
    return crbegin ();
  }

  const_reverse_iterator
  crbegin () const
  {
    return const_reverse_iterator ( _chunk_back );
  }

  reverse_iterator
  rend ()
  {
    return reverse_iterator ( _chunk_front );
  }

  const_reverse_iterator
  rend () const
  {
    return crend ();
  }

  const_reverse_iterator
  crend () const
  {
    return const_reverse_iterator ( _chunk_front );
  }

  // -- Rail pool

  /// @brief Keep at least this number of rails allocated.
  ///
  /// Unused rails are kept in a pool.
  size_type
  pool_capacity () const
  {
    return _pool_capacity;
  }

  void
  set_pool_capacity ( size_type capacity_n );

  /// @brief Number of unused rails stored in the pool.
  size_type
  pool_size () const
  {
    return _pool_size;
  }

  // -- Rail statistics

  /// @brief Total number of allocated rails (active and pooled)
  size_type
  rails_total () const
  {
    return ( _rails + _pool_size );
  }

  private:
  // -- Utility: Rail

  void
  new_rail_front ();

  void
  new_rail_back ();

  Rail *
  acquire_rail ();

  void
  release_rail ( Rail * rail_n );

  Rail *
  create_rail ();

  void
  destroy_rail ( Rail * rail_n );

  void
  pool_push ( Rail * rail_n );

  Rail *
  pool_pop ();

  private:
  // -- Attributes
  Chunk * _chunk_front = nullptr;
  Chunk * _chunk_back = nullptr;
  Rail * _rail_front = nullptr;
  Rail * _rail_back = nullptr;
  size_type _size = 0;
  size_type _rails = 0;
  size_type _rail_capacity = 0;
  size_type _pool_capacity = 0;
  size_type _pool_size = 0;
  Rail * _pool_top = nullptr;
};

} // namespace sev::mem::rails

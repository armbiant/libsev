/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <cstddef>
#include <cstdint>

namespace sev::mem::rails
{

// -- Forward declaration
class Chunk;

/// @brief Two sided type and length information node
///
class Rail
{
  public:
  // -- Types
  struct Links
  {
    Rail * prev = nullptr;
    Rail * next = nullptr;
  };

  // -- Construction

  Rail ( std::byte * buffer_n, std::size_t buffer_size_n );

  ~Rail ();

  // -- Rail connection

  const Links &
  links () const
  {
    return _links;
  }

  static void
  connect ( Rail * prev_n, Rail * next_n )
  {
    // Check if the references are valid
    DEBUG_ASSERT ( prev_n != nullptr );
    DEBUG_ASSERT ( next_n != nullptr );

    // Check if the rails are connected already
    DEBUG_ASSERT ( prev_n->links ().next == nullptr );
    DEBUG_ASSERT ( next_n->links ().prev == nullptr );

    prev_n->_links.next = next_n;
    next_n->_links.prev = prev_n;
  }

  static void
  disconnect ( Rail * prev_n, Rail * next_n )
  {
    if ( ( prev_n == nullptr ) || ( next_n == nullptr ) ) {
      return;
    }
    // Check if the rails are even connected
    DEBUG_ASSERT ( prev_n->_links.next == next_n );
    DEBUG_ASSERT ( next_n->_links.prev == prev_n );

    prev_n->_links.next = nullptr;
    next_n->_links.prev = nullptr;
  }

  // -- Chunk setup

  Chunk *
  reset_front_chunk ();

  Chunk *
  reset_back_chunk ();

  // -- Useable memory arena

  std::byte *
  arena_begin () const
  {
    return const_cast< std::byte * > (
        reinterpret_cast< const std::byte * > ( this ) + sizeof ( Rail ) );
  }

  std::byte *
  arena_end () const
  {
    return const_cast< std::byte * > (
        reinterpret_cast< const std::byte * > ( this ) + _buffer_size );
  }

  // -- Buffer

  std::byte *
  buffer () const
  {
    return _buffer;
  }

  std::size_t
  buffer_size () const
  {
    return _buffer_size;
  }

  // -- Utility

  private:
  std::size_t
  initial_free_size () const;

  private:
  // -- Attributes
  std::byte * _buffer = nullptr;
  std::size_t _buffer_size = 0;
  Links _links;
};

} // namespace sev::mem::rails

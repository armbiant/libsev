/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>
#include <cstdint>
#include <iterator>
#include <memory>

namespace sev::mem
{

/// @brief Dynamically allocated fixed size byte array
///
class Byte_Array
{
  public:
  // -- Types
  typedef std::uint8_t value_type;
  typedef std::uint8_t * iterator;
  typedef const std::uint8_t * const_iterator;
  typedef std::reverse_iterator< iterator > reverse_iterator;
  typedef std::reverse_iterator< const_iterator > const_reverse_iterator;
  typedef std::size_t size_type;

  public:
  // -- Constructors

  /// @brief Doesn't allocate anything
  ///
  Byte_Array ();

  /// @brief Sets the mallocator and allocates a chunk of memory
  ///
  Byte_Array ( size_type size_n );

  /// @brief Disable default copy constructor
  ///
  Byte_Array ( const sev::mem::Byte_Array & array_n );

  /// @brief Move constructor
  ///
  Byte_Array ( sev::mem::Byte_Array && array_n );

  ~Byte_Array ();

  /// @brief Deallocates the array.
  void
  clear ();

  /// @brief Fills the array with a single value
  void
  fill ( std::uint8_t value_n );

  /// @brief Size of the array in bytes
  ///
  size_type
  size () const
  {
    return _size;
  }

  /// @brief Resizes the array and copies any as much old data to the new
  /// location as possible.
  ///
  /// @return True if the resizing was successful.
  bool
  resize ( size_type size_n );

  /// @brief Resizes the array but avoids copying any old data to the new
  /// location.
  ///
  /// @return True if the resizing was successful.
  bool
  clear_resize ( size_type size_n );

  std::uint8_t *
  data ()
  {
    return _data;
  }

  const std::uint8_t *
  data () const
  {
    return _data;
  }

  // Iterators

  iterator
  begin ()
  {
    return data ();
  }

  const_iterator
  begin () const
  {
    return data ();
  }

  const_iterator
  cbegin () const
  {
    return data ();
  }

  std::uint8_t *
  end ()
  {
    return ( begin () + size () );
  }

  const_iterator
  end () const
  {
    return ( begin () + size () );
  }

  const_iterator
  cend () const
  {
    return ( begin () + size () );
  }

  // Operators

  std::uint8_t &
  operator[] ( size_type index_n )
  {
    return _data[ index_n ];
  }

  std::uint8_t
  operator[] ( size_type index_n ) const
  {
    return _data[ index_n ];
  }

  sev::mem::Byte_Array &
  operator= ( const sev::mem::Byte_Array & array_n );

  sev::mem::Byte_Array &
  operator= ( sev::mem::Byte_Array && array_n );

  bool
  operator== ( const sev::mem::Byte_Array & array_n ) const;

  bool
  operator!= ( const sev::mem::Byte_Array & array_n ) const;

  private:
  std::uint8_t * _data;
  size_type _size;
  std::allocator< std::uint8_t > _allocator;
};

} // namespace sev::mem

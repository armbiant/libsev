/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "byte_array.hpp"
#include <sev/math/numbers.hpp>
#include <algorithm>
#include <utility>

namespace sev::mem
{

Byte_Array::Byte_Array ()
: _data ( nullptr )
, _size ( 0 )
{
}

Byte_Array::Byte_Array ( size_type size_n )
: _data ( nullptr )
, _size ( 0 )
{
  if ( size_n != 0 ) {
    std::uint8_t * items_new ( _allocator.allocate ( size_n ) );
    if ( items_new != nullptr ) {
      // Assign new data
      _data = items_new;
      _size = size_n;
    }
  }
}

Byte_Array::Byte_Array ( const sev::mem::Byte_Array & array_n )
: _data ( 0 )
, _size ( 0 )
{
  const size_type size_new ( array_n.size () );
  if ( size_new != 0 ) {
    std::uint8_t * items_new ( _allocator.allocate ( size_new ) );
    if ( items_new != nullptr ) {
      // Copy data
      std::copy ( array_n.begin (), array_n.end (), items_new );
      // Assign new data
      _data = items_new;
      _size = size_new;
    }
  }
}

Byte_Array::Byte_Array ( sev::mem::Byte_Array && array_n )
: _data ( array_n._data )
, _size ( array_n._size )
, _allocator ( std::move ( array_n._allocator ) )
{
  array_n._data = 0;
  array_n._size = 0;
}

Byte_Array::~Byte_Array ()
{
  clear ();
}

void
Byte_Array::clear ()
{
  if ( _data != nullptr ) {
    _allocator.deallocate ( _data, _size );
    _data = 0;
    _size = 0;
  }
}

void
Byte_Array::fill ( std::uint8_t value_n )
{
  std::fill ( begin (), end (), value_n );
}

bool
Byte_Array::resize ( size_type size_n )
{
  if ( _size != size_n ) {
    if ( size_n != 0 ) {
      std::uint8_t * items_new ( _allocator.allocate ( size_n ) );
      if ( items_new != nullptr ) {
        if ( _data != nullptr ) {
          // Copy old data to new location
          {
            size_type num_copy ( _size );
            math::assign_smaller ( num_copy, size_n );
            std::copy ( _data, ( _data + num_copy ), items_new );
          }
          // Deallocate old data block
          _allocator.deallocate ( _data, _size );
        }
        // Assign new storage / size
        _data = items_new;
        _size = size_n;
      } else {
        return false;
      }
    } else {
      clear ();
    }
  }
  return true;
}

bool
Byte_Array::clear_resize ( size_type size_n )
{
  if ( _size != size_n ) {
    // Deallocate old data block
    if ( _data != nullptr ) {
      // If there is data there must be an allocator.
      _allocator.deallocate ( _data, _size );
      _data = 0;
      _size = 0;
    }
    if ( size_n != 0 ) {
      std::uint8_t * items_new ( _allocator.allocate ( size_n ) );
      if ( items_new != nullptr ) {
        _data = items_new;
        _size = size_n;
      } else {
        return false;
      }
    }
  }
  return true;
}

mem::Byte_Array &
Byte_Array::operator= ( const sev::mem::Byte_Array & array_n )
{
  clear_resize ( array_n.size () );
  if ( _data != 0 ) {
    std::copy ( array_n.begin (), array_n.end (), begin () );
  }
  return *this;
}

mem::Byte_Array &
Byte_Array::operator= ( sev::mem::Byte_Array && array_n )
{
  clear ();
  // Assign this
  {
    _data = array_n._data;
    _size = array_n._size;
    _allocator = std::move ( array_n._allocator );
  }
  // Clear source
  {
    array_n._data = 0;
    array_n._size = 0;
  }
  return *this;
}

bool
Byte_Array::operator== ( const sev::mem::Byte_Array & array_n ) const
{
  bool res ( false );
  if ( size () == array_n.size () ) {
    res = std::equal ( array_n.begin (), array_n.end (), begin () );
  }
  return res;
}

bool
Byte_Array::operator!= ( const sev::mem::Byte_Array & array_n ) const
{
  bool res ( true );
  if ( size () == array_n.size () ) {
    res = !std::equal ( array_n.begin (), array_n.end (), begin () );
  }
  return res;
}

} // namespace sev::mem

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <utility>

namespace sev::mem
{

/// @brief Key value pair
///
template < class KEY, class VALUE >
class Key_Value
{
  public:
  // -- Construction

  Key_Value () = default;

  Key_Value ( const KEY & key_n, const VALUE & value_n )
  : _key ( key_n )
  , _value ( value_n )
  {
  }

  Key_Value ( KEY && key_n, VALUE && value_n )
  : _key ( std::move ( key_n ) )
  , _value ( std::move ( value_n ) )
  {
  }

  ~Key_Value () = default;

  // -- Accessors

  const KEY &
  key () const
  {
    return _key;
  }

  KEY &
  key_ref ()
  {
    return _key;
  }

  void
  set_key ( const KEY & key_n )
  {
    _key = key_n;
  }

  void
  set_key ( KEY && key_n )
  {
    _key = static_cast< KEY && > ( key_n );
  }

  const VALUE &
  value () const
  {
    return _value;
  }

  VALUE &
  value_ref ()
  {
    return _value;
  }

  void
  set_value ( const VALUE & value_n )
  {
    _value = value_n;
  }

  void
  set_value ( VALUE && value_n )
  {
    _value = static_cast< VALUE && > ( value_n );
  }

  private:
  // -- Attributes
  KEY _key;
  VALUE _value;
};

} // namespace sev::mem

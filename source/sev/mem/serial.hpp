/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::mem
{

// -- 16 Bit

inline std::uint16_t
read_uint16_8le ( const std::uint8_t * vals8_n )
{
  return ( ( std::uint16_t ( vals8_n[ 0 ] ) << 0x00 ) |
           ( std::uint16_t ( vals8_n[ 1 ] ) << 0x08 ) );
}

inline void
set_8le_uint16 ( std::uint8_t * vals8_n, std::uint16_t val16_n )
{
  vals8_n[ 0 ] = val16_n >> 0x00;
  vals8_n[ 1 ] = val16_n >> 0x08;
}

// -- 24 Bit

inline std::uint32_t
read_uint24_8le ( const std::uint8_t * vals8_n )
{
  return ( ( std::uint32_t ( vals8_n[ 0 ] ) << 0x00 ) |
           ( std::uint32_t ( vals8_n[ 1 ] ) << 0x08 ) |
           ( std::uint32_t ( vals8_n[ 2 ] ) << 0x10 ) );
}

inline void
set_8le_uint24 ( std::uint8_t * vals8_n, std::uint32_t val32_n )
{
  vals8_n[ 0 ] = val32_n >> 0x00;
  vals8_n[ 1 ] = val32_n >> 0x08;
  vals8_n[ 2 ] = val32_n >> 0x10;
}

// -- 32 Bit

inline std::uint32_t
read_uint32_8le ( const std::uint8_t * vals8_n )
{
  return ( ( std::uint32_t ( vals8_n[ 0 ] ) << 0x00 ) |
           ( std::uint32_t ( vals8_n[ 1 ] ) << 0x08 ) |
           ( std::uint32_t ( vals8_n[ 2 ] ) << 0x10 ) |
           ( std::uint32_t ( vals8_n[ 3 ] ) << 0x18 ) );
}

inline void
set_8le_uint32 ( std::uint8_t * vals8_n, std::uint32_t val32_n )
{
  vals8_n[ 0 ] = val32_n >> 0x00;
  vals8_n[ 1 ] = val32_n >> 0x08;
  vals8_n[ 2 ] = val32_n >> 0x10;
  vals8_n[ 3 ] = val32_n >> 0x18;
}

// -- 64 Bit

inline std::uint64_t
read_uint64_8le ( const std::uint8_t * vals8_n )
{
  return ( ( std::uint64_t ( vals8_n[ 0 ] ) << 0x00 ) |
           ( std::uint64_t ( vals8_n[ 1 ] ) << 0x08 ) |
           ( std::uint64_t ( vals8_n[ 2 ] ) << 0x10 ) |
           ( std::uint64_t ( vals8_n[ 3 ] ) << 0x18 ) |
           ( std::uint64_t ( vals8_n[ 4 ] ) << 0x20 ) |
           ( std::uint64_t ( vals8_n[ 5 ] ) << 0x28 ) |
           ( std::uint64_t ( vals8_n[ 6 ] ) << 0x30 ) |
           ( std::uint64_t ( vals8_n[ 7 ] ) << 0x38 ) );
}

inline void
set_8le_uint64 ( std::uint8_t * vals8_n, std::uint64_t val64_n )
{
  vals8_n[ 0 ] = val64_n >> 0x00;
  vals8_n[ 1 ] = val64_n >> 0x08;
  vals8_n[ 2 ] = val64_n >> 0x10;
  vals8_n[ 3 ] = val64_n >> 0x18;
  vals8_n[ 4 ] = val64_n >> 0x20;
  vals8_n[ 5 ] = val64_n >> 0x28;
  vals8_n[ 6 ] = val64_n >> 0x30;
  vals8_n[ 7 ] = val64_n >> 0x38;
}

} // namespace sev::mem

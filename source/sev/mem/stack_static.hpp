/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/mem/view.hpp>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <new>
#include <type_traits>
#include <utility>

namespace sev::mem
{

///
/// @brief A static sized stack
///
template < class T, std::size_t CAP >
class Stack_Static
{
  // Public types
  public:
  typedef T value_type;
  typedef T * iterator;
  typedef const T * const_iterator;
  typedef std::reverse_iterator< iterator > reverse_iterator;
  typedef std::reverse_iterator< const_iterator > const_reverse_iterator;
  typedef std::size_t size_type;

  typedef sev::mem::View< T > Span;
  typedef sev::mem::View< const T > Span_Const;

  // Public methods
  public:
  /// @brief Empty stack
  Stack_Static ();

  /// @brief Deep copy constructor
  Stack_Static ( const sev::mem::Stack_Static< T, CAP > & stack_n );

  /// @brief Move constructor
  ///
  Stack_Static ( sev::mem::Stack_Static< T, CAP > && stack_n );

  ~Stack_Static ();

  /// @brief Removes all items but does not change the capacity()
  ///
  void
  clear ();

  /// @brief Assigns all items to item_n
  ///
  void
  fill ( const T & value_n );

  constexpr static size_type
  capacity ()
  {
    return CAP;
  }

  // -- Size

  size_type
  size () const
  {
    return _size;
  }

  size_type
  capacity_free () const
  {
    return ( CAP - _size );
  }

  bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Same as is_empty()
  bool
  empty () const
  {
    return ( _size == 0 );
  }

  bool
  is_full () const
  {
    return ( _size == capacity () );
  }

  // -- Iterators

  iterator
  begin ()
  {
    return ( &items_pointer ()[ 0 ] );
  }

  const_iterator
  begin () const
  {
    return ( &items_pointer ()[ 0 ] );
  }

  const_iterator
  cbegin () const
  {
    return ( &items_pointer ()[ 0 ] );
  }

  iterator
  end ()
  {
    return ( &items_pointer ()[ _size ] );
  }

  const_iterator
  end () const
  {
    return ( &items_pointer ()[ _size ] );
  }

  const_iterator
  cend () const
  {
    return ( &items_pointer ()[ _size ] );
  }

  reverse_iterator
  rbegin ()
  {
    return reverse_iterator ( end () );
  }

  const_reverse_iterator
  rbegin () const
  {
    return const_reverse_iterator ( cend () );
  }

  const_reverse_iterator
  crbegin () const
  {
    return const_reverse_iterator ( cend () );
  }

  reverse_iterator
  rend ()
  {
    return reverse_iterator ( begin () );
  }

  const_reverse_iterator
  rend () const
  {
    return const_reverse_iterator ( begin () );
  }

  const_reverse_iterator
  crend () const
  {
    return const_reverse_iterator ( cbegin () );
  }

  // -- Item access

  T &
  item ( size_type index_n )
  {
    return items_pointer ()[ index_n ];
  }

  const T &
  item ( size_type index_n ) const
  {
    return items_pointer ()[ index_n ];
  }

  /// @brief Stack bottom element
  T &
  bottom ()
  {
    return *items_pointer ();
  }

  /// @brief Stack bottom element
  const T &
  bottom () const
  {
    return *items_pointer ();
  }

  /// @brief Same as bottom()
  T &
  front ()
  {
    return *items_pointer ();
  }

  /// @brief Same as bottom()
  const T &
  front () const
  {
    return *items_pointer ();
  }

  T &
  top ()
  {
    return items_pointer ()[ _size - 1 ];
  }

  const T &
  top () const
  {
    return items_pointer ()[ _size - 1 ];
  }

  /// @brief Same as top()
  T &
  back ()
  {
    return items_pointer ()[ _size - 1 ];
  }

  /// @brief Same as top()
  const T &
  back () const
  {
    return items_pointer ()[ _size - 1 ];
  }

  // -- Pushing

  void
  push_not_full ( const T & item_n );

  void
  push_not_full ( T && item_n );

  template < typename... Args >
  void
  emplace_not_full ( Args &&... args_n );

  // -- Popping

  void
  pop_not_empty ();

  void
  pop_not_empty ( T * output_n );

  void
  pop_num_not_empty ( size_type num_n );

  // -- Manipulation

  void
  acquire_buffer ( Span & span_n );

  void
  acquire_buffer ( Span_Const & span_n ) const;

  void
  acquire_const_buffer ( Span_Const & span_n ) const;

  void
  acquire_buffer_free ( Span & span_n );

  /// @brief Use with care
  void
  set_size_count ( size_type size_n );

  /// @brief Use with care
  void
  increment_size_count ( size_type delta_n );

  /// @brief Use with care
  void
  decrement_size_count ( size_type delta_n );

  size_type
  index_of ( const T & match_item_n ) const;

  size_type
  index_of ( const T & match_item_n, size_type begin_at_n ) const;

  /// @brief Removes the item a index_n and moves all elements above down by
  /// one
  ///
  /// @return False if the index was invalid
  bool
  remove_at ( size_type index_n );

  /// @brief Removes the first item that matches (searching bottom up)
  ///
  /// @return True if an item was removed
  bool
  remove_first_match ( const T & match_value_n );

  /// @brief Removes all item that match
  ///
  /// @return Number of items removed
  size_type
  remove_all_matches ( const T & match_value_n );

  /// @return The iterator after the removed item or end()
  ///
  iterator
  erase ( iterator it_n );

  // -- Item access Operators

  T &
  operator[] ( size_type index_n )
  {
    return items_pointer ()[ index_n ];
  }

  const T &
  operator[] ( size_type index_n ) const
  {
    return items_pointer ()[ index_n ];
  }

  // -- Assignment operators

  sev::mem::Stack_Static< T, CAP > &
  operator= ( const sev::mem::Stack_Static< T, CAP > & stack_n )
  {
    copy_assign ( stack_n );
    return *this;
  }

  sev::mem::Stack_Static< T, CAP > &
  operator= ( sev::mem::Stack_Static< T, CAP > && stack_n )
  {
    move_assign ( std::move ( stack_n ) );
    return *this;
  }

  // Protected methods
  protected:
  void
  copy_assign ( const sev::mem::Stack_Static< T, CAP > & stack_n );

  void
  move_assign ( sev::mem::Stack_Static< T, CAP > && stack_n );

  // Private methods
  private:
  T *
  items_pointer ()
  {
    return reinterpret_cast< T * > ( &_items[ 0 ] );
  }

  const T *
  items_pointer () const
  {
    return reinterpret_cast< const T * > ( &_items[ 0 ] );
  }

  void
  destruct_reverse ();

  void
  destruct_reverse_num ( size_type num_n );

  // Private attributes
  private:
  size_type _size;
  alignas ( alignof ( T ) ) std::uint8_t _items[ sizeof ( T ) * CAP ];
};

template < class T, std::size_t CAP >
inline Stack_Static< T, CAP >::Stack_Static ()
: _size ( 0 )
{
}

template < class T, std::size_t CAP >
Stack_Static< T, CAP >::Stack_Static (
    const sev::mem::Stack_Static< T, CAP > & stack_n )
: _size ( stack_n._size )
{
  // Copy items using new
  if ( _size != 0 ) {
    T * it_new ( items_pointer () );
    const_iterator it_cur ( stack_n.begin () );
    const_iterator it_end ( stack_n.end () );
    do {
      new ( it_new ) T ( *it_cur );
      ++it_new;
      ++it_cur;
    } while ( it_cur != it_end );
  }
}

template < class T, std::size_t CAP >
Stack_Static< T, CAP >::Stack_Static (
    sev::mem::Stack_Static< T, CAP > && stack_n )
: _size ( 0 )
{
  if ( stack_n._size != 0 ) {
    // Update size
    _size = stack_n._size;
    // Move items over here using new
    {
      T * it_new ( items_pointer () );
      iterator it_cur ( stack_n.begin () );
      iterator it_end ( stack_n.end () );
      do {
        new ( it_new ) T ( std::move ( *it_cur ) );
        // Destruct source item
        it_cur->~T ();
        ++it_new;
        ++it_cur;
      } while ( it_cur != it_end );
    }
    // Reset source stack size
    stack_n._size = 0;
  }
}

template < class T, std::size_t CAP >
inline Stack_Static< T, CAP >::~Stack_Static ()
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse ();
  }
}

template < class T, std::size_t CAP >
void
Stack_Static< T, CAP >::copy_assign (
    const sev::mem::Stack_Static< T, CAP > & stack_n )
{
  clear ();
  if ( stack_n._size != 0 ) {
    // Update size
    _size = stack_n.size ();
    // Copy items
    {
      T * it_new ( items_pointer () );
      const_iterator it_cur ( stack_n.begin () );
      const_iterator it_end ( stack_n.end () );
      do {
        new ( it_new ) T ( *it_cur );
        ++it_new;
        ++it_cur;
      } while ( it_cur != it_end );
    }
  }
}

template < class T, std::size_t CAP >
void
Stack_Static< T, CAP >::move_assign (
    sev::mem::Stack_Static< T, CAP > && stack_n )
{
  clear ();
  if ( stack_n._size != 0 ) {
    // Update size
    _size = stack_n._size;
    // Move items over here using new
    {
      T * it_new ( items_pointer () );
      iterator it_cur ( stack_n.begin () );
      iterator it_end ( stack_n.end () );
      do {
        new ( it_new ) T ( std::move ( *it_cur ) );
        // Destruct source item
        it_cur->~T ();
        ++it_new;
        ++it_cur;
      } while ( it_cur != it_end );
    }
    // Reset source stack size
    stack_n._size = 0;
  }
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::clear ()
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse ();
  }
  _size = 0;
}

template < class T, std::size_t CAP >
void
Stack_Static< T, CAP >::destruct_reverse ()
{
  if ( _size != 0 ) {
    iterator it_cur ( end () );
    iterator it_last ( begin () );
    do {
      --it_cur;
      it_cur->~T ();
    } while ( it_cur != it_last );
  }
}

template < class T, std::size_t CAP >
void
Stack_Static< T, CAP >::destruct_reverse_num ( size_type num_n )
{
  iterator it_cur ( end () );
  iterator it_last ( it_cur - num_n );
  while ( it_cur != it_last ) {
    --it_cur;
    it_cur->~T ();
  }
}

template < class T, std::size_t CAP >
void
Stack_Static< T, CAP >::fill ( const T & value_n )
{
  for ( auto & item : *this ) {
    item = value_n;
  }
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::push_not_full ( const T & item_n )
{
  DEBUG_ASSERT ( !is_full () );
  new ( ( items_pointer () + _size ) ) T ( item_n );
  ++_size;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::push_not_full ( T && item_n )
{
  new ( ( items_pointer () + _size ) ) T ( std::move ( item_n ) );
  ++_size;
}

template < class T, std::size_t CAP >
template < typename... Args >
inline void
Stack_Static< T, CAP >::emplace_not_full ( Args &&... args_n )
{
  new ( ( items_pointer () + _size ) ) T ( std::forward< Args > ( args_n )... );
  ++_size;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::pop_not_empty ()
{
  top ().~T ();
  --_size;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::pop_not_empty ( T * output_n )
{
  T & bitem ( top () );
  *output_n = std::move ( bitem );
  bitem.~T ();
  --_size;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::pop_num_not_empty ( size_type num_n )
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse_num ( num_n );
  }
  _size -= num_n;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::acquire_buffer ( Span & span_n )
{
  span_n.reset ( items_pointer (), _size );
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::acquire_buffer ( Span_Const & span_n ) const
{
  span_n.reset ( items_pointer (), _size );
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::acquire_const_buffer ( Span_Const & span_n ) const
{
  span_n.reset ( items_pointer (), _size );
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::acquire_buffer_free ( Span & span_n )
{
  span_n.reset ( ( items_pointer () + _size ), capacity_free () );
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::set_size_count ( size_type size_n )
{
  DEBUG_ASSERT ( size_n <= CAP );
  _size = size_n;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::increment_size_count ( size_type delta_n )
{
  DEBUG_ASSERT ( ( _size + delta_n ) <= CAP );
  _size += delta_n;
}

template < class T, std::size_t CAP >
inline void
Stack_Static< T, CAP >::decrement_size_count ( size_type delta_n )
{
  DEBUG_ASSERT ( _size >= delta_n );
  _size -= delta_n;
}

template < class T, std::size_t CAP >
typename Stack_Static< T, CAP >::size_type
Stack_Static< T, CAP >::index_of ( const T & match_item_n ) const
{
  size_type res ( _size );
  {
    const_iterator it_begin ( cbegin () );
    const_iterator itc ( it_begin );
    const_iterator it_end ( it_begin + _size );
    for ( ; itc != it_end; ++itc ) {
      if ( *itc == match_item_n ) {
        res = std::distance ( it_begin, itc );
        break;
      }
    }
  }
  return res;
}

template < class T, std::size_t CAP >
typename Stack_Static< T, CAP >::size_type
Stack_Static< T, CAP >::index_of ( const T & match_item_n,
                                   size_type begin_at_n ) const
{
  size_type res ( _size );
  if ( begin_at_n < res ) {
    const_iterator it_begin ( cbegin () );
    const_iterator itc ( it_begin + begin_at_n );
    const_iterator it_end ( it_begin + _size );
    for ( ; itc != it_end; ++itc ) {
      if ( *itc == match_item_n ) {
        res = std::distance ( it_begin, itc );
        break;
      }
    }
  }
  return res;
}

template < class T, std::size_t CAP >
bool
Stack_Static< T, CAP >::remove_at ( size_type index_n )
{
  if ( index_n < _size ) {
    {
      iterator it_end ( end () );
      --it_end;
      // Move items above downwards by one
      {
        iterator itc ( begin () );
        itc += index_n;
        while ( itc != it_end ) {
          iterator it_next ( itc );
          ++it_next;
          *itc = std::move ( *it_next );
          itc = it_next;
        }
      }
      // Destruct last item
      it_end->~T ();
    }
    --_size;
    return true;
  }
  return false;
}

template < class T, std::size_t CAP >
bool
Stack_Static< T, CAP >::remove_first_match ( const T & match_value_n )
{
  bool found ( false );
  if ( !is_empty () ) {
    const size_type index ( index_of ( match_value_n ) );
    if ( index != _size ) {
      remove_at ( index );
      found = true;
    }
  }
  return found;
}

template < class T, std::size_t CAP >
typename Stack_Static< T, CAP >::size_type
Stack_Static< T, CAP >::remove_all_matches ( const T & match_value_n )
{
  size_type num_found ( 0 );
  if ( !is_empty () ) {
    // Init with search start index
    size_type index ( 0 );
    do {
      index = index_of ( match_value_n, index );
      if ( index != _size ) {
        remove_at ( index );
        ++num_found;
      } else {
        break;
      }
    } while ( !is_empty () );
  }
  return num_found;
}

template < class T, std::size_t CAP >
typename Stack_Static< T, CAP >::iterator
Stack_Static< T, CAP >::erase ( iterator it_n )
{
  const size_type item_index ( std::distance ( begin (), it_n ) );
  if ( item_index < _size ) {
    {
      iterator it_end ( end () );
      --it_end;
      {
        iterator itc ( it_n );
        while ( itc != it_end ) {
          iterator it_next ( itc );
          ++it_next;
          *itc = std::move ( *it_next );
          itc = it_next;
        }
      }
      // Destruct last item
      it_end->~T ();
    }
    --_size;
    return it_n;
  }
  return end ();
}

} // namespace sev::mem

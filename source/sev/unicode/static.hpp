/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::unicode
{

// -- Code point span
static const std::uint_fast32_t code_point_max_32 = 0x0010fffful;
static const std::uint_fast32_t code_point_end_32 = code_point_max_32 + 1ul;

// -- UTF-16 Surrogates
static const std::uint_fast32_t high_surrogate_min = 0xd800u;
static const std::uint_fast32_t high_surrogate_max = 0xdbffu;
static const std::uint_fast32_t low_surrogate_min = 0xdc00u;
static const std::uint_fast32_t low_surrogate_max = 0xdfffu;
static const std::uint_fast32_t surrogate_offset = 0x10000u;

static const std::uint_fast32_t surrogate_span_under =
    ( high_surrogate_min - 1ul );
static const std::uint_fast32_t surrogate_span_over =
    ( low_surrogate_max + 1ul );

// -- Replacement character
/// @brief Replacement character for invalid encoded characters
static const std::uint_fast32_t replacement_uc32 = 0x0000fffd;
static const std::size_t replacement_utf8_count = 2;
static const std::size_t replacement_utf16_count = 1;

// -- Utility functions

inline bool
code_point_is_high_surrogate ( std::uint_fast32_t u32_n )
{
  return ( ( u32_n >= high_surrogate_min ) && ( u32_n <= high_surrogate_max ) );
}

inline bool
code_point_is_low_surrogate ( std::uint_fast32_t u32_n )
{
  return ( ( u32_n >= low_surrogate_min ) && ( u32_n <= low_surrogate_max ) );
}

inline bool
code_point_is_surrogate ( std::uint_fast32_t u32_n )
{
  return ( ( u32_n > surrogate_span_under ) &&
           ( u32_n < surrogate_span_over ) );
}

inline bool
code_point_is_valid ( std::uint_fast32_t u32_n )
{
  return ( ( u32_n < code_point_end_32 ) &&
           !code_point_is_surrogate ( u32_n ) );
}

} // namespace sev::unicode

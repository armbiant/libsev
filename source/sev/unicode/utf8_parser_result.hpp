/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/utf8_error.hpp>

namespace sev::unicode
{

// -- Forward declaration
class Utf8_Parser;

/// @brief Contains result buffer reference, result state and error state
///
class Utf8_Parser_Result
{
  public:
  // -- Types

  friend class sev::unicode::Utf8_Parser;

  public:
  // -- Constructors

  Utf8_Parser_Result ()
  : _utf32_size ( 0 )
  , _utf8_size ( 0 )
  {
  }

  Utf8_Parser_Result ( std::size_t utf32_size_n,
                       std::size_t utf8_size_n,
                       Utf8_Error error_n )
  : _utf32_size ( utf32_size_n )
  , _utf8_size ( utf8_size_n )
  , _error ( error_n )
  {
  }

  // -- Data

  /// @brief Number of generated utf32 code points
  std::size_t
  utf32_size () const
  {
    return _utf32_size;
  }

  /// @brief Number of processed utf8 bytes
  std::size_t
  utf8_size () const
  {
    return _utf8_size;
  }

  const Utf8_Error &
  error () const
  {
    return _error;
  }

  private:
  std::size_t _utf32_size;
  std::size_t _utf8_size;
  Utf8_Error _error;
};

} // namespace sev::unicode

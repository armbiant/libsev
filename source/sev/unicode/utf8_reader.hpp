/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/utf8_error.hpp>
#include <array>
#include <cstdint>

namespace sev::unicode
{

/// @brief Parses a single UTF-32 code point from repeated UTF-8 byte pusing
///
class Utf8_Reader
{
  public:
  // -- Types

  using Feed_Func = bool ( Utf8_Reader::* ) ( char cha_n );

  // -- Construction

  Utf8_Reader ();

  // -- Parse / reset

  /// @brief Reset to construction state
  void
  reset ();

  /// @return True if a utf32 character is complete. May also indicate an
  /// error.
  ///
  /// In case of an error u32() contains a replacement character
  bool
  feed ( char cha_n )
  {
    return ( this->*_feed_func ) ( cha_n );
  }

  /// @return True if there is an error and a replacement charater in u32().
  ///
  bool
  finalize ();

  // -- UTF-32 character

  char32_t
  u32 () const
  {
    return _u32;
  }

  // -- Error

  bool
  has_error () const
  {
    return _error.is_valid ();
  }

  const Utf8_Error &
  error () const
  {
    return _error;
  }

  private:
  // -- Utility

  void
  finish_u32 ( std::uint8_t seq_length_n,
               std::uint_fast32_t span_begin_n,
               std::uint_fast32_t u32_n );

  // -- Callbacks

  bool
  feed_first ( char cha_n );

  bool
  feed_reset_first ( char cha_n );

  bool
  feed_2_1 ( char cha_n );

  bool
  feed_3_1 ( char cha_n );

  bool
  feed_3_2 ( char cha_n );

  bool
  feed_4_1 ( char cha_n );

  bool
  feed_4_2 ( char cha_n );

  bool
  feed_4_3 ( char cha_n );

  private:
  std::array< char, 3 > _buffer;
  char32_t _u32;
  Feed_Func _feed_func;
  Utf8_Error _error;
};

} // namespace sev::unicode

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::unicode
{

/// @brief Iterates along an utf8 string returning utf32 code points
///
enum class Utf8_Error_Type : std::uint8_t
{
  /// @brief No error
  NONE,
  // @brief The first byte of a single character sequence is invalid
  FIRST_INVALID,
  // @brief A trailing byte is invalid
  TRAIL_INVALID,
  // @brief A trailing byte is missing
  TRAIL_MISSING,
  /// @brief The code point is encoded in too many bytes
  OVERSIZE,
  /// @brief The code point is in the surrogate span
  SURROGATE,
  /// @brief The code point outside of the unicode range
  OUT_OF_RANGE
};

const char *
utf8_error_type_name ( Utf8_Error_Type type_n );

/// @brief Identifies an error when parsing a UTF-32 code point from UTF-8 bytes
///
class Utf8_Error
{
  public:
  // Constructors

  Utf8_Error ()
  : _type ( Utf8_Error_Type::NONE )
  , _length ( 0 )
  , _pos ( 0 )
  {
  }

  void
  clear ()
  {
    _type = Utf8_Error_Type::NONE;
    _length = 0;
    _pos = 0;
  }

  bool
  is_valid () const
  {
    return ( _type != Utf8_Error_Type::NONE );
  }

  Utf8_Error_Type
  type () const
  {
    return _type;
  }

  const char *
  type_name () const
  {
    return utf8_error_type_name ( _type );
  }

  void
  set_type ( Utf8_Error_Type type_n )
  {
    _type = type_n;
  }

  /// @brief Length of the multi byte UTF-8 character sequence
  std::uint8_t
  length () const
  {
    return _length;
  }

  void
  set_length ( std::uint8_t length_n )
  {
    _length = length_n;
  }

  /// @brief Position of the error in the multi byte UTF-8 character sequence
  std::uint8_t
  pos () const
  {
    return _pos;
  }

  void
  set_pos ( std::uint8_t pos_n )
  {
    _pos = pos_n;
  }

  void
  set_all ( Utf8_Error_Type type_n, std::uint8_t length_n, std::uint8_t pos_n )
  {
    _type = type_n;
    _length = length_n;
    _pos = pos_n;
  }

  public:
  Utf8_Error_Type _type;
  std::uint8_t _length;
  std::uint8_t _pos;
};

} // namespace sev::unicode

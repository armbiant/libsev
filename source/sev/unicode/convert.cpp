/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/unicode/convert.hpp>
#include <sev/unicode/utf16_iterator_replacing.hpp>
#include <sev/unicode/utf16_static.hpp>
#include <sev/unicode/utf8_iterator_replacing.hpp>
#include <sev/unicode/utf8_static.hpp>

namespace sev::unicode
{

// -- Static count and replace functions

/// @brief Returns the number of utf8 code uints required to represent uc32_n
inline static std::size_t
utf32_as_utf8_length_invalid_replace ( char32_t uc32_n )
{
  if ( uc32_n < utf8_sequence_1_cp_end ) {
    return 1;
  } else if ( uc32_n < utf8_sequence_2_cp_end ) {
    return 2;
  } else if ( uc32_n < utf8_sequence_3_cp_end ) {
    return 3;
  } else if ( uc32_n < utf8_sequence_4_cp_end ) {
    return 4;
  }
  return replacement_utf8_count;
}

/// @brief Returns the number of utf16 code uints required to represent uc32_n
inline std::size_t
utf32_as_utf16_length_invalid_replace ( char32_t uc32_n )
{
  if ( uc32_n < utf16_sequence_1_cp_end ) {
    return 1;
  } else if ( uc32_n < utf16_sequence_2_cp_end ) {
    return 2;
  }
  return replacement_utf16_count;
}

inline static void
utf8_write_invalid_replace ( char *& data_n, char32_t uc32_n )
{
  // -- Replace invalid character
  if ( !code_point_is_valid ( uc32_n ) ) {
    uc32_n = replacement_uc32;
  }
  // -- Write
  if ( uc32_n < utf8_sequence_1_cp_end ) {
    *( data_n++ ) = static_cast< char > ( uc32_n );
  } else if ( uc32_n < utf8_sequence_2_cp_end ) {
    *( data_n++ ) = static_cast< char > ( ( uc32_n >> 6 ) | 0xc0 );
    *( data_n++ ) = static_cast< char > ( ( uc32_n & 0x3f ) | 0x80 );
  } else if ( uc32_n < utf8_sequence_3_cp_end ) {
    *( data_n++ ) = static_cast< char > ( ( uc32_n >> 12 ) | 0xe0 );
    *( data_n++ ) = static_cast< char > ( ( ( uc32_n >> 6 ) & 0x3f ) | 0x80 );
    *( data_n++ ) = static_cast< char > ( ( uc32_n & 0x3f ) | 0x80 );
  } else {
    *( data_n++ ) = static_cast< char > ( ( uc32_n >> 18 ) | 0xf0 );
    *( data_n++ ) = static_cast< char > ( ( ( uc32_n >> 12 ) & 0x3f ) | 0x80 );
    *( data_n++ ) = static_cast< char > ( ( ( uc32_n >> 6 ) & 0x3f ) | 0x80 );
    *( data_n++ ) = static_cast< char > ( ( uc32_n & 0x3f ) | 0x80 );
  }
}

inline void
utf16_write_invalid_replace ( char16_t *& data_n, char32_t uc32_n )
{
  // -- Replace invalid character
  if ( !unicode::code_point_is_valid ( uc32_n ) ) {
    uc32_n = replacement_uc32;
  }
  // -- Write
  if ( uc32_n < utf16_sequence_1_cp_end ) {
    *( data_n++ ) = static_cast< char16_t > ( uc32_n );
  } else {
    uc32_n -= surrogate_offset;
    // High surrogate
    *( data_n++ ) = static_cast< char16_t > ( ( ( uc32_n >> 10 ) & 0x3ff ) |
                                              high_surrogate_min );
    // Low surrogate
    *( data_n++ ) =
        static_cast< char16_t > ( ( uc32_n & 0x3ff ) | low_surrogate_min );
  }
}

// -- Count function definitions

std::size_t
count_utf16_as_utf8 ( std::u16string_view view_n )
{
  std::size_t res ( 0 );
  {
    Utf16_Iterator_Replacing itu ( view_n );
    while ( true ) {
      const char32_t uc32 ( itu.read_next () );
      if ( uc32 == 0 ) {
        break;
      }
      res += utf32_as_utf8_length_invalid_replace ( uc32 );
    }
  }
  return res;
}

std::size_t
count_utf32_as_utf8 ( std::u32string_view view_n )
{
  std::size_t res ( 0 );
  for ( char32_t uc32 : view_n ) {
    if ( uc32 == 0 ) {
      break;
    }
    res += utf32_as_utf8_length_invalid_replace ( uc32 );
  }
  return res;
}

std::size_t
count_utf8_as_utf16 ( std::string_view view_n )
{
  std::size_t res ( 0 );
  {
    Utf8_Iterator_Replacing itu ( view_n );
    while ( true ) {
      const char32_t uc32 ( itu.read_next () );
      if ( uc32 == 0 ) {
        break;
      }
      res += sev::unicode::utf32_as_utf16_length_invalid_replace ( uc32 );
    }
  }
  return res;
}

std::size_t
count_utf32_as_utf16 ( std::u32string_view view_n )
{
  std::size_t res ( 0 );
  for ( char32_t uc32 : view_n ) {
    if ( uc32 == 0 ) {
      break;
    }
    res += sev::unicode::utf32_as_utf16_length_invalid_replace ( uc32 );
  }
  return res;
}

std::size_t
count_utf8_as_utf32 ( std::string_view view_n )
{
  std::size_t res ( 0 );
  {
    Utf8_Iterator_Replacing itu ( view_n );
    while ( itu.read_next () != 0 ) {
      ++res;
    }
  }
  return res;
}

std::size_t
count_utf16_as_utf32 ( std::u16string_view view_n )
{
  std::size_t res ( 0 );
  {
    Utf16_Iterator_Replacing itu ( view_n );
    while ( itu.read_next () != 0 ) {
      ++res;
    }
  }
  return res;
}

// -- Conversion function definitions

void
convert_utf16_to_utf8_counted ( char * begin_n, std::u16string_view view_n )
{
  Utf16_Iterator_Replacing itu ( view_n );
  char * str_it ( begin_n );
  while ( true ) {
    const char32_t uc32 ( itu.read_next () );
    if ( uc32 == 0 ) {
      break;
    }
    utf8_write_invalid_replace ( str_it, uc32 );
  }
}

void
convert_utf32_to_utf8_counted ( char * begin_n, std::u32string_view view_n )
{
  char * str_it ( begin_n );
  for ( char32_t uc32 : view_n ) {
    if ( uc32 == 0 ) {
      break;
    }
    utf8_write_invalid_replace ( str_it, uc32 );
  }
}

void
convert_utf8_to_utf16_counted ( char16_t * begin_n, std::string_view view_n )
{
  Utf8_Iterator_Replacing itu ( view_n );
  char16_t * str_it ( begin_n );
  while ( true ) {
    const char32_t uc32 ( itu.read_next () );
    if ( uc32 == 0 ) {
      break;
    }
    sev::unicode::utf16_write_invalid_replace ( str_it, uc32 );
  }
}

void
convert_utf32_to_utf16_counted ( char16_t * begin_n,
                                 std::u32string_view view_n )
{
  char16_t * str_it ( begin_n );
  for ( char32_t uc32 : view_n ) {
    if ( uc32 == 0 ) {
      break;
    }
    sev::unicode::utf16_write_invalid_replace ( str_it, uc32 );
  }
}

void
convert_utf8_to_utf32_counted ( char32_t * begin_n, std::string_view view_n )
{
  Utf8_Iterator_Replacing itu ( view_n );
  char32_t * str_it ( begin_n );
  while ( true ) {
    const char32_t uc32 ( itu.read_next () );
    if ( uc32 == 0 ) {
      break;
    }
    *str_it = uc32;
    ++str_it;
  }
}

void
convert_utf16_to_utf32_counted ( char32_t * begin_n,
                                 std::u16string_view view_n )
{
  Utf16_Iterator_Replacing itu ( view_n );
  char32_t * str_it ( begin_n );
  while ( true ) {
    const char32_t uc32 ( itu.read_next () );
    if ( uc32 == 0 ) {
      break;
    }
    *str_it = uc32;
    ++str_it;
  }
}

// -- String conversion to UTF-8

void
convert ( std::string & res_n, std::u16string_view view_n )
{
  res_n.resize ( count_utf16_as_utf8 ( view_n ) );
  convert_utf16_to_utf8_counted ( &res_n[ 0 ], view_n );
}

void
convert ( std::string & res_n, const std::u16string & src_n )
{
  convert ( res_n, std::u16string_view ( src_n.c_str (), src_n.size () ) );
}

void
convert ( std::string & res_n, std::u32string_view view_n )
{
  res_n.resize ( count_utf32_as_utf8 ( view_n ) );
  convert_utf32_to_utf8_counted ( &res_n[ 0 ], view_n );
}

void
convert ( std::string & res_n, const std::u32string & src_n )
{
  convert ( res_n, std::u32string_view ( src_n.c_str (), src_n.size () ) );
}

// -- String conversion to UTF-16

void
convert ( std::u16string & res_n, std::string_view view_n )
{
  res_n.resize ( count_utf8_as_utf16 ( view_n ) );
  convert_utf8_to_utf16_counted ( &res_n[ 0 ], view_n );
}

void
convert ( std::u16string & res_n, const std::string & src_n )
{
  convert ( res_n, std::string_view ( src_n.c_str (), src_n.size () ) );
}

void
convert ( std::u16string & res_n, std::u32string_view view_n )
{
  res_n.resize ( count_utf32_as_utf16 ( view_n ) );
  convert_utf32_to_utf16_counted ( &res_n[ 0 ], view_n );
}

void
convert ( std::u16string & res_n, const std::u32string & src_n )
{
  convert ( res_n, std::u32string_view ( src_n.c_str (), src_n.size () ) );
}

// -- String conversion to UTF-32

void
convert ( std::u32string & res_n, std::string_view view_n )
{
  res_n.resize ( count_utf8_as_utf32 ( view_n ) );
  convert_utf8_to_utf32_counted ( &res_n[ 0 ], view_n );
}

void
convert ( std::u32string & res_n, const std::string & src_n )
{
  convert ( res_n, std::string_view ( src_n.c_str (), src_n.size () ) );
}

void
convert ( std::u32string & res_n, std::u16string_view view_n )
{
  res_n.resize ( count_utf16_as_utf32 ( view_n ) );
  convert_utf16_to_utf32_counted ( &res_n[ 0 ], view_n );
}

void
convert ( std::u32string & res_n, const std::u16string & src_n )
{
  convert ( res_n, std::u16string_view ( src_n.c_str (), src_n.size () ) );
}

} // namespace sev::unicode

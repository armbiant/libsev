/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::unicode
{

/// @brief String encoding type
enum class Encoding : std::uint8_t
{
  UTF_8,
  UTF_16,
  UTF_32
};

} // namespace sev::unicode

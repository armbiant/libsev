/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/encoding.hpp>
#include <cstddef>
#include <string>
#include <string_view>

namespace sev::unicode
{

/// @brief String span reference with string encoding information
///
class View
{
  public:
  // -- Constructors

  /// @brief Sets to an empty invalid (@see is_valid()) UTF_8 sequence
  constexpr View () = default;

  /// @brief Set to the given zero terminated UTF-8 string (nullptr safe)
  constexpr View ( std::nullptr_t nullp_n [[maybe_unused]] ) {}

  /// @brief Set to the given zero terminated UTF-8 string (nullptr safe)
  constexpr View ( const char * utf8_n )
  {
    if ( utf8_n != nullptr ) {
      _data = static_cast< const void * > ( utf8_n );
      _size = std::char_traits< char >::length ( utf8_n );
    }
  }

  /// @brief Set to the given UTF-8 string.
  constexpr View ( const char * utf8_n, std::size_t size_n )
  : _data ( static_cast< const void * > ( utf8_n ) )
  , _size ( size_n )
  , _encoding ( Encoding::UTF_8 )
  {
  }

  /// @brief Set to the given UTF-8 string span
  constexpr View ( std::string_view span_n )
  : _data ( static_cast< const void * > ( span_n.data () ) )
  , _size ( span_n.size () )
  , _encoding ( Encoding::UTF_8 )
  {
  }

  /// @brief Set to the given zero terminated UTF-16 string (nullptr safe)
  constexpr View ( const char16_t * utf16_n )
  : _encoding ( Encoding::UTF_16 )
  {
    if ( utf16_n != nullptr ) {
      _data = static_cast< const void * > ( utf16_n );
      _size = std::char_traits< char16_t >::length ( utf16_n );
    }
  }

  /// @brief Set to the given UTF-16 string.
  constexpr View ( const char16_t * utf16_n, std::size_t size_n )
  : _data ( static_cast< const void * > ( utf16_n ) )
  , _size ( size_n )
  , _encoding ( Encoding::UTF_16 )
  {
  }

  /// @brief Set to the given UTF-16 string span
  constexpr View ( std::u16string_view span_n )
  : _data ( static_cast< const void * > ( span_n.data () ) )
  , _size ( span_n.size () )
  , _encoding ( Encoding::UTF_16 )
  {
  }

  /// @brief Set to the given zero terminated UTF-32 string (nullptr safe)
  constexpr View ( const char32_t * utf32_n )
  : _encoding ( Encoding::UTF_32 )
  {
    if ( utf32_n != nullptr ) {
      _data = static_cast< const void * > ( utf32_n );
      _size = std::char_traits< char32_t >::length ( utf32_n );
    }
  }

  /// @brief Set to the given UTF-16 string.
  constexpr View ( const char32_t * utf32_n, std::size_t size_n )
  : _data ( static_cast< const void * > ( utf32_n ) )
  , _size ( size_n )
  , _encoding ( Encoding::UTF_32 )
  {
  }

  /// @brief Set to the given UTF-32 string span
  constexpr View ( std::u32string_view span_n )
  : _data ( static_cast< const void * > ( span_n.data () ) )
  , _size ( span_n.size () )
  , _encoding ( Encoding::UTF_32 )
  {
  }

  /// @brief Initialize to string content
  View ( const std::string & string_n )
  : _data ( static_cast< const void * > ( string_n.c_str () ) )
  , _size ( string_n.size () )
  , _encoding ( Encoding::UTF_8 )
  {
  }

  /// @brief Initialize to string content
  View ( const std::u16string & string_n )
  : _data ( static_cast< const void * > ( string_n.c_str () ) )
  , _size ( string_n.size () )
  , _encoding ( Encoding::UTF_16 )
  {
  }

  /// @brief Initialize to string content
  View ( const std::u32string & string_n )
  : _data ( static_cast< const void * > ( string_n.c_str () ) )
  , _size ( string_n.size () )
  , _encoding ( Encoding::UTF_32 )
  {
  }

  constexpr View ( const View & span_n ) = default;

  constexpr View ( View && span_n ) = default;

  // -- Type

  constexpr Encoding
  encoding () const
  {
    return _encoding;
  }

  /// @brief Returns true if encoding() is UTF_8
  constexpr bool
  is_utf8 () const
  {
    return ( _encoding == Encoding::UTF_8 );
  }

  /// @brief Returns true if encoding() is UTF_16
  constexpr bool
  is_utf16 () const
  {
    return ( _encoding == Encoding::UTF_16 );
  }

  /// @brief Returns true if encoding() is UTF_32
  constexpr bool
  is_utf32 () const
  {
    return ( _encoding == Encoding::UTF_32 );
  }

  // -- Span

  /// @brief Returns true if the ( data != nullptr ) and ( size != 0 )
  constexpr bool
  is_valid () const
  {
    return ( ( _data != nullptr ) && ( _size != 0 ) );
  }

  /// @brief Returns true if the ( size == 0 )
  constexpr bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Only valid if is_utf8()
  constexpr std::string_view
  std_string_view () const
  {
    return std::string_view ( static_cast< const char * > ( _data ), _size );
  }

  /// @brief Only valid if is_utf16()
  constexpr std::u16string_view
  std_u16string_view () const
  {
    return std::u16string_view ( static_cast< const char16_t * > ( _data ),
                                 _size );
  }

  /// @brief Only valid if is_utf32()
  constexpr std::u32string_view
  std_u32string_view () const
  {
    return std::u32string_view ( static_cast< const char32_t * > ( _data ),
                                 _size );
  }

  // -- String generation

  /// @return Create a string
  std::string
  string () const
  {
    std::string res;
    get ( res );
    return res;
  }

  /// @return Create a string
  std::u16string
  string_16 () const
  {
    std::u16string res;
    get ( res );
    return res;
  }

  /// @return Create a string
  std::u32string
  string_32 () const
  {
    std::u32string res;
    get ( res );
    return res;
  }

  void
  get ( std::string & string_n ) const;

  void
  get ( std::u16string & string_n ) const;

  void
  get ( std::u32string & string_n ) const;

  // -- Assignment operators

  constexpr View &
  operator= ( const View & span_n ) = default;

  constexpr View &
  operator= ( View && span_n ) = default;

  private:
  const void * _data = nullptr;
  std::size_t _size = 0;
  Encoding _encoding = Encoding::UTF_8;
};

} // namespace sev::unicode

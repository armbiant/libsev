/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/view.hpp>
#include <sev/unicode/utf8_parser_result.hpp>
#include <array>

namespace sev::unicode
{

/// @brief Parses code points from an UTF-8 encoded bytestream section into a
///        UTF-32 code points buffer
///
class Utf8_Parser
{
  public:
  // -- Types

  typedef void ( Utf8_Parser::*Feed_Func ) ();

  public:
  // -- Constructors

  Utf8_Parser ();

  // -- Parse / reset

  /// @brief Reset to construction state
  void
  reset ();

  Utf8_Parser_Result
  parse ( sev::mem::View< char32_t > utf32_n,
          sev::mem::View< const char > utf8_n,
          bool last_block_n );

  private:
  void
  finalize ();

  void
  finish_u32 ( std::uint8_t seq_length_n,
               std::uint_fast32_t span_begin_n,
               std::uint_fast32_t u32_n );

  void
  feed_first ();

  void
  feed_2_1 ();

  void
  feed_3_1 ();

  void
  feed_3_2 ();

  void
  feed_4_1 ();

  void
  feed_4_2 ();

  void
  feed_4_3 ();

  private:
  char32_t * _u32_cur;
  char32_t * _u32_end;
  const char * _u8_cur;
  const char * _u8_end;
  Feed_Func _feed_func;
  std::array< char, 3 > _buffer;
  Utf8_Error _error;
};

} // namespace sev::unicode

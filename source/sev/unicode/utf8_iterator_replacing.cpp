/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/unicode/utf8_iterator_replacing.hpp>
#include <sev/unicode/utf8_static.hpp>

namespace sev::unicode
{

// -- Utility functions

/// @brief Read 8 bit and return in 32 bit
template < typename Iter >
inline std::uint_fast32_t
read_8 ( Iter it_n )
{
  return static_cast< std::uint_fast32_t > ( ( *it_n ) & 0xff );
}

static inline bool
merge_2 ( std::uint_fast32_t & uc32_n,
          const char *& it8_n,
          std::uint_fast32_t cha0_n )
{
  const std::uint_fast32_t cha1 ( read_8 ( it8_n ) );
  if ( utf8_is_trail ( cha1 ) ) {
    ++it8_n;
    const std::uint_fast32_t fcha ( utf8_merge_2 ( cha0_n, cha1 ) );
    if ( fcha >= utf8_sequence_1_cp_end ) {
      uc32_n = fcha;
      return true;
    }
  }
  return false;
}

static inline bool
merge_3 ( std::uint_fast32_t & uc32_n,
          const char *& it8_n,
          std::uint_fast32_t cha0_n )
{
  const std::uint_fast32_t cha1 ( read_8 ( it8_n ) );
  if ( utf8_is_trail ( cha1 ) ) {
    ++it8_n;
    std::uint_fast32_t cha2 ( read_8 ( it8_n ) );
    if ( utf8_is_trail ( cha2 ) ) {
      ++it8_n;
      const std::uint_fast32_t fcha ( utf8_merge_3 ( cha0_n, cha1, cha2 ) );
      if ( fcha >= utf8_sequence_2_cp_end ) {
        uc32_n = fcha;
        return true;
      }
    }
  }
  return false;
}

static inline bool
merge_4 ( std::uint_fast32_t & uc32_n,
          const char *& it8_n,
          std::uint_fast32_t cha0_n )
{
  const std::uint_fast32_t cha1 ( read_8 ( it8_n ) );
  if ( utf8_is_trail ( cha1 ) ) {
    ++it8_n;
    std::uint_fast32_t cha2 ( read_8 ( it8_n ) );
    if ( utf8_is_trail ( cha2 ) ) {
      ++it8_n;
      std::uint_fast32_t cha3 ( read_8 ( it8_n ) );
      if ( utf8_is_trail ( cha3 ) ) {
        ++it8_n;
        const std::uint_fast32_t fcha (
            utf8_merge_4 ( cha0_n, cha1, cha2, cha3 ) );
        if ( fcha >= utf8_sequence_3_cp_end ) {
          uc32_n = fcha;
          return true;
        }
      }
    }
  }
  return false;
}

/// @return True if a valid encoded character was read
static inline bool
read_char32 ( std::uint_fast32_t & uc32_n,
              const char *& it8_n,
              const std::size_t num_max_n )
{
  const std::uint_fast32_t cha0 ( read_8 ( it8_n ) );
  if ( utf8_length_is_1 ( cha0 ) ) {
    if ( cha0 != 0 ) {
      ++it8_n;
      uc32_n = cha0;
    }
    return true;
  } else {
    ++it8_n;
    if ( utf8_length_is_2 ( cha0 ) ) {
      if ( num_max_n >= 2 ) {
        if ( merge_2 ( uc32_n, it8_n, cha0 ) ) {
          return true;
        }
      }
    } else if ( utf8_length_is_3 ( cha0 ) ) {
      if ( num_max_n >= 3 ) {
        if ( merge_3 ( uc32_n, it8_n, cha0 ) ) {
          return true;
        }
      }
    } else if ( utf8_length_is_4 ( cha0 ) ) {
      if ( num_max_n >= 4 ) {
        if ( merge_4 ( uc32_n, it8_n, cha0 ) ) {
          return true;
        }
      }
    }
  }
  return false;
}

// -- Iterator class

char32_t
Utf8_Iterator_Replacing::read_next ()
{
  const std::size_t dist ( _it_end - _it_now );
  if ( dist != 0 ) {
    std::uint_fast32_t uc32 ( 0 );
    if ( read_char32 ( uc32, _it_now, dist ) ) {
      if ( code_point_is_valid ( uc32 ) ) {
        return uc32;
      }
    }
    return replacement_uc32;
  }
  return 0;
}

} // namespace sev::unicode

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once
#include <sev/unicode/static.hpp>

namespace sev::unicode
{

// -- Multi by character sequence code point upper limits

static const std::size_t utf16_sequence_1_cp_end = 0xffff;
static const std::size_t utf16_sequence_2_cp_end = code_point_end_32;

// -- Utility functions

/// @brief Returns the number of utf16 code uints required to represent uc32_n
inline std::size_t
utf32_as_utf16_length ( char32_t uc32_n )
{
  if ( uc32_n < utf16_sequence_1_cp_end ) {
    return 1;
  } else if ( uc32_n < utf16_sequence_2_cp_end ) {
    return 2;
  }
  return 0;
}

} // namespace sev::unicode

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/unicode/utf8_parser.hpp>
#include <sev/unicode/utf8_static.hpp>

namespace sev::unicode
{

// -- Utility functions
static inline std::uint_fast32_t
read_unsigned_char ( const char * ptr_n )
{
  // Make unsigned
  return *reinterpret_cast< const std::uint8_t * > ( ptr_n );
}

// -- Class methods

Utf8_Parser::Utf8_Parser ()
: _u32_cur ( nullptr )
, _u32_end ( nullptr )
, _u8_cur ( nullptr )
, _u8_end ( nullptr )
, _feed_func ( &Utf8_Parser::feed_first )
, _buffer{ { 0, 0, 0 } }
{
}

void
Utf8_Parser::reset ()
{
  _u32_cur = nullptr;
  _u32_end = nullptr;
  _u8_cur = nullptr;
  _u8_end = nullptr;
  _feed_func = &Utf8_Parser::feed_first;
  _buffer = { { 0, 0, 0 } };
  _error.clear ();
}

Utf8_Parser_Result
Utf8_Parser::parse ( sev::mem::View< char32_t > utf32_n,
                     sev::mem::View< const char > utf8_n,
                     bool last_block_n )
{
  // Update local copies
  _error.clear ();
  _u32_cur = utf32_n.begin ();
  _u32_end = utf32_n.end ();
  _u8_cur = utf8_n.begin ();
  _u8_end = utf8_n.end ();

  // Parse characters
  while ( ( _u32_cur != _u32_end ) && ( _u8_cur != _u8_end ) ) {
    // Call method pointer
    ( this->*_feed_func ) ();

    // Look for errors
    if ( _error.is_valid () ) {
      // Reset feed funtion
      _feed_func = &Utf8_Parser::feed_first;
      break;
    }
  }

  // Finalize last block
  if ( last_block_n && ( _u8_cur == _u8_end ) ) {
    finalize ();
  }

  // Update result
  return Utf8_Parser_Result (
      ( _u32_cur - utf32_n.begin () ), ( _u8_cur - utf8_n.begin () ), _error );
}

inline void
Utf8_Parser::finalize ()
{
  if ( _feed_func != &Utf8_Parser::feed_first ) {
    // Update _error if there are trail bytes missing
    if ( !_error.is_valid () ) {
      if ( _feed_func == &Utf8_Parser::feed_2_1 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 2, 1 );
      } else if ( _feed_func == &Utf8_Parser::feed_3_1 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 3, 1 );
      } else if ( _feed_func == &Utf8_Parser::feed_3_2 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 3, 2 );
      } else if ( _feed_func == &Utf8_Parser::feed_4_1 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 4, 1 );
      } else if ( _feed_func == &Utf8_Parser::feed_4_2 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 4, 2 );
      } else if ( _feed_func == &Utf8_Parser::feed_4_3 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 4, 3 );
      }
    }
    _feed_func = &Utf8_Parser::feed_first;
  }
}

inline void
Utf8_Parser::finish_u32 ( std::uint8_t length_n,
                          std::uint_fast32_t span_begin_n,
                          std::uint_fast32_t u32_n )
{
  if ( u32_n < span_begin_n ) {
    // The code point was encoded in too many bytes
    _error.set_all ( Utf8_Error_Type::OVERSIZE, length_n, length_n );
  } else if ( code_point_is_surrogate ( u32_n ) ) {
    _error.set_all ( Utf8_Error_Type::SURROGATE, length_n, length_n );
  } else if ( u32_n > code_point_max_32 ) {
    _error.set_all ( Utf8_Error_Type::OUT_OF_RANGE, length_n, length_n );
  } else {
    // Valid code point
    *_u32_cur = u32_n;
    ++_u32_cur;
  }
  // Reset feed function
  _feed_func = &Utf8_Parser::feed_first;
}

void
Utf8_Parser::feed_first ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  ++_u8_cur;
  if ( utf8_length_is_1 ( ncha ) ) {
    // Use directly
    *_u32_cur = ncha;
    ++_u32_cur;
  } else {
    _buffer[ 0 ] = ncha;
    if ( utf8_length_is_2 ( ncha ) ) {
      _feed_func = &Utf8_Parser::feed_2_1;
    } else if ( utf8_length_is_3 ( ncha ) ) {
      _feed_func = &Utf8_Parser::feed_3_1;
    } else if ( utf8_length_is_4 ( ncha ) ) {
      _feed_func = &Utf8_Parser::feed_4_1;
    } else {
      // Invalid first byte
      _error.set_all ( Utf8_Error_Type::FIRST_INVALID, 0, 0 );
    }
  }
}

void
Utf8_Parser::feed_2_1 ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  if ( utf8_is_trail ( ncha ) ) {
    ++_u8_cur;
    finish_u32 (
        2, utf8_sequence_1_cp_end, utf8_merge_2 ( _buffer[ 0 ], ncha ) );
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 2, 1 );
  }
}

void
Utf8_Parser::feed_3_1 ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  if ( utf8_is_trail ( ncha ) ) {
    ++_u8_cur;
    _buffer[ 1 ] = ncha;
    _feed_func = &Utf8_Parser::feed_3_2;
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 3, 1 );
  }
}

void
Utf8_Parser::feed_3_2 ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  if ( utf8_is_trail ( ncha ) ) {
    ++_u8_cur;
    finish_u32 ( 3,
                 utf8_sequence_2_cp_end,
                 utf8_merge_3 ( _buffer[ 0 ], _buffer[ 1 ], ncha ) );
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 3, 2 );
  }
}

void
Utf8_Parser::feed_4_1 ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  if ( utf8_is_trail ( ncha ) ) {
    ++_u8_cur;
    _buffer[ 1 ] = ncha;
    _feed_func = &Utf8_Parser::feed_4_2;
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 4, 1 );
  }
}

void
Utf8_Parser::feed_4_2 ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  if ( utf8_is_trail ( ncha ) ) {
    ++_u8_cur;
    _buffer[ 2 ] = ncha;
    _feed_func = &Utf8_Parser::feed_4_3;
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 4, 2 );
  }
}

void
Utf8_Parser::feed_4_3 ()
{
  const std::uint_fast32_t ncha ( read_unsigned_char ( _u8_cur ) );
  if ( utf8_is_trail ( ncha ) ) {
    ++_u8_cur;
    finish_u32 (
        4,
        utf8_sequence_3_cp_end,
        utf8_merge_4 ( _buffer[ 0 ], _buffer[ 1 ], _buffer[ 2 ], ncha ) );
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 4, 3 );
  }
}

} // namespace sev::unicode

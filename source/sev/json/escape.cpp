/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/json/escape.hpp>

namespace sev::json
{

const Char_Escapes char_escapes = { { { '\\', { '\\', '\\' } },
                                      { '/', { '\\', '/' } },
                                      { '"', { '\\', '"' } },
                                      { '\b', { '\\', 'b' } },
                                      { '\f', { '\\', 'f' } },
                                      { '\n', { '\\', 'n' } },
                                      { '\r', { '\\', 'r' } },
                                      { '\t', { '\\', 't' } } } };

} // namespace sev::json

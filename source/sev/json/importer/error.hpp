/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::json::importer
{

/// @brief Importer error type
///
enum class Error_Type : std::uint8_t
{
  NONE,
  UNEXPECTED_CHARACTER,
  STRING_MALFORMED,
  KEY_TOO_LONG,
  VALUE_TOO_LONG,
  VALUE_INVALID,
  NESTING_TOO_DEEP
};

/// @brief Importer error description
///
class Error
{
  public:
  // -- Construction

  Error ()
  : _type ( Error_Type::NONE )
  {
  }

  Error ( const Error & error_n ) = default;

  Error ( Error && error_n ) = default;

  ~Error () = default;

  // -- Assignment operators

  Error &
  operator= ( const Error & error_n ) = default;

  Error &
  operator= ( Error && error_n ) = default;

  // -- Setup

  void
  clear ()
  {
    _type = Error_Type::NONE;
  }

  // -- Type

  Error_Type
  type () const
  {
    return _type;
  }

  void
  set_type ( Error_Type type_n )
  {
    _type = type_n;
  }

  bool
  is_valid () const
  {
    return ( _type != Error_Type::NONE );
  }

  private:
  // -- Attributes
  Error_Type _type;
};

} // namespace sev::json::importer

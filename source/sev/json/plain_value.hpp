/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::json
{

/// @brief Numeric representation of a plain value string
///
class Plain_Value
{
  public:
  // -- Types

  enum class Type : std::uint_fast8_t
  {
    T_NULL,
    T_BOOL,
    T_DOUBLE
  };

  // -- Construction

  /// @brief Null type
  Plain_Value () = default;

  /// @brief Bool type
  explicit Plain_Value ( bool value_n )
  : _type ( Type::T_BOOL )
  , _bool ( value_n )
  {
  }

  /// @brief Double type
  explicit Plain_Value ( double value_n )
  : _type ( Type::T_DOUBLE )
  , _double ( value_n )
  {
  }

  // -- Type

  Type
  type () const
  {
    return _type;
  }

  bool
  is_null () const
  {
    return ( _type == Type::T_NULL );
  }

  bool
  is_bool () const
  {
    return ( _type == Type::T_BOOL );
  }

  bool
  is_double () const
  {
    return ( _type == Type::T_DOUBLE );
  }

  // -- Value

  bool
  as_bool () const
  {
    return _bool;
  }

  double
  as_double () const
  {
    return _double;
  }

  private:
  // -- Attributes
  Type _type = Type::T_NULL;
  bool _bool = false;
  double _double = 0.0;
};

} // namespace sev::json

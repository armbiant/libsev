/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/json/composer.hpp>
#include <sev/string/utility.hpp>

namespace sev::json::composer
{

Composer::Composer ()
{
  _string.reserve ( 256 );
}

Node::Node ( Composer & composer_n )
: _composer ( composer_n )
{
}

Node::Node ( Node & parent_n )
: _composer ( parent_n._composer )
, _parent ( &parent_n )
, _depth ( parent_n._depth + 1 )
{
}

inline bool
Node::first_value () const
{
  return ( _num_values == 0 );
}

inline void
Node::increment_num_values ()
{
  ++_num_values;
}

inline void
Node::append ( char char_n )
{
  _composer._string.push_back ( char_n );
}

inline void
Node::append ( std::string_view text_n )
{
  _composer._string.append ( text_n.begin (), text_n.size () );
}

std::string
Node::quoted ( std::string_view text_n ) const
{
  std::string text;
  // Reserve some extra space for escapes and quotes
  text.reserve ( text_n.size () * 5 / 4 + 2 );
  text.push_back ( '"' );
  for ( char schar : text_n ) {
    using Esc_Type = decltype ( Char_Escape::esc );
    const Esc_Type * repl = nullptr;
    for ( const auto & esc : char_escapes ) {
      if ( schar == esc.cha ) {
        repl = &esc.esc;
        break;
      }
    }
    if ( repl != nullptr ) {
      const Esc_Type & rep = ( *repl );
      text.append ( &rep[ 0 ], std::size ( rep ) );
    } else {
      text.push_back ( schar );
    }
  }
  text.push_back ( '"' );
  return text;
}

Node_Object::Node_Object ( Composer & composer_n )
: Node ( composer_n )
{
}

Node_Object::Node_Object ( Node & parent_n )
: Node ( parent_n )
{
}

void
Node_Object::value ( std::string_view key_n, std::string_view value_n )
{
  append_value ( key_n, quoted ( value_n ) );
}

void
Node_Object::value ( std::string_view key_n, double value_n )
{
  append_value ( key_n, sev::string::number ( value_n ) );
}

void
Node_Object::value ( std::string_view key_n, bool value_n )
{
  append_value ( key_n, value_n ? "true" : "false" );
}

void
Node_Object::value_number ( std::string_view key_n, std::int32_t value_n )
{
  value ( key_n, sev::string::number ( value_n ) );
}

void
Node_Object::value_number ( std::string_view key_n, std::uint32_t value_n )
{
  value ( key_n, sev::string::number ( value_n ) );
}

void
Node_Object::value_number ( std::string_view key_n, std::int64_t value_n )
{
  value ( key_n, sev::string::number ( value_n ) );
}

void
Node_Object::value_number ( std::string_view key_n, std::uint64_t value_n )
{
  value ( key_n, sev::string::number ( value_n ) );
}

void
Node_Object::append_value ( std::string_view key_n, std::string_view value_n )
{
  if ( !first_value () ) {
    append ( ',' );
  }
  append ( quoted ( key_n ) );
  append ( ':' );
  append ( value_n );
  increment_num_values ();
}

Object::Object ( Composer & composer_n )
: Node_Object ( composer_n )
{
  append ( '{' );
}

Object::Object ( Node & parent_n, std::string_view key_n )
: Node_Object ( parent_n )
{
  if ( !parent ()->first_value () ) {
    append ( ',' );
  }
  append ( quoted ( key_n ) );
  append ( ':' );
  append ( '{' );
  parent ()->increment_num_values ();
}

Object::~Object ()
{
  append ( '}' );
}

Root_Object::Root_Object ()
: Composer ()
, Node_Object ( static_cast< Composer & > ( *this ) )
{
  append ( '{' );
}

const std::string &
Root_Object::finish ()
{
  append ( '}' );
  return composer ().string ();
}

} // namespace sev::json::composer

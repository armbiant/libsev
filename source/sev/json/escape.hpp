/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <array>

namespace sev::json
{

// -- Types

struct Char_Escape
{
  char cha;
  char esc[ 2 ];
};

using Char_Escapes = std::array< Char_Escape, 8 >;

// -- Variables
extern const Char_Escapes char_escapes;

} // namespace sev::json

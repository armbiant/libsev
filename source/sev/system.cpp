/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "system.hpp"
#include <features.h>
#include <string.h>
#include <fstream>
#include <vector>

namespace
{
// --- Static variables
static constexpr std::size_t cache_line_size_default = 64;
static constexpr std::size_t cache_line_size_max = 1024;

char *
strerror_string_impl ( char * buffer_n,
                       std::size_t buffer_size_n,
                       int error_code_n )
{
  char * res = nullptr;
// strerror_r comes in two flavors XSI and GNU
// with slightly different behavior.
//
#if ( _POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600 ) && !_GNU_SOURCE
  // -- XSI version
  // always uses the provided buffer
  if ( strerror_r ( error_code_n, buffer_n, buffer_size_n ) == 0 ) {
    res = buffer;
  }
#else
  // -- GNU version
  // may or may not use the buffer.
  // The final string begin is returned in a pointer
  res = strerror_r ( error_code_n, buffer_n, buffer_size_n );
#endif
  return res;
}
} // namespace

namespace sev
{

// --- Definitions
std::size_t
cache_line_size ()
{
  static std::size_t cache_line_size = 0;

  // Check if the size was already read
  if ( cache_line_size == 0 ) {
    {
      const char * fname ( "/sys/devices/system/cpu/cpu0/cache/index0/"
                           "coherency_line_size" );
      std::ifstream ifs ( fname );
      if ( ifs.is_open () ) {
        if ( ifs.good () ) {
          ifs >> cache_line_size;
        }
      }
    }
    // Use default value on demand
    if ( cache_line_size == 0 ) {
      cache_line_size = cache_line_size_default;
    } else {
      // Keep this in reasonable dimensions on faulty information
      if ( cache_line_size > cache_line_size_max ) {
        cache_line_size = cache_line_size_max;
      }
    }
  }
  return cache_line_size;
}

std::string
strerror_string ( int error_code_n )
{
  std::string res;
  {
    constexpr std::size_t capacity = 1024;
    std::vector< char > buffer ( capacity, 0 );
    if ( char * pstr =
             strerror_string_impl ( buffer.data (), capacity, error_code_n ) ) {
      res = pstr;
    }
  }
  return res;
}

} // namespace sev

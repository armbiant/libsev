/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/number.hpp>
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <string>
#include <string_view>

namespace sev::string
{

/// @brief Calls clear() and shrink_to_fit() on the string
void
clear_and_shrink ( std::string & proc_n );

/// @brief Replaces all occurences of search_n in proc_n with replace_n
/// @return Number of replacements
std::size_t
replace_all ( std::string & proc_n,
              const std::string & search_n,
              const std::string & replace_n );

/// @brief Removes the search string from the begin of the string
/// @return True if the string was found at the begin and was removed
bool
remove_at_begin ( std::string & proc_n, const std::string & search_n );

/// @brief Replaces the search string at the begin of the string
/// @return True if the string was found at the begin and was replaced
bool
replace_at_begin ( std::string & proc_n,
                   const std::string & search_n,
                   const std::string & replace_n );

/// @brief Removes the search string from the end of the string
/// @return True if the string was found at the end and was removed
bool
remove_at_end ( std::string & proc_n, const std::string & search_n );

/// @brief Replaces the search string at the end of the string
/// @return True if the string was found at the end and was replaced
bool
replace_at_end ( std::string & proc_n,
                 const std::string & search_n,
                 const std::string & replace_n );

/// @brief Replaces space characters at front and end of the string
/// @return True if the string was found at the begin and was replaced
void
trim ( std::string & proc_n );

/// @brief Returns a C locale encoded number string
///
/// The number string will have enough precision to allow
/// perfectl restore of the double value.
///
std::string
number ( double value_n );

std::string
number ( float value_n );

std::string
number ( std::int32_t value_n );

std::string
number ( std::uint32_t value_n );

std::string
number ( std::int64_t value_n );

std::string
number ( std::uint64_t value_n );

/// @brief Sets the string to a C locale encoded number string
///
/// The number string will have enough precision to perfectly
/// restore the double value.
void
set_number ( std::string & proc_n, double value_n );

void
set_number ( std::string & proc_n, float value_n );

void
set_number ( std::string & proc_n, std::int64_t );

void
set_number ( std::string & proc_n, std::uint64_t );

inline void
set_number ( std::string & proc_n, sev::mem::Number number_n )
{
  switch ( number_n.type () ) {
  case sev::mem::Number::Type::I_8:
  case sev::mem::Number::Type::I_16:
  case sev::mem::Number::Type::I_32:
  case sev::mem::Number::Type::I_64:
    set_number ( proc_n, number_n.get_int64 () );
    break;
  case sev::mem::Number::Type::UI_8:
  case sev::mem::Number::Type::UI_16:
  case sev::mem::Number::Type::UI_32:
  case sev::mem::Number::Type::UI_64:
    set_number ( proc_n, number_n.get_int64 () );
    break;
  case sev::mem::Number::Type::F_S:
    set_number ( proc_n, number_n.get_float () );
    break;
  case sev::mem::Number::Type::F_D:
    set_number ( proc_n, number_n.get_double () );
    break;
  }
}

/// @return True on success
bool
get_number ( double & value_n, const std::string & string_n );

/// @return True on success
bool
get_number ( float & value_n, const std::string & string_n );

/// @return True on success
bool
get_number ( std::int64_t & value_n, const std::string & string_n );

/// @return True on success
bool
get_number ( std::uint64_t & value_n, const std::string & string_n );

/** Concatenate string pieces into a single string.  */
std::string
cat_views ( std::initializer_list< std::string_view > views_n );

/** Utility class for cmStrCat.  */
class Alpha_Num
{
  public:
  Alpha_Num ( std::string_view view_n )
  : View_ ( view_n )
  {
  }
  Alpha_Num ( std::string const & str_n )
  : View_ ( str_n )
  {
  }
  Alpha_Num ( const char * str_n )
  : View_ ( str_n )
  {
  }
  Alpha_Num ( char ch_n )
  : View_ ( Digits_, 1 )
  {
    Digits_[ 0 ] = ch_n;
  }
  Alpha_Num ( bool val_n )
  : View_ ( Digits_, 1 )
  {
    Digits_[ 0 ] = val_n ? '1' : '0';
  }
  Alpha_Num ( int val_n );
  Alpha_Num ( unsigned int val_n );
  Alpha_Num ( long int val_n );
  Alpha_Num ( unsigned long int val_n );
  Alpha_Num ( long long int val_n );
  Alpha_Num ( unsigned long long int val_n );
  Alpha_Num ( const void * val_n )
  : Alpha_Num ( reinterpret_cast< std::uintptr_t > ( val_n ) )
  {
  }
  Alpha_Num ( float val_n );
  Alpha_Num ( double val_n );

  std::string_view
  view () const
  {
    return View_;
  }

  private:
  std::string_view View_;
  char Digits_[ 32 ];
};

/** Concatenate string pieces and numbers into a single string.  */
template < typename... AV >
inline std::string
cat ( Alpha_Num const & a0_n, AV const &... args_n )
{
  return cat_views (
      { a0_n.view (),
        static_cast< Alpha_Num const & > ( args_n ).view ()... } );
}

} // namespace sev::string

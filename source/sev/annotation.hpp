/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cassert>

/// @brief Check for unused result
#if defined( __GNUC__ ) && ( __GNUC__ >= 4 )
#define ANN_WARN_UNUSED_RESULT __attribute__ ( ( warn_unused_result ) )
#elif defined( _MSC_VER ) && ( _MSC_VER >= 1700 )
#define ANN_WARN_UNUSED_RESULT _Check_return_
#else
#define ANN_WARN_UNUSED_RESULT
#endif

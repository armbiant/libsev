/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::logt
{

using Flags = std::uint_fast32_t;

/// @brief Log flags
///
enum Flag
{
  // -- Defaul flags
  /// @brief Critically invalid and uncatched program state encountered
  FL_ERROR = ( 1 << 0 ),
  /// @brief Non critical program error
  FL_WARNING = ( 1 << 1 ),
  /// @brief Info message
  FL_INFO = ( 1 << 2 ),
  /// @brief Progress message
  FL_PROGRESS = ( 1 << 3 ),
  /// @brief Write to log
  FL_LOG = ( 1 << 4 ),
  /// @brief Request synchronized (blocking) write
  FL_SYNC = ( 1 << 5 ),
  /// @brief Debug flags
  FL_DEBUG = ( 1 << 6 ),
  FL_DEBUG_0 = FL_DEBUG,
  FL_DEBUG_1 = ( 1 << 7 ),
  FL_DEBUG_2 = ( 1 << 8 ),
  /// @brief Begin of user defined flags
  FL_USER = ( 1 << 12 ),

  // -- Flag groups
  FLG_NONE = Flags ( 0 ),
  FLG_ALL = ~Flags ( 0 ),
  FLG_DEFAULT = 0xfff,

  FLG_EWI = FL_ERROR | FL_WARNING | FL_INFO,
  FLG_EWIP = FL_ERROR | FL_WARNING | FL_INFO | FL_PROGRESS
};

} // namespace sev::logt

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/reference.hpp>
#include <sev/logt/server/chain.hpp>
#include <sev/logt/server/core.hpp>
#include <sev/thread/reference_count.hpp>
#include <cstdint>

namespace sev::logt::server
{

/// @brief Internal context owned by a logt::Server
///
class Context
{
  public:
  Context ( logt::server::Core * core_n );

  Context ( const Context & context_n ) = delete;

  Context ( Context && context_n ) = delete;

  ~Context ();

  /// @brief Clears context_name() and parent_ref()
  void
  clear ();

  void
  setup ( logt::Flags mask_server_n,
          sev::unicode::View name_n,
          logt::server::Context * parent_n );

  // -- Context name

  const std::string &
  context_name () const
  {
    return _context_name;
  }

  // -- Local flag mask

  logt::Flags
  mask_local () const
  {
    return _mask_local.load ();
  }

  void
  set_mask_local ( logt::Flags flags_n );

  // -- Server flag mask

  logt::Flags
  mask_server () const
  {
    return _mask_server.load ();
  }

  void
  set_mask_server ( logt::Flags flags_n );

  // -- Log flags test

  bool
  test_flags ( logt::Flags flags_n )
  {
    return ( ( flags_n & _mask_total.load () ) != 0 );
  }

  // -- Log server

  logt::server::Core *
  core () const
  {
    return _core;
  }

  // -- Parent context

  logt::Reference &
  parent_ref ()
  {
    return _parent_ref;
  }

  const logt::Reference &
  parent_ref () const
  {
    return _parent_ref;
  }

  // -- Logging

  void
  log ( logt::Flags flags_n, std::string && string_n )
  {
    core ()->log ( this, flags_n, std::move ( string_n ) );
  }

  // -- Service

  /// @brief Id of the current service
  ///
  uintptr_t
  context_id () const
  {
    return reinterpret_cast< uintptr_t > ( this );
  }

  /// @brief Time point of the current service begin.
  ///
  const logt::server::Time_Point &
  service_time () const
  {
    return _service_time;
  }

  logt::server::Time_Point &
  service_time ()
  {
    return _service_time;
  }

  // -- Reference counting

  /// @brief Increments the user count
  ///
  void
  reference_count_increment ()
  {
    _reference_count.add ();
  }

  /// @brief Decrements the user count releases the context to the server on
  /// underrun
  ///
  void
  reference_count_decrement ();

  // -- Chaining

  logt::server::Context *
  chain_prev () const
  {
    return _chain_prev;
  }

  logt::server::Context *
  chain_next () const
  {
    return _chain_next;
  }

  // -- Operators

  Context &
  operator= ( const Context & context_n ) = delete;

  Context &
  operator= ( Context && context_n ) = delete;

  private:
  friend class Chain< Context >;

  void
  chain_prev_set ( Context * context_n )
  {
    _chain_prev = context_n;
  }

  void
  chain_next_set ( Context * context_n )
  {
    _chain_next = context_n;
  }

  void
  chain_clear ()
  {
    _chain_prev = nullptr;
    _chain_next = nullptr;
  }

  private:
  // -- Features
  std::atomic< logt::Flags > _mask_total;
  std::atomic< logt::Flags > _mask_local;
  std::atomic< logt::Flags > _mask_server;
  std::string _context_name;
  logt::server::Time_Point _service_time;

  // -- Chaining
  logt::server::Context * _chain_prev = nullptr;
  logt::server::Context * _chain_next = nullptr;

  // -- References
  logt::server::Core * _core = nullptr;
  logt::Reference _parent_ref;
  sev::thread::Reference_Count _reference_count;
};

} // namespace sev::logt::server

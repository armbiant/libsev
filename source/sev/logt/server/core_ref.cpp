/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/server/core.hpp>
#include <sev/logt/server/core_ref.hpp>

namespace sev::logt::server
{

Core_Ref::Core_Ref ()
: _core ( nullptr )
{
  _core = new Core;
  _core->ref_count_increment ();
}

Core_Ref::Core_Ref ( const logt::server::Core_Ref & ref_n )
: _core ( ref_n._core )
{
  if ( _core != nullptr ) {
    _core->ref_count_increment ();
  }
}

Core_Ref::~Core_Ref ()
{
  if ( _core != nullptr ) {
    decrement ( _core );
  }
}

void
Core_Ref::set_core ( logt::server::Core * core_n )
{
  if ( _core != core_n ) {
    if ( _core != nullptr ) {
      decrement ( _core );
    }
    _core = core_n;
    if ( _core != nullptr ) {
      _core->ref_count_increment ();
    }
  }
}

void
Core_Ref::copy_assign ( const Core_Ref & ref_n )
{
  set_core ( ref_n.core () );
}

void
Core_Ref::move_assign ( Core_Ref && ref_n )
{
  if ( &ref_n != this ) {
    if ( _core != nullptr ) {
      decrement ( _core );
    }
    _core = ref_n._core;
    if ( _core != nullptr ) {
      ref_n._core = nullptr;
    }
  }
}

void
Core_Ref::decrement ( Core * core_n )
{
  if ( !core_n->ref_count_decrement () ) {
    delete core_n;
  }
}

} // namespace sev::logt::server

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/context.hpp>
#include <sev/logt/server/context.hpp>

namespace sev::logt::server
{

Context::Context ( logt::server::Core * core_n )
: _mask_total ( logt::FLG_NONE )
, _mask_local ( logt::FLG_NONE )
, _mask_server ( logt::FLG_NONE )
, _chain_prev ( nullptr )
, _chain_next ( nullptr )
, _core ( core_n )
{
}

Context::~Context () {}

void
Context::clear ()
{
  _parent_ref.clear ();
  _context_name.clear ();
}

void
Context::setup ( logt::Flags mask_server_n,
                 sev::unicode::View name_n,
                 logt::server::Context * parent_n )
{
  _mask_total = ( mask_server_n & logt::Context::mask_default );
  _mask_local = logt::Context::mask_default;
  _mask_server = mask_server_n;
  name_n.get ( _context_name );
  _parent_ref.reset ( parent_n );
}

void
Context::reference_count_decrement ()
{
  if ( _reference_count.sub () == 1 ) {
    _core->context_release ( this );
  }
}

void
Context::set_mask_local ( logt::Flags mask_n )
{
  _mask_local.store ( mask_n );
  _mask_total.store ( _mask_local.load () & _mask_server.load () );
}

void
Context::set_mask_server ( logt::Flags mask_n )
{
  _mask_server.store ( mask_n );
  _mask_total.store ( _mask_local.load () & _mask_server.load () );
}

} // namespace sev::logt::server

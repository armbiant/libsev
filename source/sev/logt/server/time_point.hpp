/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <chrono>

namespace sev::logt::server
{

/// @brief Time point of a log event
///
class Time_Point
{
  public:
  Time_Point () { set_now (); }

  // @brief Sets the time points to the clocks' now()
  void
  set_now ();

  const std::chrono::steady_clock::time_point &
  steady () const
  {
    return _steady;
  }

  void
  set_steady ( std::chrono::steady_clock::time_point tp_n )
  {
    _steady = tp_n;
  }

  const std::chrono::system_clock::time_point &
  system () const
  {
    return _system;
  }

  void
  set_system ( std::chrono::system_clock::time_point tp_n )
  {
    _system = tp_n;
  }

  private:
  std::chrono::steady_clock::time_point _steady;
  std::chrono::system_clock::time_point _system;
};

} // namespace sev::logt::server

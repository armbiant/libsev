/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/flags.hpp>
#include <sev/logt/ostream_logger.hpp>
#include <sev/logt/reference.hpp>
#include <sev/string/utility.hpp>
#include <sev/unicode/view.hpp>
#include <sstream>

namespace sev::logt
{

// -- Forward declaration
class Server;
namespace server
{
class Context;
}

/// @brief Names log server client used to send log messages
///
class Context
{
  public:
  // -- Types

  static constexpr Flags mask_default = FLG_DEFAULT;
  static constexpr Flags flags_default = FL_INFO;

  public:
  // -- Constructors

  /// @brief Creates an invalid context
  Context ();

  /// @brief Make this context an alias to the given context
  Context ( const Reference & log_alias_n );

  /// @brief Connects to the parent's server with the given name
  Context ( const Reference & parent_n, sev::unicode::View name_n );

  /// @brief Copy constructor. Disabled.
  Context ( const Context & context_n ) = delete;

  /// @brief Move constructor
  Context ( Context && context_n )
  : _context_ref ( std::move ( context_n._context_ref ) )
  {
  }

  ~Context () {}

  // -- Server connection

  /// @brief Returns true if the context is connected to a
  /// server::Core
  ///
  /// @return True if the context is connected to a Server
  bool
  is_valid () const
  {
    return _context_ref.is_valid ();
  }

  /// @brief Log server reference.
  ///
  /// @see is_valid()
  /// @return Valid pointer when connected to a server nullptr otherwise.
  server::Core *
  server_core () const
  {
    return _context_ref.server_core ();
  }

  /// @brief Anonymous private context reference which can be passed around
  ///
  const Reference &
  context_ref () const
  {
    return _context_ref;
  }

  Reference
  parent_ref () const;

  /// @brief Disconnect from the log server core
  ///
  /// Resets context_ref(), server(), level_limit()
  void
  clear ();

  // -- Context name

  /// @brief Context name. Only valid (non empty) when is_valid().
  ///
  const std::string &
  context_name () const;

  // -- Log flags mask

  /// @brief Messages get logged when their flags pass through the mask
  ///
  /// This is a Context local mask that is only valid if is_valid()
  Flags
  mask () const;

  /// @brief Sets the log flags mask
  ///
  /// Has effect only if is_valid()
  void
  set_mask ( Flags mask_n );

  /// @brief Tests if flags_n pass the flags mask
  ///
  /// @return Will return false if !is_valid() or the test result
  bool
  test_flags ( Flags flags_n ) const;

  // -- Logging

  /// @brief Logs a unicode string using flags_default
  ///
  void
  str ( sev::unicode::View span_n ) const
  {
    str ( flags_default, span_n );
  }

  /// @brief Logs a unicode string
  ///
  void
  str ( Flags flags_n, sev::unicode::View span_n ) const;

  /// @brief Logs a concatenated string
  ///
  template < typename T, typename... AV >
  void
  cat ( Flags flags_n, T const & arg0_n, AV const &... args_n ) const
  {
    if ( test_flags ( flags_n ) ) {
      log_cat_unchecked (
          flags_n,
          { static_cast< sev::string::Alpha_Num const & > ( arg0_n ).view (),
            static_cast< sev::string::Alpha_Num const & > ( args_n )
                .view ()... } );
    }
  }

  // -- Log object logging

  OStream_Logger
  ostr ( Flags flags_n ) const
  {
    return test_flags ( flags_n ) ? OStream_Logger ( *this, flags_n )
                                  : OStream_Logger ();
  }

  // -- Flushing

  /// @brief Blocking request the server to flush all messages
  void
  flush () const;

  // -- Cast operators

  /// @brief Cast operator
  operator const Reference & () const { return _context_ref; }

  // -- Assignment operators

  Context &
  operator= ( const Context & context_n ) = delete;

  Context &
  operator= ( Context && context_n )
  {
    move_from ( std::move ( context_n ) );
    return *this;
  }

  private:
  friend class sev::logt::OStream_Logger;

  void
  log_unchecked ( Flags flags_n, sev::unicode::View span_n ) const;

  void
  log_cat_unchecked ( Flags flags_n,
                      std::initializer_list< std::string_view > views_n ) const;

  void
  move_from ( Context && context_n );

  private:
  Reference _context_ref;
};

} // namespace sev::logt

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

namespace sev::logt
{

// -- Forward declaration
namespace server
{
class Core;
class Context;
} // namespace server

/// @brief Shareable reference to the private server::Context of a
/// Context
///
class Reference
{
  public:
  // -- Constructors

  /// @brief Creates an invalid reference
  Reference ()
  : _context ( nullptr )
  {
  }

  /// @brief Copy constructor generates a new reference
  Reference ( server::Context * context_n );

  /// @brief Copy constructor generates a new reference
  Reference ( const Reference & ref_n );

  /// @brief Move constructor moves the reference ownership and clears the
  /// source
  Reference ( Reference && ref_n )
  : _context ( ref_n._context )
  {
    if ( _context != nullptr ) {
      ref_n._context = nullptr;
    }
  }

  /// @brief Clears before destruction
  ~Reference ();

  // -- Reset / clear

  void
  reset ( server::Context * context_n );

  /// @brief Clears the server_context() reference
  void
  clear ();

  // -- Accessors

  /// @brief Server context reference
  server::Context *
  context () const
  {
    return _context;
  }

  /// @return True if server_context() != nullptr
  bool
  is_valid () const
  {
    return ( _context != nullptr );
  }

  /// @brief Log server obtained from server_context()
  server::Core *
  server_core () const;

  // -- Cast operators

  /// @brief Cast operator
  ///
  operator bool () const { return ( _context != nullptr ); }

  /// @brief Cast operator
  ///
  operator server::Context * () const { return _context; }

  /// @brief Pointer operator
  ///
  server::Context *
  operator-> () const
  {
    return _context;
  }

  // -- Assignment operators

  /// @brief Generates a new reference
  Reference &
  operator= ( const Reference & ref_n )
  {
    reset ( ref_n.context () );
    return *this;
  }

  /// @brief Moves the reference ownership and clears the source
  Reference &
  operator= ( Reference && ref_n )
  {
    move_assign ( static_cast< Reference && > ( ref_n ) );
    return *this;
  }

  private:
  void
  move_assign ( Reference && ref_n );

  private:
  server::Context * _context;
};

} // namespace sev::logt

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/reference.hpp>
#include <sev/logt/server/context.hpp>

namespace sev::logt
{

Reference::Reference ( server::Context * context_n )
: _context ( context_n )
{
  // Register this reference
  if ( _context != nullptr ) {
    _context->reference_count_increment ();
  }
}

Reference::Reference ( const Reference & ref_n )
: _context ( ref_n._context )
{
  // Register this reference
  if ( _context != nullptr ) {
    _context->reference_count_increment ();
  }
}

Reference::~Reference ()
{
  if ( _context != nullptr ) {
    _context->reference_count_decrement ();
  }
}

void
Reference::reset ( server::Context * context_n )
{
  server::Context * cold ( _context );
  _context = context_n;
  if ( _context != nullptr ) {
    _context->reference_count_increment ();
  }
  // Dereference old context
  if ( cold != nullptr ) {
    cold->reference_count_decrement ();
  }
}

void
Reference::clear ()
{
  if ( _context != nullptr ) {
    _context->reference_count_decrement ();
    _context = nullptr;
  }
}

server::Core *
Reference::server_core () const
{
  if ( _context != nullptr ) {
    return _context->core ();
  }
  return nullptr;
}

void
Reference::move_assign ( Reference && ref_n )
{
  if ( &ref_n != this ) {
    if ( _context != nullptr ) {
      _context->reference_count_decrement ();
    }
    _context = ref_n._context;
    // Clear source
    if ( ref_n._context != nullptr ) {
      ref_n._context = nullptr;
    }
  }
}

} // namespace sev::logt

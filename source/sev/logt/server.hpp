/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/flags.hpp>
#include <sev/logt/reference.hpp>
#include <sev/logt/server/core_ref.hpp>

namespace sev::logt
{

// -- Forward declaration
class Context;
class Sink;
namespace server
{
class Context;
}

/// @brief Log server base class with interface
///
class Server
{
  public:
  // -- Types

  static const Flags mask_default = FLG_ALL;
  friend class Context;

  public:
  // -- Constructors

  Server ();

  Server ( Flags mask_n );

  /// @brief Destructor
  ///
  /// Contexts must not outlive the server.
  /// All remaining sinks get passed to sink_unregister()
  ///
  ~Server ();

  // -- Flags mask

  /// @brief Returns the server log flags mask
  ///
  Flags
  mask () const;

  /// @brief Sets the server log flags mask
  ///
  void
  set_mask ( Flags mask_n );

  // -- Flushing

  /// @brief Flush all buffered messages
  void
  flush ();

  // -- Server root context

  // @brief Root context
  const Reference &
  root_context () const;

  // -- Sinks

  /// @brief Installs a sink
  ///
  /// This method just registers the sink.
  /// It does not take over the ownership of the sink.
  ///
  /// @return True on success
  void
  sink_register ( Sink * sink_n );

  void
  sink_unregister ( Sink * sink_n );

  // -- Cast operators

  /// @brief Cast operator
  ///
  operator const Reference & () const { return root_context (); }

  private:
  server::Core_Ref _core_ref;
};

} // namespace sev::logt

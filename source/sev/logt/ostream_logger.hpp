/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/logt/flags.hpp>
#include <optional>
#include <sstream>

namespace sev::logt
{

// -- Forward declaration
class Context;

/// @brief ostream logger
///
class OStream_Logger
{
  public:
  // -- Constructors

  /// @brief Creates an invalid logger
  constexpr OStream_Logger () = default;

  OStream_Logger ( const Context & context_n, Flags flags_n )
  : _body ( std::in_place, context_n, flags_n )
  {
  }

  ~OStream_Logger ()
  {
    if ( _body ) {
      _body->log ();
    }
  }

  std::ostream &
  ost ()
  {
    DEBUG_ASSERT ( _body );
    return _body->ost;
  }

  template < typename T, typename... Args >
  void
  cat ( const T & arg_n, Args &&... args_n )
  {
    DEBUG_ASSERT ( _body );
    cat_ ( arg_n, std::forward< Args > ( args_n )... );
  }

  template < typename T >
  OStream_Logger &
  operator<< ( const T & arg_n )
  {
    DEBUG_ASSERT ( _body );
    _body->ost << arg_n;
    return *this;
  }

  operator bool () const { return _body.operator bool (); }

  private:
  template < typename T, typename... Args >
  void
  cat_ ( const T & arg_n, Args &&... args_n )
  {
    _body->ost << arg_n;
    cat_ ( std::forward< Args > ( args_n )... );
  }

  void
  cat_ ()
  {
  }

  struct Body
  {
    Body ( const Context & context_n, Flags flags_n )
    : context ( context_n )
    , flags ( flags_n )
    {
    }

    void
    log () const;

    const Context & context;
    Flags flags;
    std::ostringstream ost;
  };
  std::optional< Body > _body;
};

} // namespace sev::logt

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/sinks/ostream_stdout.hpp>
#include <iostream>

namespace sev::logt::sinks
{

OStream_StdOut::OStream_StdOut ( logt::Flags mask_n )
: logt::sinks::OStream ( std::cout.rdbuf (), mask_n )
{
}

} // namespace sev::logt::sinks

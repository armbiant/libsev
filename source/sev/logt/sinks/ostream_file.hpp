/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/flags.hpp>
#include <sev/logt/sink.hpp>
#include <sev/logt/sinks/ostream.hpp>
#include <fstream>
#include <string>

namespace sev::logt::sinks
{

/// @brief OStream_File
///
class OStream_File : public logt::sinks::OStream
{
  // Public methods
  public:
  OStream_File ( logt::Flags mask_n = mask_default );

  ~OStream_File ();

  bool
  open ( const std::string & file_n, bool truncate_n = false );

  void
  close ();

  // Private attributes
  public:
  std::ofstream _ofstream;
};

} // namespace sev::logt::sinks

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/sinks/ostream.hpp>

namespace sev::logt::sinks
{

/// @brief OStream_StdOut
///
class OStream_StdOut : public logt::sinks::OStream
{
  // Public methods
  public:
  OStream_StdOut ( logt::Flags mask_n = mask_default );
};

} // namespace sev::logt::sinks

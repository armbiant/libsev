/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/flags.hpp>
#include <sev/logt/sink.hpp>
#include <ostream>
#include <string>

// -- Forward declaration
namespace sev::logt::server
{
class Context;
class Message;
} // namespace sev::logt::server

namespace sev::logt::sinks
{

/// @brief OStream
///
/// Writes to an std::ostream
class OStream : public logt::Sink
{
  public:
  // -- Constructors

  OStream ( std::streambuf * rdbuf_n = nullptr,
            logt::Flags mask_n = mask_default );

  ~OStream () override;

  // -- Colors

  bool
  colors_enabled () const
  {
    return _colors_enabled;
  }

  void
  set_colors_enabled ( bool flag_n );

  // -- OStream

  std::streambuf *
  rdbuf () const
  {
    return _ostream.rdbuf ();
  }

  void
  set_rdbuf ( std::streambuf * rdbuf_n );

  // -- Abstract sink interface

  void
  log_message ( logt::server::Message * message_n ) override;

  void
  flush_request () override;

  void
  flush_wait () override;

  protected:
  void
  server_session_end () override;

  private:
  void
  acquire_line_prefix ( std::string & prefix_n,
                        logt::server::Context * scontext_n,
                        bool continued_n ) const;

  private:
  std::ostream _ostream;
  bool _colors_enabled;
  const char _char_endl;
  const char * _prefix_separator;
  const char * _prefix_begin;
  const char * _prefix_end;

  const char * _col_brace;
  const char * _col_name;
  const char * _col_separator;
  const char * _col_normal;
};

} // namespace sev::logt::sinks

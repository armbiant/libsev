/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <iterator>
#include <utility>

namespace sev
{

template < typename T, typename V >
inline bool
change ( T & item_n, const V & value_n )
{
  if ( item_n == value_n ) {
    return false;
  }
  item_n = value_n;
  return true;
}

template < typename T, typename V >
inline bool
change ( T & item_n, V && value_n )
{
  if ( item_n == value_n ) {
    return false;
  }
  item_n = std::move ( value_n );
  return true;
}

template < typename T >
struct reverse_range
{
  T & iterable;

  auto
  begin ()
  {
    return std::rbegin ( iterable );
  }

  auto
  end ()
  {
    return std::rend ( iterable );
  }
};

template < typename T >
reverse_range< T >
reverse ( T & iterable )
{
  return { iterable };
}

} // namespace sev

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "spinlock.hpp"
#include <thread>

namespace sev::thread
{

void
Spinlock::lock ()
{
  while ( !try_lock () ) {
    std::this_thread::yield ();
  }
}

} // namespace sev::thread

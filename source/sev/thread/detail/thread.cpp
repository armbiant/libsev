/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "thread.hpp"
#include <sev/thread/detail/tracker.hpp>
#include <sev/thread/tracker.hpp>
#include <sev/unicode/view.hpp>
#include <memory>

namespace sev::thread::detail
{

Thread::Thread ( sev::unicode::View name_n, sev::thread::Tracker * tracker_n )
: name ( name_n.string () )
, tracker ( tracker_n )
{
  if ( tracker != nullptr ) {
    tracker->_detail->thread_register ( this );
  }
}

Thread::~Thread ()
{
  join ();
  if ( tracker != nullptr ) {
    tracker->_detail->thread_unregister ( this );
  }
}

bool
Thread::start ()
{
  if ( is_running || !function ) {
    return false;
  }

  // Start thread
  std_thread = std::thread ( function );

  // Setup statistics
  is_running = true;
  {
    pthread_t pthread = std_thread.native_handle ();
    // Acquire clock id for statistics
    cpu_clock.valid = ( pthread_getcpuclockid ( pthread, &cpu_clock.id ) == 0 );
  }

  // Register
  if ( tracker != nullptr ) {
    tracker->_detail->thread_started ( this );
  }
  return true;
}

void
Thread::join ()
{
  if ( !is_running ) {
    return;
  }

  // Join thread
  std_thread.join ();

  // Reset statistics
  is_running = false;
  cpu_clock.valid = false;
  cpu_clock.id = {};

  // Unregister
  if ( tracker != nullptr ) {
    tracker->_detail->thread_joined ( this );
  }
}

} // namespace sev::thread::detail

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/thread/statistics/threads.hpp>
#include <cstddef>
#include <memory>
#include <mutex>
#include <vector>

// -- Forward declaration
namespace sev::thread::detail
{
class Thread;
}
namespace sev::thread::statistics
{
class Thread;
}

namespace sev::thread::detail
{

/// @brief Thread server private implementation
///
class Tracker
{
  public:
  // -- Types

  using Lock_Guard = std::lock_guard< std::mutex >;
  struct Entry
  {
    std::uint_fast32_t id = 0;
    sev::thread::detail::Thread * thread = nullptr;
  };

  // -- Construction

  Tracker ();

  Tracker ( const Tracker & tracker_n ) = delete;

  Tracker ( Tracker && tracker_n ) = delete;

  ~Tracker ();

  // -- Assignment operators

  Tracker &
  operator= ( const Tracker & tracker_n ) = delete;

  Tracker &
  operator= ( Tracker && tracker_n ) = delete;

  // -- Threads

  std::size_t
  thread_index ( sev::thread::detail::Thread * thread_n ) const;

  // -- Thread registration

  void
  thread_register ( sev::thread::detail::Thread * thread_n );

  void
  thread_unregister ( sev::thread::detail::Thread * thread_n );

  void
  thread_started ( sev::thread::detail::Thread * thread_n );

  void
  thread_joined ( sev::thread::detail::Thread * thread_n );

  // -- Statistics

  void
  statistics_update_thread ( sev::thread::detail::Thread & thread_n,
                             sev::thread::statistics::Thread & stats_n );

  void
  statistics_update ();

  std::shared_ptr< sev::thread::statistics::Threads >
  statistics_acquire ();

  void
  statistics_release (
      std::shared_ptr< sev::thread::statistics::Threads > && threads_stats_n );

  private:
  // -- Utility

  std::uint_fast32_t
  acquire_thread_id ();

  private:
  // -- Attributes
  std::mutex _mutex;
  // - Threads register
  std::uint_fast32_t _thread_id_latest = 0;
  sev::thread::statistics::Threads _statistics;
  std::vector< Entry > _threads;
  // - Statistics pool
  std::vector< std::shared_ptr< sev::thread::statistics::Threads > >
      _statistics_pool;
};

} // namespace sev::thread::detail

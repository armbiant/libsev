/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <bits/types/clockid_t.h>
#include <sev/assert.hpp>
#include <sev/thread/detail/thread.hpp>
#include <sev/thread/detail/tracker.hpp>
#include <sev/thread/statistics/thread.hpp>
#include <sev/thread/statistics/threads.hpp>
#include <pthread.h>
#include <time.h>
#include <algorithm>
#include <chrono>
#include <iterator>
#include <thread>
#include <utility>

namespace sev::thread::detail
{

Tracker::Tracker ()
{
  // Allocate some statistics
  constexpr std::size_t pool_size = 2;
  for ( std::size_t ii = 0; ii != pool_size; ++ii ) {
    _statistics_pool.push_back (
        std::make_shared< sev::thread::statistics::Threads > () );
  }
}

Tracker::~Tracker () = default;

std::size_t
Tracker::thread_index ( sev::thread::detail::Thread * thread_n ) const
{
  auto it_beg = _threads.cbegin ();
  auto it_end = _threads.cend ();
  auto it = std::find_if (
      it_beg, it_end, [ id = thread_n->id ] ( const Entry & entry_n ) {
        return entry_n.id == id;
      } );
  DEBUG_ASSERT ( it != it_end );
  return std::distance ( it_beg, it );
}

std::uint_fast32_t
Tracker::acquire_thread_id ()
{
  std::uint_fast32_t id = _thread_id_latest;
  while ( true ) {
    ++id;
    // Reserve zero as an invalid id
    if ( id == 0 ) {
      continue;
    }
    // Check if id is in use
    bool in_use = false;
    for ( auto & entry : _threads ) {
      if ( entry.id == id ) {
        in_use = true;
        break;
      }
    }
    if ( !in_use ) {
      break;
    }
  }

  // Accept id
  _thread_id_latest = id;
  return id;
}

void
Tracker::thread_register ( sev::thread::detail::Thread * thread_n )
{
  DEBUG_ASSERT ( thread_n != nullptr );
  {
    Lock_Guard lock ( _mutex );
    // Acquire thread id
    thread_n->id = acquire_thread_id ();
    // Register thread
    _threads.push_back ( { thread_n->id, thread_n } );
    _statistics.thread_emplace ( *thread_n );
  }
}

void
Tracker::thread_unregister ( sev::thread::detail::Thread * thread_n )
{
  DEBUG_ASSERT ( thread_n != nullptr );
  {
    Lock_Guard lock ( _mutex );
    const std::size_t tindex = thread_index ( thread_n );
    _threads.erase ( _threads.cbegin () + tindex );
    _statistics.thread_remove ( tindex );
  }
}

void
Tracker::thread_started ( sev::thread::detail::Thread * thread_n )
{
  DEBUG_ASSERT ( thread_n != nullptr );
  {
    Lock_Guard lock ( _mutex );
    ++_statistics._threads_running;
    _statistics._threads[ thread_index ( thread_n ) ]._is_running = true;
  }
}

void
Tracker::thread_joined ( sev::thread::detail::Thread * thread_n )
{
  DEBUG_ASSERT ( thread_n != nullptr );
  {
    Lock_Guard lock ( _mutex );
    --_statistics._threads_running;
    _statistics._threads[ thread_index ( thread_n ) ]._is_running = false;
  }
}

void
Tracker::statistics_update_thread ( sev::thread::detail::Thread & thread_n,
                                    sev::thread::statistics::Thread & stats_n )
{
  struct timespec tspec;
  if ( thread_n.cpu_clock.valid &&
       ( clock_gettime ( thread_n.cpu_clock.id, &tspec ) == 0 ) ) {
    std::chrono::nanoseconds nnow ( tspec.tv_nsec );
    nnow += std::chrono::seconds ( tspec.tv_sec );
    stats_n._cpu_time_frame = nnow - stats_n._cpu_time_total;
    stats_n._cpu_time_total = nnow;
  } else {
    stats_n._cpu_time_frame = std::chrono::nanoseconds ( 0 );
    stats_n._cpu_time_total = std::chrono::nanoseconds ( 0 );
  }
}

void
Tracker::statistics_update ()
{
  // Update thread statistics
  {
    const std::size_t num = _threads.size ();
    for ( std::size_t ii = 0; ii != num; ++ii ) {
      statistics_update_thread ( *_threads[ ii ].thread,
                                 _statistics._threads[ ii ] );
    }
  }
  // Update frame time
  {
    auto tnow = decltype ( _statistics )::Clock::now ();
    _statistics._frame_begin = _statistics.frame_end ();
    _statistics._frame_end = tnow;
    _statistics._frame_duration =
        _statistics._frame_end - _statistics.frame_begin ();
  }
  // Update thread loads
  if ( _statistics._frame_duration.count () != 0 ) {
    const std::chrono::nanoseconds frame_ns ( _statistics._frame_duration );
    const double frame_nsd = frame_ns.count ();
    for ( auto & stats : _statistics._threads ) {
      double load = double ( stats._cpu_time_frame.count () ) / frame_nsd;
      stats._frame_load = std::clamp ( 0.0, load, 1.0 );
    }
  }
}

std::shared_ptr< sev::thread::statistics::Threads >
Tracker::statistics_acquire ()
{
  std::shared_ptr< sev::thread::statistics::Threads > res;
  {
    Lock_Guard lock ( _mutex );
    // -- Update local statistics
    statistics_update ();
    // -- Create a copy of the local statistics
    if ( _statistics_pool.empty () ) {
      res = std::move ( _statistics_pool.back () );
      _statistics_pool.pop_back ();
      *res = _statistics;
    } else {
      res =
          std::make_shared< sev::thread::statistics::Threads > ( _statistics );
    }
  }
  return res;
}

void
Tracker::statistics_release (
    std::shared_ptr< sev::thread::statistics::Threads > && threads_stats_n )
{
  if ( !threads_stats_n ) {
    return;
  }
  {
    Lock_Guard lock ( _mutex );
    _statistics_pool.emplace_back ( std::move ( threads_stats_n ) );
  }
}

} // namespace sev::thread::detail

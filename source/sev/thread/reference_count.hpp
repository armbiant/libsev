/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <atomic>
#include <cstddef>

namespace sev::thread
{

/// @brief Reference counter with zero count assert on destruction
///
template < class INT_TYPE >
class Basic_Reference_Count
{
  public:
  // -- Constructors

  Basic_Reference_Count ()
  : _count ( 0 )
  {
  }

  Basic_Reference_Count ( INT_TYPE start_value_n )
  : _count ( start_value_n )
  {
  }

  ~Basic_Reference_Count () { DEBUG_ASSERT ( _count.load () == 0 ); }

  // -- Load

  /// @brief Current reference count
  INT_TYPE
  load ( std::memory_order order_n = std::memory_order_seq_cst )
  {
    return _count.load ( order_n );
  }

  // -- Add / sub

  /// @brief Increments the count
  /// @return Value before the incrementation
  INT_TYPE
  add ( std::memory_order order_n = std::memory_order_seq_cst )
  {
    return _count.fetch_add ( 1, order_n );
  }

  /// @brief Decrements the count
  /// @return Value before the decrement
  INT_TYPE
  sub ( std::memory_order order_n = std::memory_order_seq_cst )
  {
    return _count.fetch_sub ( 1, order_n );
  }

  private:
  std::atomic< INT_TYPE > _count;
};

// -- Type definitions
typedef Basic_Reference_Count< std::size_t > Reference_Count;

} // namespace sev::thread

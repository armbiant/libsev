/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "tracker.hpp"
#include <sev/thread/detail/tracker.hpp>
#include <utility>

namespace sev::thread
{

Tracker::Tracker ()
: _detail ( std::make_shared< detail::Tracker > () )
{
}

Tracker::Tracker ( Tracker && server_n ) = default;

Tracker::~Tracker () = default;

Tracker &
Tracker::operator= ( Tracker && tracker_n ) = default;

std::shared_ptr< statistics::Threads >
Tracker::statistics_acquire ()
{
  return _detail->statistics_acquire ();
}

void
Tracker::statistics_release (
    std::shared_ptr< statistics::Threads > && stats_n )
{
  _detail->statistics_release ( std::move ( stats_n ) );
}

} // namespace sev::thread

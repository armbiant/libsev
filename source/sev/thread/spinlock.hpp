/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <atomic>

namespace sev::thread
{

/// @brief Spinlock
///
class Spinlock
{
  public:
  // -- Constructors

  Spinlock () = default;

  Spinlock ( Spinlock const & ) = delete;

  Spinlock ( Spinlock && ) = delete;

  ~Spinlock () = default;

  // -- Lock / unlock

  void
  lock ();

  bool
  try_lock ()
  {
    return !_locked.test_and_set ( std::memory_order_acquire );
  }

  void
  unlock ()
  {
    _locked.clear ( std::memory_order_release );
  }

  private:
  std::atomic_flag _locked = ATOMIC_FLAG_INIT;
};

} // namespace sev::thread

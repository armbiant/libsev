/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <chrono>
#include <string>

// -- Forward declaration
namespace sev::thread::detail
{
class Thread;
class Tracker;
} // namespace sev::thread::detail

namespace sev::thread::statistics
{

/// @brief Thread statistics
///
class Thread
{
  public:
  // -- Construction

  Thread ( const sev::thread::detail::Thread & thread_n );

  // -- Thread identification

  std::uint_fast32_t
  id () const
  {
    return _id;
  }

  const std::string &
  name () const
  {
    return _name;
  }

  // -- Thread state

  bool
  is_running () const
  {
    return _is_running;
  }

  // -- Thread processing time

  std::chrono::nanoseconds const &
  cpu_time_total () const
  {
    return _cpu_time_total;
  }

  double
  cpu_time_total_seconds () const
  {
    return ( double ( _cpu_time_total.count () ) / 1000000000.0 );
  }

  std::chrono::nanoseconds const &
  cpu_time_frame () const
  {
    return _cpu_time_frame;
  }

  double
  cpu_time_frame_seconds () const
  {
    return ( double ( _cpu_time_frame.count () ) / 1000000000.0 );
  }

  double
  frame_load () const
  {
    return _frame_load;
  }

  private:
  // -- Attributes
  friend class sev::thread::detail::Tracker;
  std::uint_fast32_t _id = 0;
  std::string _name;
  bool _is_running = false;
  std::chrono::nanoseconds _cpu_time_total;
  std::chrono::nanoseconds _cpu_time_frame;
  double _frame_load = 0.0;
};

} // namespace sev::thread::statistics

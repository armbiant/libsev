/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "thread.hpp"
#include <sev/thread/detail/thread.hpp>

namespace sev::thread::statistics
{

Thread::Thread ( const sev::thread::detail::Thread & thread_n )
: _id ( thread_n.id )
, _name ( thread_n.name )
, _cpu_time_total ( 0 )
, _cpu_time_frame ( 0 )
{
}

} // namespace sev::thread::statistics

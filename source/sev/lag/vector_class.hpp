/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/lag/init.hpp>
#include <array>
#include <cstdint>
#include <initializer_list>
#include <iterator>

namespace sev::lag
{

/// @brief Vector class (template)
///
template < typename FLT, std::size_t DIM >
class Vector
{
  public:
  // -- Types
  static constexpr const std::size_t dim = DIM;
  using value_type = FLT;
  using iterator = value_type *;
  using const_iterator = value_type const *;
  using reverse_iterator = std::reverse_iterator< iterator >;
  using const_reverse_iterator = std::reverse_iterator< const_iterator >;
  using value_array = value_type[ DIM ];
  using std_array = std::array< value_type, DIM >;
  using vec = Vector< value_type, DIM >;

  // -- Construction

  /// @brief Komponents are value (zero) initialized
  constexpr Vector () = default;

  constexpr Vector ( vec const & ) = default;

  constexpr Vector ( vec && ) = default;

  constexpr Vector ( const value_array & array_n ) { assign ( array_n ); }

  constexpr Vector ( const std_array & array_n ) { assign ( array_n ); }

  constexpr explicit Vector ( const init::Zero & init_n [[maybe_unused]] )
  {
    fill ( 0.0 );
  }

  constexpr explicit Vector ( const init::One & init_n [[maybe_unused]] )
  {
    fill ( 1.0 );
  }

  constexpr explicit Vector ( init::Fill< value_type > init_n )
  {
    fill ( init_n.value );
  }

  // -- Assignment

  constexpr void
  fill ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] = value_n;
    }
  }

  constexpr void
  assign ( const value_array & array_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] = array_n[ ii ];
    }
  }

  constexpr void
  assign ( const std_array & array_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] = array_n[ ii ];
    }
  }

  constexpr void
  assign ( const vec & vec_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] = vec_n[ ii ];
    }
  }

  constexpr vec &
  operator= ( value_array const & array_n )
  {
    assign ( array_n );
    return *this;
  }

  constexpr vec &
  operator= ( const std_array & array_n )
  {
    assign ( array_n );
    return *this;
  }

  constexpr vec &
  operator= ( vec const & ) = default;

  constexpr vec &
  operator= ( vec && ) = default;

  // -- Meta information

  /// @brief Number of elements
  static constexpr std::size_t
  size ()
  {
    return DIM;
  }

  // -- Component accessors

  constexpr value_type const &
  x ( std::size_t index_n ) const
  {
    return _xx[ index_n ];
  }

  constexpr value_type &
  xr ( std::size_t index_n )
  {
    return _xx[ index_n ];
  }

  constexpr void
  set ( std::size_t index_n, value_type value_n )
  {
    _xx[ index_n ] = value_n;
  }

  // -- Array accessors

  constexpr value_array &
  data ()
  {
    return _xx;
  }

  constexpr const value_array &
  data () const
  {
    return _xx;
  }

  // -- Iteration

  constexpr iterator
  begin ()
  {
    return ( &_xx[ 0 ] );
  }

  constexpr iterator
  end ()
  {
    return ( &_xx[ DIM ] );
  }

  constexpr const_iterator
  begin () const
  {
    return ( &_xx[ 0 ] );
  }

  constexpr const_iterator
  end () const
  {
    return ( &_xx[ DIM ] );
  }

  constexpr const_iterator
  cbegin () const
  {
    return ( &_xx[ 0 ] );
  }

  constexpr const_iterator
  cend () const
  {
    return ( &_xx[ DIM ] );
  }

  // -- Component access operators

  constexpr value_type const &
  operator[] ( std::size_t index_n ) const
  {
    return _xx[ index_n ];
  }

  constexpr value_type &
  operator[] ( std::size_t index_n )
  {
    return _xx[ index_n ];
  }

  // -- Unary arithmetic operators

  constexpr vec
  operator+ () const
  {
    return vec ( *this );
  }

  constexpr vec
  operator- () const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = -_xx[ ii ];
    }
    return res;
  }

  // -- Scalar arithmetics operators

  constexpr vec
  operator+ ( value_type value_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] + value_n;
    }
    return res;
  }

  constexpr vec
  operator- ( value_type value_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] - value_n;
    }
    return res;
  }

  constexpr vec
  operator* ( value_type value_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] * value_n;
    }
    return res;
  }

  constexpr vec
  operator/ ( value_type value_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] / value_n;
    }
    return res;
  }

  // -- Scalar arithmetic assignment operators

  constexpr vec &
  operator+= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] += value_n;
    }
    return *this;
  }

  constexpr vec &
  operator-= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] -= value_n;
    }
    return *this;
  }

  constexpr vec &
  operator*= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] *= value_n;
    }
    return *this;
  }

  constexpr vec &
  operator/= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] /= value_n;
    }
    return *this;
  }

  // -- Vector arithmetic operators

  constexpr vec
  operator+ ( vec const & vector_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] + vector_n[ ii ];
    }
    return res;
  }

  constexpr vec
  operator- ( vec const & vector_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] - vector_n[ ii ];
    }
    return res;
  }

  constexpr vec
  operator* ( vec const & vector_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] * vector_n[ ii ];
    }
    return res;
  }

  constexpr vec
  operator/ ( vec const & vector_n ) const
  {
    vec res;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      res._xx[ ii ] = _xx[ ii ] / vector_n[ ii ];
    }
    return res;
  }

  // -- Vector arithmetic assignment operators

  constexpr vec &
  operator+= ( vec const & vector_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] += vector_n._xx[ ii ];
    }
    return *this;
  }

  constexpr vec &
  operator-= ( vec const & vector_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] -= vector_n._xx[ ii ];
    }
    return *this;
  }

  constexpr vec &
  operator*= ( vec const & vector_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] *= vector_n._xx[ ii ];
    }
    return *this;
  }

  constexpr vec &
  operator/= ( vec const & vector_n )
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      _xx[ ii ] /= vector_n._xx[ ii ];
    }
    return *this;
  }

  // -- Comparison operators

  constexpr bool
  operator== ( vec const & vector_n ) const
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      if ( _xx[ ii ] != vector_n._xx[ ii ] ) {
        return false;
      }
    }
    return true;
  }

  constexpr bool
  operator!= ( vec const & vector_n ) const
  {
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      if ( _xx[ ii ] != vector_n._xx[ ii ] ) {
        return true;
      }
    }
    return false;
  }

  private:
  value_array _xx = {};
};

// -- Non member operators - Vector * Vector

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM > &
operator+ ( Vector< FLT, DIM > & vector_n )
{
  return vector_n;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM > const &
operator+ ( Vector< FLT, DIM > const & vector_n )
{
  return vector_n;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator- ( Vector< FLT, DIM > const & vector_n )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res._xx[ ii ] = -vector_n._xx[ ii ];
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator+ ( Vector< FLT, DIM > const & v_0, Vector< FLT, DIM > const & v_1 )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] + v_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator- ( Vector< FLT, DIM > const & v_0, Vector< FLT, DIM > const & v_1 )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] - v_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator* ( Vector< FLT, DIM > const & v_0, Vector< FLT, DIM > const & v_1 )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] * v_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator/ ( Vector< FLT, DIM > const & v_0, Vector< FLT, DIM > const & v_1 )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] / v_1._xx[ ii ] );
  }
  return res;
}

// -- Non member operators - Vector * Scalar

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator+ ( Vector< FLT, DIM > const & v_0, FLT value_n )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res[ ii ] = v_0[ ii ] + value_n;
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator- ( Vector< FLT, DIM > const & v_0, FLT value_n )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res[ ii ] = v_0[ ii ] - value_n;
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator* ( Vector< FLT, DIM > const & v_0, FLT value_n )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res[ ii ] = v_0[ ii ] * value_n;
  }
  return res;
}

template < typename FLT, std::size_t DIM >
constexpr inline Vector< FLT, DIM >
operator/ ( Vector< FLT, DIM > const & v_0, FLT value_n )
{
  Vector< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    res[ ii ] = v_0[ ii ] / value_n;
  }
  return res;
}

// -- Types

template < class FLT >
using Vector1 = Vector< FLT, 1 >;
using Vector1f = Vector1< float >;
using Vector1d = Vector1< double >;

template < class FLT >
using Vector2 = Vector< FLT, 2 >;
using Vector2f = Vector2< float >;
using Vector2d = Vector2< double >;

template < class FLT >
using Vector3 = Vector< FLT, 3 >;
using Vector3f = Vector3< float >;
using Vector3d = Vector3< double >;

template < class FLT >
using Vector4 = Vector< FLT, 4 >;
using Vector4f = Vector4< float >;
using Vector4d = Vector4< double >;

} // namespace sev::lag

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "matrix_square_class.hpp"

namespace sev::lag
{

template < typename FLT >
inline void
mat_rf_set_rotation ( Matrix2x2< FLT > & mat_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  mat_n[ 0 ] = cn;
  mat_n[ 1 ] = -sn;
  mat_n[ 2 ] = sn;
  mat_n[ 3 ] = cn;
}

template < typename FLT >
inline void
mat_cf_set_rotation ( Matrix2x2< FLT > & mat_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  mat_n[ 0 ] = cn;
  mat_n[ 1 ] = sn;
  mat_n[ 2 ] = -sn;
  mat_n[ 3 ] = cn;
}

template < typename FLT >
inline void
mat_rf_rotated ( Matrix2x2< FLT > & mdst,
                 Matrix2x2< FLT > const & msrc_n,
                 FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  mdst[ 0 ] = cn * msrc_n[ 0 ] - sn * msrc_n[ 2 ];
  mdst[ 1 ] = cn * msrc_n[ 1 ] - sn * msrc_n[ 3 ];

  mdst[ 2 ] = sn * msrc_n[ 0 ] + cn * msrc_n[ 2 ];
  mdst[ 3 ] = sn * msrc_n[ 1 ] + cn * msrc_n[ 3 ];
}

template < typename FLT >
inline void
mat_cf_rotated ( Matrix2x2< FLT > & mdst,
                 Matrix2x2< FLT > const & msrc_n,
                 FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  mdst[ 0 ] = cn * msrc_n[ 0 ] - sn * msrc_n[ 1 ];
  mdst[ 1 ] = sn * msrc_n[ 0 ] + cn * msrc_n[ 1 ];

  mdst[ 2 ] = cn * msrc_n[ 2 ] - sn * msrc_n[ 3 ];
  mdst[ 3 ] = sn * msrc_n[ 2 ] + cn * msrc_n[ 3 ];
}

template < typename FLT >
inline void
mat_rf_rotate ( Matrix2x2< FLT > & mat_n, FLT radians_n )
{
  Matrix2x2< FLT > tmp;
  mat_rf_rotated ( tmp, mat_n, radians_n );
  mat_n = tmp;
}

template < typename FLT >
inline void
mat_cf_rotate ( Matrix2x2< FLT > & mat_n, FLT radians_n )
{
  Matrix2x2< FLT > tmp;
  mat_cf_rotated ( tmp, mat_n, radians_n );
  mat_n = tmp;
}

} // namespace sev::lag

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "matrix_square.hpp"
#include "vector_class.hpp"
#include <algorithm>

namespace sev::lag
{

// Transpose

template < typename FLT >
void
transpose ( Matrix3x3< FLT > & mat_n )
{
  std::swap ( mat_n[ 1 ], mat_n[ 3 ] );
  std::swap ( mat_n[ 2 ], mat_n[ 6 ] );
  std::swap ( mat_n[ 5 ], mat_n[ 7 ] );
}

template < typename FLT >
Matrix3x3< FLT >
transposed ( Matrix3x3< FLT > const & msrc_n )
{
  Matrix3x3< FLT > mres;
  mres[ 0 ] = msrc_n[ 0 ]; // Diagonal
  mres[ 1 ] = msrc_n[ 3 ];
  mres[ 2 ] = msrc_n[ 6 ];
  mres[ 3 ] = msrc_n[ 1 ];
  mres[ 4 ] = msrc_n[ 4 ]; // Diagonal
  mres[ 5 ] = msrc_n[ 7 ];
  mres[ 6 ] = msrc_n[ 2 ];
  mres[ 7 ] = msrc_n[ 5 ];
  mres[ 8 ] = msrc_n[ 8 ]; // Diagonal
  return mres;
}

// Determinant

template < typename FLT >
FLT
determinant ( Matrix3x3< FLT > const & mat_n )
{
  FLT res;
  FLT tmp;

  res = mat_n[ 0 ] * mat_n[ 4 ] * mat_n[ 8 ];
  res += mat_n[ 1 ] * mat_n[ 5 ] * mat_n[ 6 ];
  res += mat_n[ 2 ] * mat_n[ 3 ] * mat_n[ 7 ];

  tmp = mat_n[ 6 ] * mat_n[ 4 ] * mat_n[ 2 ];
  tmp += mat_n[ 7 ] * mat_n[ 5 ] * mat_n[ 0 ];
  tmp += mat_n[ 8 ] * mat_n[ 3 ] * mat_n[ 1 ];

  return ( res - tmp );
}

// Inverse

template < typename FLT >
inline Matrix3x3< FLT >
inverse_det ( Matrix3x3< FLT > const & msrc_n, FLT det_n )
{
  Matrix3x3< FLT > mres;
  mres[ 0 ] = msrc_n[ 4 ] * msrc_n[ 8 ] - msrc_n[ 5 ] * msrc_n[ 7 ];
  mres[ 1 ] = msrc_n[ 2 ] * msrc_n[ 7 ] - msrc_n[ 1 ] * msrc_n[ 8 ];
  mres[ 2 ] = msrc_n[ 1 ] * msrc_n[ 5 ] - msrc_n[ 2 ] * msrc_n[ 4 ];

  mres[ 3 ] = msrc_n[ 5 ] * msrc_n[ 6 ] - msrc_n[ 3 ] * msrc_n[ 8 ];
  mres[ 4 ] = msrc_n[ 0 ] * msrc_n[ 8 ] - msrc_n[ 2 ] * msrc_n[ 6 ];
  mres[ 5 ] = msrc_n[ 2 ] * msrc_n[ 3 ] - msrc_n[ 0 ] * msrc_n[ 5 ];

  mres[ 6 ] = msrc_n[ 3 ] * msrc_n[ 7 ] - msrc_n[ 4 ] * msrc_n[ 6 ];
  mres[ 7 ] = msrc_n[ 1 ] * msrc_n[ 6 ] - msrc_n[ 0 ] * msrc_n[ 7 ];
  mres[ 8 ] = msrc_n[ 0 ] * msrc_n[ 4 ] - msrc_n[ 1 ] * msrc_n[ 3 ];

  mres /= det_n;
  return mres;
}

template < typename FLT >
inline void
invert_det ( Matrix3x3< FLT > & mat_n, FLT det_n )
{
  mat_n = inverse_det ( mat_n, det_n );
}

template < typename FLT >
inline bool
invert ( Matrix3x3< FLT > & mat_n )
{
  FLT const det ( determinant ( mat_n ) );
  if ( det != 0.0 ) {
    invert_det ( mat_n, det );
    return true;
  }
  return false;
}

template < typename FLT >
inline bool
inverse ( Matrix3x3< FLT > & mres_n, Matrix3x3< FLT > const & msrc_n )
{
  FLT const det ( determinant ( msrc_n ) );
  if ( det != 0.0 ) {
    inverse_det ( mres_n, msrc_n, det );
    return true;
  }
  return false;
}

template < typename FLT >
inline bool
solve_rf ( Vector3< FLT > & vres_n,
           Matrix3x3< FLT > const & msrc_n,
           Vector3< FLT > const & vsrc_n )
{
  Matrix3x3< FLT > mat_i;
  if ( inverse ( mat_i, msrc_n ) ) {
    mult_rf ( vres_n, mat_i, vsrc_n );
    return true;
  }
  return false;
}

template < typename FLT >
inline bool
solve_cf ( Vector3< FLT > & vres_n,
           Matrix3x3< FLT > const & msrc_n,
           Vector3< FLT > const & vsrc_n )
{
  Matrix3x3< FLT > mat_i;
  if ( inverse ( mat_i, msrc_n ) ) {
    mult_cf ( vres_n, mat_i, vsrc_n );
    return true;
  }
  return false;
}

} // namespace sev::lag

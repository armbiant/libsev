/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "vector_class.hpp"

namespace sev::lag
{

template < typename FLT >
inline constexpr void
rotate ( Vector2< FLT > & vec_n, FLT rad_n )
{
  Vector2< FLT > tmp;
  {
    FLT const sn ( sin ( rad_n ) );
    FLT const cn ( cos ( rad_n ) );
    tmp[ 0 ] = vec_n[ 0 ] * cn - vec_n[ 1 ] * sn;
    tmp[ 1 ] = vec_n[ 0 ] * sn + vec_n[ 1 ] * cn;
  }
  vec_n = tmp;
}

template < typename FLT >
inline constexpr Vector2< FLT >
rotated ( Vector2< FLT > & vres_n, Vector2< FLT > const & vsrc_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );
  return Vector2< FLT > ( { vsrc_n[ 0 ] * cn - vsrc_n[ 1 ] * sn,
                            vsrc_n[ 0 ] * sn + vsrc_n[ 1 ] * cn } );
}

template < typename FLT >
inline constexpr void
rotate_90 ( Vector2< FLT > & vec_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return;
  } else if ( times_n == 1 ) {
    std::swap ( vec_n[ 0 ], vec_n[ 1 ] );
    vec_n[ 0 ] = -vec_n[ 0 ];
  } else if ( times_n == 2 ) {
    vec_n[ 0 ] = -vec_n[ 0 ];
    vec_n[ 1 ] = -vec_n[ 1 ];
  } else {
    std::swap ( vec_n[ 0 ], vec_n[ 1 ] );
    vec_n[ 1 ] = -vec_n[ 1 ];
  }
}

template < typename FLT >
inline constexpr Vector2< FLT >
rotated_90 ( Vector2< FLT > const & vsrc_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return vsrc_n;
  } else if ( times_n == 1 ) {
    return Vector2< FLT > ( { -vsrc_n[ 1 ], vsrc_n[ 0 ] } );
  } else if ( times_n == 2 ) {
    return Vector2< FLT > ( { -vsrc_n[ 0 ], -vsrc_n[ 1 ] } );
  }
  return Vector2< FLT > ( { vsrc_n[ 1 ], -vsrc_n[ 0 ] } );
}

} // namespace sev::lag

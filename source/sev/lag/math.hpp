/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cmath>
#include <utility>

namespace sev::lag
{

// Constants double

/// @brief pi as double
const double
    pi_d ( 3.14159265358979323846264338327950288419716939937510582097494 );
/// @brief pi / 2.0 as double
const double
    pi_half_d ( 1.57079632679489661923132169163975144209858469968755291048747 );
/// @brief 2.0 * pi as double
const double
    two_pi_d ( 6.28318530717958647692528676655900576839433879875021164194989 );
/// @brief sqrt ( 2.0 ) as double
const double
    sqrt2_d ( 1.41421356237309504880168872420969807856967187537694807317668 );
/// @brief 1.0 / sqrt ( 2.0 ) as double
const double inv_sqrt2_d (
    0.70710678118654752440084436210484903928483593768847403658834 );

// Constants float

/// @brief pi as float
const float pi_f ( pi_d );
/// @brief pi / 2.0 as float
const float pi_half_f ( pi_half_d );
/// @brief 2.0 * pi as float
const float two_pi_f ( two_pi_d );
/// @brief sqrt ( 2.0 ) as float
const float sqrt2_f ( sqrt2_d );
/// @brief 1.0 / sqrt ( 2.0 ) as float
const float inv_sqrt2_f ( inv_sqrt2_d );

// Functions

template < typename FLT >
inline constexpr FLT
square ( FLT val_n )
{
  return ( val_n * val_n );
}

template < typename FLT >
inline constexpr FLT
square_root ( FLT val_n )
{
  return std::sqrt ( val_n );
}

template < typename FLT >
inline constexpr FLT
degrees_to_radians ( FLT degrees_n )
{
  return ( degrees_n / FLT ( 180.0 ) ) * pi_d;
}

template < typename FLT >
inline constexpr FLT
degrees_in_360 ( FLT degrees_n )
{
  FLT res ( degrees_n );
  FLT const three_sixty ( FLT ( 360.0 ) );
  if ( res < FLT ( 0.0 ) ) {
    res = -res;
    res = std::fmod ( res, three_sixty );
    res = three_sixty - res;
  }
  res = std::fmod ( res, three_sixty );
  return res;
}

template < typename FLT >
inline constexpr FLT
radians_to_degrees ( FLT radians_n )
{
  return ( radians_n / pi_d ) * FLT ( 180.0 );
}

template < typename FLT >
inline constexpr FLT
radians_in_two_pi ( FLT radians_n )
{
  FLT res ( radians_n );
  if ( res < FLT ( 0.0 ) ) {
    res = -res;
    res = std::fmod ( res, two_pi_d );
    res = two_pi_d - res;
  }
  res = std::fmod ( res, two_pi_d );
  return res;
}

} // namespace sev::lag

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "math_array.hpp"
#include "matrix_square_class.hpp"
#include "vector_class.hpp"
#include <cstdint>

namespace sev::lag
{

// Row getting

template < typename FLT, std::size_t DIM >
inline void
get_row_rf ( FLT * data_n,
             Matrix_Square< FLT, DIM > const & mat_n,
             std::size_t index_n )
{
  std::size_t const index ( index_n * DIM );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    data_n[ ii ] = mat_n.x ( index + ii );
  }
}

template < typename FLT, std::size_t DIM >
inline void
get_row_cf ( FLT * data_n,
             Matrix_Square< FLT, DIM > const & mat_n,
             std::size_t index_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    data_n[ ii ] = mat_n.x ( ii * DIM + index_n );
  }
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
get_row_rf ( Matrix_Square< FLT, DIM > const & mat_n, std::size_t index_n )
{
  Vector< FLT, DIM > vres;
  std::size_t const index ( index_n * DIM );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = mat_n.x ( index + ii );
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
get_row_cf ( Matrix_Square< FLT, DIM > const & mat_n, std::size_t index_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = mat_n.x ( ii * DIM + index_n );
  }
  return vres;
}

// Row setting

template < typename FLT, std::size_t DIM >
inline void
set_row_rf ( Matrix_Square< FLT, DIM > & mat_n,
             FLT const * data_n,
             std::size_t index_n )
{
  std::size_t index ( index_n * DIM );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    mat_n[ index + ii ] = data_n[ ii ];
  }
}

template < typename FLT, std::size_t DIM >
inline void
set_row_cf ( Matrix_Square< FLT, DIM > & mat_n,
             FLT const * data_n,
             std::size_t index_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    mat_n[ ii * DIM + index_n ] = data_n[ ii ];
  }
}

template < typename FLT, std::size_t DIM >
inline void
set_row_rf ( Matrix_Square< FLT, DIM > & mat_n,
             double value_n,
             std::size_t index_n )
{
  std::size_t index ( index_n * DIM );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    mat_n[ index + ii ] = value_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
set_row_cf ( Matrix_Square< FLT, DIM > & mat_n,
             double value_n,
             std::size_t index_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    mat_n[ ii * DIM + index_n ] = value_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
set_row_rf ( Matrix_Square< FLT, DIM > & mat_n,
             Vector< FLT, DIM > const & vec_n,
             std::size_t index_n )
{
  set_row_rf ( mat_n, vec_n.begin (), index_n );
}

template < typename FLT, std::size_t DIM >
inline void
set_row_cf ( Matrix_Square< FLT, DIM > & mat_n,
             Vector< FLT, DIM > const & vec_n,
             std::size_t index_n )
{
  set_row_cf ( mat_n, vec_n.begin (), index_n );
}

// Row swapping

template < typename FLT, std::size_t DIM >
inline void
swap_rows_rf ( Matrix_Square< FLT, DIM > & mat_n,
               std::size_t index0_n,
               std::size_t index1_n )
{
  std::size_t index0 ( index0_n * DIM );
  std::size_t index1 ( index1_n * DIM );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    std::swap ( mat_n.xr ( index0 ), mat_n.xr ( index1 ) );
    ++index0;
    ++index1;
  }
}

template < typename FLT, std::size_t DIM >
inline void
swap_rows_cf ( Matrix_Square< FLT, DIM > & mat_n,
               std::size_t index0_n,
               std::size_t index1_n )
{
  std::size_t index0 ( index0_n );
  std::size_t index1 ( index1_n );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    std::swap ( mat_n.xr ( index0 ), mat_n.xr ( index1 ) );
    index0 += DIM;
    index1 += DIM;
  }
}

// Column getting

template < typename FLT, std::size_t DIM >
inline void
get_column_rf ( FLT * data_n,
                Matrix_Square< FLT, DIM > const & mat_n,
                std::size_t index_n )
{
  get_row_cf ( data_n, mat_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline void
get_column_cf ( FLT * data_n,
                Matrix_Square< FLT, DIM > const & mat_n,
                std::size_t index_n )
{
  get_row_rf ( data_n, mat_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
get_column_rf ( Matrix_Square< FLT, DIM > const & mat_n, std::size_t index_n )
{
  return get_row_cf ( mat_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
get_column_cf ( Matrix_Square< FLT, DIM > const & mat_n, std::size_t index_n )
{
  return get_row_rf ( mat_n, index_n );
}

// Column setting

template < typename FLT, std::size_t DIM >
inline void
set_column_rf ( Matrix_Square< FLT, DIM > & mat_n,
                FLT const * data_n,
                std::size_t index_n )
{
  set_row_cf ( mat_n, data_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline void
set_column_cf ( Matrix_Square< FLT, DIM > & mat_n,
                FLT const * data_n,
                std::size_t index_n )
{
  set_row_rf ( mat_n, data_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline void
set_column_rf ( Matrix_Square< FLT, DIM > & mat_n,
                double value_n,
                std::size_t index_n )
{
  set_row_cf ( mat_n, value_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline void
set_column_cf ( Matrix_Square< FLT, DIM > & mat_n,
                FLT value_n,
                std::size_t index_n )
{
  set_row_rf ( mat_n, value_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline void
set_column_rf ( Matrix_Square< FLT, DIM > & mat_n,
                Vector< FLT, DIM > const & vec_n,
                std::size_t index_n )
{
  set_row_cf ( mat_n, vec_n, index_n );
}

template < typename FLT, std::size_t DIM >
inline void
set_column_cf ( Matrix_Square< FLT, DIM > & mat_n,
                Vector< FLT, DIM > const & vec_n,
                std::size_t index_n )
{
  set_row_rf ( mat_n, vec_n, index_n );
}

// Column swapping

template < typename FLT, std::size_t DIM >
inline void
swap_columns_rf ( Matrix_Square< FLT, DIM > & mat_n,
                  std::size_t index0_n,
                  std::size_t index1_n )
{
  mat_cf_swap_rows ( mat_n, index0_n, index1_n );
}

template < typename FLT, std::size_t DIM >
inline void
swap_columns_cf ( Matrix_Square< FLT, DIM > & mat_n,
                  std::size_t index0_n,
                  std::size_t index1_n )
{
  mat_rf_swap_rows ( mat_n, index0_n, index1_n );
}

} // namespace sev::lag

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "math_array.hpp"
#include "vector_class.hpp"
#include <cstdint>

namespace sev::lag
{

template < typename FLT, std::size_t DIM >
inline bool
match ( Vector< FLT, DIM > const & vec0_n,
        Vector< FLT, DIM > const & vec1_n,
        FLT epsilon_n )
{
  return array_match ( vec0_n.begin (), vec1_n.begin (), DIM, epsilon_n );
}

template < typename FLT, std::size_t DIM >
inline bool
almost_zero ( Vector< FLT, DIM > const & vec_n, FLT epsilon_n )
{
  return array_almost_zero ( vec_n.begin (), DIM, epsilon_n );
}

template < typename FLT, std::size_t DIM >
inline void
adjust_almost_zero ( Vector< FLT, DIM > & vec_n, FLT epsilon_n )
{
  array_adjust_almost_zero ( vec_n.begin (), DIM, epsilon_n );
}

template < typename FLT, std::size_t DIM >
inline FLT
magnitude2 ( Vector< FLT, DIM > const & vec_n )
{
  FLT res ( square ( vec_n.x ( 0 ) ) );
  for ( std::size_t ii = 1; ii != DIM; ++ii ) {
    res += square ( vec_n.x ( ii ) );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
inline double
magnitude ( Vector< FLT, DIM > const & vec_n )
{
  return square_root ( magnitude2 ( vec_n ) );
}

template < typename FLT, std::size_t DIM >
inline void
invert ( Vector< FLT, DIM > & vec_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vec_n[ ii ] = -vec_n[ ii ];
  }
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
inverted ( Vector< FLT, DIM > const & vsrc_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = -vsrc_n[ ii ];
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
added ( Vector< FLT, DIM > const & vsrc_n, FLT value_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] + value_n;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
added ( Vector< FLT, DIM > const & vsrc_n, Vector< FLT, DIM > const & vop_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] + vop_n[ ii ];
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
subtracted ( Vector< FLT, DIM > const & vsrc_n, FLT value_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] - value_n;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
subtracted ( Vector< FLT, DIM > const & vsrc_n,
             Vector< FLT, DIM > const & vop_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] - vop_n[ ii ];
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
multiplied ( Vector< FLT, DIM > const & vsrc_n, FLT value_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] * value_n;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
multiplied ( Vector< FLT, DIM > const & vsrc_n,
             Vector< FLT, DIM > const & vop_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] * vop_n[ ii ];
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
divided ( Vector< FLT, DIM > const & vsrc_n, FLT value_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] / value_n;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
divided ( Vector< FLT, DIM > const & vsrc_n, Vector< FLT, DIM > const & vop_n )
{
  Vector< FLT, DIM > vres;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] / vop_n[ ii ];
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline void
scale_to_magnitude ( Vector< FLT, DIM > & vres_n, FLT mag_n )
{
  FLT rscale ( mag_n / magnitude ( vres_n ) );
  vres_n *= rscale;
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
scaled_to_magnitude ( Vector< FLT, DIM > const & vsrc_n, FLT mag_n )
{
  Vector< FLT, DIM > vres;
  FLT rscale ( mag_n / magnitude ( vsrc_n ) );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] * rscale;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline void
normalize ( Vector< FLT, DIM > & vec_n )
{
  vec_n /= magnitude ( vec_n );
}

template < typename FLT, std::size_t DIM >
inline Vector< FLT, DIM >
normalized ( Vector< FLT, DIM > const & vsrc_n )
{
  Vector< FLT, DIM > vres;
  double mag ( magnitude ( vsrc_n ) );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    vres[ ii ] = vsrc_n[ ii ] / mag;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline double
dot_prod ( Vector< FLT, DIM > const & v0_n, Vector< FLT, DIM > const & v1_n )
{
  FLT res ( v0_n.x ( 0 ) * v1_n.x ( 0 ) );
  for ( std::size_t ii = 1; ii != DIM; ++ii ) {
    res += v0_n.x ( ii ) * v1_n.x ( ii );
  }
  return res;
}

} // namespace sev::lag

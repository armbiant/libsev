/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "matrix_square_class.hpp"
#include <cstdint>
#include <istream>
#include <ostream>

namespace sev::lag
{

// Stream operators

template < typename FLT, std::size_t DIM >
std::ostream &
operator<< ( std::ostream & ostr_n, Matrix_Square< FLT, DIM > const & mat_n )
{
  const int width ( ostr_n.width () );
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    if ( ii != 0 ) {
      ostr_n << ' ';
      ostr_n.width ( width );
    }
    ostr_n << mat_n.x ( ii );
  }
  return ostr_n;
}

template < typename FLT, std::size_t DIM >
std::istream &
operator>> ( std::istream & istr_n, Matrix_Square< FLT, DIM > & mat_n )
{
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    istr_n >> mat_n.xr ( ii );
  }
  return istr_n;
}

// Row printing

template < typename FLT, std::size_t DIM >
void
print_row_rf ( std::ostream & ostr_n,
               Matrix_Square< FLT, DIM > const & mat_n,
               std::size_t row_n )
{
  const int width ( ostr_n.width () );
  const std::size_t ibeg ( row_n * DIM );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( ii != 0 ) {
      ostr_n << ' ';
      ostr_n.width ( width );
    }
    ostr_n << mat_n.x ( ibeg + ii );
  }
}

template < typename FLT, std::size_t DIM >
void
print_row_cf ( std::ostream & ostr_n,
               Matrix_Square< FLT, DIM > const & mat_n,
               std::size_t row_n )
{
  const int width ( ostr_n.width () );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( ii != 0 ) {
      ostr_n << ' ';
      ostr_n.width ( width );
    }
    ostr_n << mat_n.x ( row_n + ( ii * DIM ) );
  }
}

// Rows printing

template < typename FLT, std::size_t DIM >
void
print_rows_rf ( std::ostream & ostr_n, Matrix_Square< FLT, DIM > const & mat_n )
{
  const int width ( ostr_n.width () );
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    if ( ii != 0 ) {
      ostr_n.width ( width );
    }
    ostr_n << mat_n.x ( ii );
    if ( ( ii % DIM ) == ( DIM - 1 ) ) {
      ostr_n << "\n";
    } else {
      ostr_n << ' ';
    }
  }
}

template < typename FLT, std::size_t DIM >
void
print_rows_cf ( std::ostream & ostr_n, Matrix_Square< FLT, DIM > const & mat_n )
{
  const int width ( ostr_n.width () );
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    if ( ii != 0 ) {
      ostr_n.width ( width );
    }
    const std::size_t col ( ii % DIM );
    const std::size_t row ( ii / DIM );
    ostr_n << mat_n.x ( col * DIM + row );
    if ( col == ( DIM - 1 ) ) {
      ostr_n << "\n";
    } else {
      ostr_n << ' ';
    }
  }
}

} // namespace sev::lag

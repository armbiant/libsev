/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

namespace sev::lag::init
{

/// @brief Dummy struct that signals that no initialization should be done.
struct None
{
};

/// @brief Dummy struct that signals that all values should be initialized to
/// zero
struct Zero
{
};

/// @brief Dummy struct that signals that all values should be initialized to
/// one
struct One
{
};

/// @brief Dummy struct that signals that identity initialization should be
/// done.
struct Identity
{
};

/// @brief Dummy struct that signals that all values should be filled
template < typename FLT >
class Fill
{
  public:
  // -- Construction
  constexpr Fill ( FLT value_n )
  : value ( value_n )
  {
  }

  public:
  // -- Attributes
  const FLT value;
};

// -- Instances

/// @brief An instance of the Init_Not dummy signal struct
///
/// @see Init_Not
constexpr None none = {};

/// @brief An instance of the Init_Zero dummy signal struct
///
/// @see Init_Zero
constexpr Zero zero = {};

/// @brief An instance of the Init_One dummy signal struct
///
/// @see Init_One
constexpr One one = {};

/// @brief An instance of Init_Identity the dummy signal struct
///
/// @see Init_Identity
constexpr Identity identity = {};

} // namespace sev::lag::init

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/lag/init.hpp>
#include <array>
#include <cstddef>
#include <iterator>

namespace sev::lag
{

/// @brief A quadratic matrix class (template)
///
template < typename FLT, std::size_t DIM >
class Matrix_Square
{
  public:
  // -- Types
  static constexpr const std::size_t dim = DIM;
  static constexpr const std::size_t vrows = DIM;
  static constexpr const std::size_t vcols = DIM;
  static constexpr const std::size_t vsize = DIM * DIM;
  using value_type = FLT;
  using iterator = value_type *;
  using const_iterator = value_type const *;
  using reverse_iterator = std::reverse_iterator< iterator >;
  using const_reverse_iterator = std::reverse_iterator< const_iterator >;
  using value_array = value_type[ vsize ];
  using std_array = std::array< value_type, vsize >;
  using mat = Matrix_Square< value_type, DIM >;

  // -- Constructors

  /// @brief Komponents are value (zero) initialized
  constexpr Matrix_Square () = default;

  constexpr Matrix_Square ( mat const & ) = default;

  constexpr Matrix_Square ( mat && ) = default;

  constexpr Matrix_Square ( const value_array & array_n )
  {
    assign ( array_n );
  }

  constexpr Matrix_Square ( const std_array & array_n ) { assign ( array_n ); }

  constexpr explicit Matrix_Square ( const init::Zero & init_n
                                     [[maybe_unused]] )
  {
    fill ( 0.0 );
  }

  constexpr explicit Matrix_Square ( const init::One & init_n [[maybe_unused]] )
  {
    fill ( 1.0 );
  }

  constexpr explicit Matrix_Square ( init::Fill< value_type > init_n )
  {
    fill ( init_n.value );
  }

  constexpr explicit Matrix_Square ( const init::Identity & init_n
                                     [[maybe_unused]] )
  {
    set_identity ();
  }

  // -- Assignment

  constexpr void
  fill ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] = value_n;
    }
  }

  constexpr void
  set_identity ();

  constexpr void
  assign ( const value_array & array_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] = array_n[ ii ];
    }
  }

  constexpr void
  assign ( const std_array & array_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] = array_n[ ii ];
    }
  }

  constexpr void
  assign ( const mat & mat_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] = mat_n[ ii ];
    }
  }

  constexpr mat &
  operator= ( const value_array & array_n )
  {
    assign ( array_n );
    return *this;
  }

  constexpr mat &
  operator= ( const std_array & array_n )
  {
    assign ( array_n );
    return *this;
  }

  constexpr mat &
  operator= ( mat const & ) = default;

  constexpr mat &
  operator= ( mat && ) = default;

  // -- Meta information

  /// @brief Number of elements in the matrix (DIM*DIM)
  static constexpr std::size_t
  size ()
  {
    return vsize;
  }

  // -- Component accessors

  constexpr value_type const &
  x ( std::size_t index_n ) const
  {
    return _xx[ index_n ];
  }

  constexpr value_type &
  xr ( std::size_t index_n )
  {
    return _xx[ index_n ];
  }

  constexpr void
  set ( std::size_t index_n, value_type value_n )
  {
    _xx[ index_n ] = value_n;
  }

  // -- Array accessors

  constexpr value_array &
  data ()
  {
    return _xx;
  }

  constexpr const value_array &
  data () const
  {
    return _xx;
  }

  // -- Iteration

  constexpr iterator
  begin ()
  {
    return ( &_xx[ 0 ] );
  }

  constexpr iterator
  end ()
  {
    return ( &_xx[ vsize ] );
  }

  constexpr const_iterator
  begin () const
  {
    return ( &_xx[ 0 ] );
  }

  constexpr const_iterator
  end () const
  {
    return ( &_xx[ vsize ] );
  }

  constexpr const_iterator
  cbegin () const
  {
    return ( &_xx[ 0 ] );
  }

  constexpr const_iterator
  cend () const
  {
    return ( &_xx[ vsize ] );
  }

  // -- Component access operators

  constexpr value_type const &
  operator[] ( std::size_t index_n ) const
  {
    return _xx[ index_n ];
  }

  constexpr value_type &
  operator[] ( std::size_t index_n )
  {
    return _xx[ index_n ];
  }

  // -- Scalar arithmetics operators

  constexpr mat &
  operator+= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] += value_n;
    }
    return *this;
  }

  constexpr mat &
  operator-= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] -= value_n;
    }
    return *this;
  }

  constexpr mat &
  operator*= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] *= value_n;
    }
    return *this;
  }

  constexpr mat &
  operator/= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] /= value_n;
    }
    return *this;
  }

  // -- Matrix arithmetics operators

  constexpr mat &
  operator+= ( mat const & mat_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] += mat_n._xx[ ii ];
    }
    return *this;
  }

  constexpr mat &
  operator-= ( mat const & mat_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] -= mat_n._xx[ ii ];
    }
    return *this;
  }

  constexpr mat &
  operator*= ( mat const & mat_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] *= mat_n._xx[ ii ];
    }
    return *this;
  }

  constexpr mat &
  operator/= ( mat const & mat_n )
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      _xx[ ii ] /= mat_n._xx[ ii ];
    }
    return *this;
  }

  // -- Comparison operators

  constexpr bool
  operator== ( mat const & mat_n ) const
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      if ( _xx[ ii ] != mat_n._xx[ ii ] ) {
        return false;
      }
    }
    return true;
  }

  constexpr bool
  operator!= ( mat const & mat_n ) const
  {
    for ( std::size_t ii = 0; ii != vsize; ++ii ) {
      if ( _xx[ ii ] != mat_n._xx[ ii ] ) {
        return true;
      }
    }
    return false;
  }

  private:
  value_array _xx = {};
};

template < typename FLT, std::size_t DIM >
constexpr void
Matrix_Square< FLT, DIM >::set_identity ()
{
  constexpr FLT zero = 0.0;
  constexpr FLT one = 1.0;
  FLT * ptr = &_xx[ 0 ];
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    for ( std::size_t jj = 0; jj != ii; ++jj ) {
      *ptr = zero;
      ++ptr;
    }
    *ptr = one;
    ++ptr;
    for ( std::size_t jj = ii + 1; jj != DIM; ++jj ) {
      *ptr = zero;
      ++ptr;
    }
  }
}

// -- External Operators - Matrix * Matrix

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM > &
operator+ ( Matrix_Square< FLT, DIM > & mat_n )
{
  return mat_n;
}

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM > const &
operator+ ( Matrix_Square< FLT, DIM > const & mat_n )
{
  return mat_n;
}

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM >
operator- ( Matrix_Square< FLT, DIM > const & mat_n )
{
  Matrix_Square< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    res._xx[ ii ] = -mat_n._xx[ ii ];
  }
  return res;
}

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM >
operator+ ( Matrix_Square< FLT, DIM > const & m_0,
            Matrix_Square< FLT, DIM > const & m_1 )
{
  Matrix_Square< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    res._xx[ ii ] = ( m_0._xx[ ii ] + m_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM >
operator- ( Matrix_Square< FLT, DIM > const & m_0,
            Matrix_Square< FLT, DIM > const & m_1 )
{
  Matrix_Square< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    res._xx[ ii ] = ( m_0._xx[ ii ] - m_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM >
operator* ( Matrix_Square< FLT, DIM > const & m_0,
            Matrix_Square< FLT, DIM > const & m_1 )
{
  Matrix_Square< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    res._xx[ ii ] = ( m_0._xx[ ii ] * m_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT, std::size_t DIM >
inline Matrix_Square< FLT, DIM >
operator/ ( Matrix_Square< FLT, DIM > const & m_0,
            Matrix_Square< FLT, DIM > const & m_1 )
{
  Matrix_Square< FLT, DIM > res;
  for ( std::size_t ii = 0; ii != ( DIM * DIM ); ++ii ) {
    res._xx[ ii ] = ( m_0._xx[ ii ] / m_1._xx[ ii ] );
  }
  return res;
}

// -- Types

template < typename FLT >
using Matrix2x2 = Matrix_Square< FLT, 2 >;
using Matrix2x2f = Matrix2x2< float >;
using Matrix2x2d = Matrix2x2< double >;

template < typename FLT >
using Matrix3x3 = Matrix_Square< FLT, 3 >;
using Matrix3x3f = Matrix3x3< float >;
using Matrix3x3d = Matrix3x3< double >;

template < typename FLT >
using Matrix4x4 = Matrix_Square< FLT, 4 >;
using Matrix4x4f = Matrix4x4< float >;
using Matrix4x4d = Matrix4x4< double >;

} // namespace sev::lag

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "vector_class.hpp"
#include "vector_math.hpp"

namespace sev::lag
{

template < typename FLT >
inline constexpr Vector3< FLT >
cross_prod ( Vector3< FLT > const & v0_n, Vector3< FLT > const & v1_n )
{
  return Vector3< FLT > ( { v0_n[ 1 ] * v1_n[ 2 ] - v0_n[ 2 ] * v1_n[ 1 ],
                            v0_n[ 2 ] * v1_n[ 0 ] - v0_n[ 0 ] * v1_n[ 2 ],
                            v0_n[ 0 ] * v1_n[ 1 ] - v0_n[ 1 ] * v1_n[ 0 ] } );
}

template < typename FLT >
inline constexpr Vector3< FLT >
cross_prod_3 ( Vector3< FLT > const & v0_n,
               Vector3< FLT > const & v1_n,
               Vector3< FLT > const & v2_n )
{
  Vector3< FLT > vv[ 2 ];
  vv[ 0 ] = v1_n;
  vv[ 0 ] -= v0_n;
  vv[ 1 ] = v2_n;
  vv[ 1 ] -= v0_n;
  return cross_prod ( vv[ 0 ], vv[ 1 ] );
}

template < typename FLT >
inline constexpr Vector3< FLT >
cross_prod_3 ( Vector3< FLT > const ( &v3_n )[ 3 ] )
{
  return cross_prod_3 ( v3_n[ 0 ], v3_n[ 1 ], v3_n[ 2 ] );
}

template < typename FLT >
inline constexpr void
rotate_x0 ( Vector3< FLT > & vec_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );

  FLT tmp1 = vec_n[ 1 ] * cn - vec_n[ 2 ] * sn;
  FLT tmp2 = vec_n[ 1 ] * sn + vec_n[ 2 ] * cn;

  vec_n[ 1 ] = tmp1;
  vec_n[ 2 ] = tmp2;
}

template < typename FLT >
inline constexpr void
rotate_x1 ( Vector3< FLT > & vec_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );

  FLT const tmp0 ( vec_n[ 2 ] * sn + vec_n[ 0 ] * cn );
  FLT const tmp2 ( vec_n[ 2 ] * cn - vec_n[ 0 ] * sn );

  vec_n[ 0 ] = tmp0;
  vec_n[ 2 ] = tmp2;
}

template < typename FLT >
inline constexpr void
rotate_x2 ( Vector3< FLT > & vec_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );

  FLT const tmp0 ( vec_n[ 0 ] * cn - vec_n[ 1 ] * sn );
  FLT const tmp1 ( vec_n[ 0 ] * sn + vec_n[ 1 ] * cn );

  vec_n[ 0 ] = tmp0;
  vec_n[ 1 ] = tmp1;
}

template < typename FLT >
inline constexpr void
rotate_x0_90 ( Vector3< FLT > & vec_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return;
  } else if ( times_n == 1 ) {
    std::swap ( vec_n[ 1 ], vec_n[ 2 ] );
    vec_n[ 1 ] = -vec_n[ 1 ];
  } else if ( times_n == 2 ) {
    vec_n[ 1 ] = -vec_n[ 1 ];
    vec_n[ 2 ] = -vec_n[ 2 ];
  } else {
    std::swap ( vec_n[ 1 ], vec_n[ 2 ] );
    vec_n[ 2 ] = -vec_n[ 2 ];
  }
}

template < typename FLT >
inline constexpr void
rotate_x1_90 ( Vector3< FLT > & vec_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return;
  } else if ( times_n == 1 ) {
    std::swap ( vec_n[ 2 ], vec_n[ 0 ] );
    vec_n[ 2 ] = -vec_n[ 2 ];
  } else if ( times_n == 2 ) {
    vec_n[ 0 ] = -vec_n[ 0 ];
    vec_n[ 2 ] = -vec_n[ 2 ];
  } else {
    std::swap ( vec_n[ 2 ], vec_n[ 0 ] );
    vec_n[ 0 ] = -vec_n[ 0 ];
  }
}

template < typename FLT >
inline constexpr void
rotate_x2_90 ( Vector3< FLT > & vec_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return;
  } else if ( times_n == 1 ) {
    std::swap ( vec_n[ 0 ], vec_n[ 1 ] );
    vec_n[ 0 ] = -vec_n[ 0 ];
  } else if ( times_n == 2 ) {
    vec_n[ 0 ] = -vec_n[ 0 ];
    vec_n[ 1 ] = -vec_n[ 1 ];
  } else {
    std::swap ( vec_n[ 0 ], vec_n[ 1 ] );
    vec_n[ 1 ] = -vec_n[ 1 ];
  }
}

template < typename FLT >
inline constexpr void
rotate_axis ( Vector3< FLT > & vec_n,
              Vector3< FLT > const & vaxis_n,
              FLT rad_n )
{
  Vector3< FLT > tmp;
  rotated_axis ( tmp, vec_n, vaxis_n, rad_n );
  vec_n = tmp;
}

template < typename FLT >
inline constexpr void
rotated_x0 ( Vector3< FLT > & vres_n, Vector3< FLT > const & vsrc_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );

  vres_n[ 0 ] = vsrc_n[ 0 ];
  vres_n[ 1 ] = vsrc_n[ 1 ] * cn - vsrc_n[ 2 ] * sn;
  vres_n[ 2 ] = vsrc_n[ 1 ] * sn + vsrc_n[ 2 ] * cn;
}

template < typename FLT >
inline constexpr void
rotated_x1 ( Vector3< FLT > & vres_n, Vector3< FLT > const & vsrc_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );

  vres_n[ 0 ] = vsrc_n[ 2 ] * sn + vsrc_n[ 0 ] * cn;
  vres_n[ 1 ] = vsrc_n[ 1 ];
  vres_n[ 2 ] = vsrc_n[ 2 ] * cn - vsrc_n[ 0 ] * sn;
}

template < typename FLT >
inline constexpr void
rotated_x2 ( Vector3< FLT > & vres_n, Vector3< FLT > const & vsrc_n, FLT rad_n )
{
  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );

  vres_n[ 0 ] = vsrc_n[ 0 ] * cn - vsrc_n[ 1 ] * sn;
  vres_n[ 1 ] = vsrc_n[ 0 ] * sn + vsrc_n[ 1 ] * cn;
  vres_n[ 2 ] = vsrc_n[ 2 ];
}

template < typename FLT >
inline constexpr Vector3< FLT >
rotated_x0_90 ( Vector3< FLT > const & vsrc_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return vsrc_n;
  } else if ( times_n == 1 ) {
    return Vector3< FLT > ( { vsrc_n[ 0 ], -vsrc_n[ 2 ], vsrc_n[ 1 ] } );
  } else if ( times_n == 2 ) {
    return Vector3< FLT > ( { vsrc_n[ 0 ], -vsrc_n[ 1 ], -vsrc_n[ 2 ] } );
  }
  return Vector3< FLT > ( { vsrc_n[ 0 ], vsrc_n[ 2 ], -vsrc_n[ 1 ] } );
}

template < typename FLT >
inline constexpr Vector3< FLT >
rotated_x1_90 ( Vector3< FLT > const & vsrc_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return vsrc_n;
  } else if ( times_n == 1 ) {
    return Vector3< FLT > ( { vsrc_n[ 2 ], vsrc_n[ 1 ], -vsrc_n[ 0 ] } );
  } else if ( times_n == 2 ) {
    return Vector3< FLT > ( { -vsrc_n[ 0 ], vsrc_n[ 1 ], -vsrc_n[ 2 ] } );
  }
  return Vector3< FLT > ( { -vsrc_n[ 2 ], vsrc_n[ 1 ], vsrc_n[ 0 ] } );
}

template < typename FLT >
inline constexpr Vector3< FLT >
rotated_x2_90 ( Vector3< FLT > const & vsrc_n, int times_n )
{
  if ( times_n < 0 ) {
    times_n = 4 - ( ( -times_n ) % 4 );
  } else {
    times_n %= 4;
  }

  if ( times_n == 0 ) {
    return vsrc_n;
  } else if ( times_n == 1 ) {
    return Vector3< FLT > ( { -vsrc_n[ 1 ], vsrc_n[ 0 ], vsrc_n[ 2 ] } );
  } else if ( times_n == 2 ) {
    return Vector3< FLT > ( { -vsrc_n[ 0 ], -vsrc_n[ 1 ], vsrc_n[ 2 ] } );
  }
  return Vector3< FLT > ( { vsrc_n[ 1 ], -vsrc_n[ 0 ], vsrc_n[ 2 ] } );
}

template < typename FLT >
inline constexpr Vector3< FLT >
rotated_axis ( Vector3< FLT > const & vsrc_n,
               Vector3< FLT > const & vaxis_n,
               FLT rad_n )
{
  Vector3< FLT > vres;

  FLT const sn ( sin ( rad_n ) );
  FLT const cn ( cos ( rad_n ) );
  FLT cn_1 ( FLT ( 1.0 ) - cn );

  {
    Vector3< FLT > tmp ( Init_Fill ( vaxis_n[ 0 ] * cn_1 ) );
    tmp *= vaxis_n;
    tmp[ 0 ] += cn;
    tmp[ 1 ] += -vaxis_n[ 2 ] * sn;
    tmp[ 2 ] += vaxis_n[ 1 ] * sn;
    vres[ 0 ] = dot_prod ( tmp, vsrc_n );
  }

  {
    Vector3< FLT > tmp ( Init_Fill ( vaxis_n[ 1 ] * cn_1 ) );
    tmp *= vaxis_n;
    tmp[ 0 ] += vaxis_n[ 2 ] * sn;
    tmp[ 1 ] += cn;
    tmp[ 2 ] += -vaxis_n[ 0 ] * sn;
    vres[ 1 ] = dot_prod ( tmp, vsrc_n );
  }

  {
    Vector3< FLT > tmp ( Init_Fill ( vaxis_n[ 2 ] * cn_1 ) );
    tmp *= vaxis_n;
    tmp[ 0 ] += -vaxis_n[ 1 ] * sn;
    tmp[ 1 ] += vaxis_n[ 0 ] * sn;
    tmp[ 2 ] += cn;
    vres[ 2 ] = dot_prod ( tmp, vsrc_n );
  }

  return vres;
}

} // namespace sev::lag

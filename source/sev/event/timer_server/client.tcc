/// libsev: C++ library for threads, math strings and trees.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/timer_server/client.hpp>
#include <sev/event/timer_server/detail/private.hpp>
#include <sev/event/timer_server/server.hpp>

namespace sev::event::timer_server
{

template < class CLK >
Client< CLK >::Client ()
{
}

template < class CLK >
Client< CLK >::Client ( Server * server_n )
{
  connect ( server_n );
}

template < class CLK >
Client< CLK >::~Client ()
{
  disconnect ();
}

template < class CLK >
bool
Client< CLK >::connect ( Server * server_n )
{
  disconnect ();

  if ( server_n != nullptr ) {
    _server_private = server_n->_private.get ();
    if ( _server_private != nullptr ) {
      _server_entry = server_n->_private->client_register ();
      // Check if a valid server entry was returned
      if ( _server_entry == nullptr ) {
        // Fail
        _server_private = nullptr;
      }
    }
  }
  return is_connected ();
}

template < class CLK >
void
Client< CLK >::disconnect ()
{
  if ( _server_private == nullptr ) {
    return;
  }
  _server_private->client_unregister ( _server_entry );
  _server_entry = nullptr;
  _server_private = nullptr;
}

template < class CLK >
void
Client< CLK >::set_point ( const Time_Point & time_point_n )
{
  stop ();
  _point = time_point_n;
}

template < class CLK >
void
Client< CLK >::adjust_point ( const Duration & duration_n )
{
  stop ();
  _point += duration_n;
}

template < class CLK >
void
Client< CLK >::set_duration ( const Duration & duration_n )
{
  stop ();
  _duration = duration_n;
}

template < class CLK >
bool
Client< CLK >::is_running ()
{
  if ( !is_connected () ) {
    return false;
  }
  return _server_entry->ext_is_running ();
}

template < class CLK >
bool
Client< CLK >::is_running_single ()
{
  if ( !is_connected () ) {
    return false;
  }
  return _server_entry->ext_is_running_single ();
}

template < class CLK >
bool
Client< CLK >::is_running_period ()
{
  if ( !is_connected () ) {
    return false;
  }
  return _server_entry->ext_is_running_period ();
}

template < class CLK >
void
Client< CLK >::start_single_point ()
{
  if ( !is_connected () ) {
    return;
  }
  _server_private->client_insert_point ( _server_entry, _point );
}

template < class CLK >
void
Client< CLK >::start_single_point ( const Time_Point & time_point_n )
{
  if ( !is_connected () ) {
    return;
  }
  _point = time_point_n;
  _server_private->client_insert_point ( _server_entry, _point );
}

template < class CLK >
void
Client< CLK >::start_single_period ( const Time_Point & time_point_n )
{
  if ( !is_connected () ) {
    return;
  }
  _point = time_point_n;
  _point += _duration;
  _server_private->client_insert_point ( _server_entry, _point );
}

template < class CLK >
void
Client< CLK >::start_single_period_next ()
{
  if ( !is_connected () ) {
    return;
  }
  _point += _duration;
  _server_private->client_insert_point ( _server_entry, _point );
}

template < class CLK >
void
Client< CLK >::start_single_period_now ()
{
  if ( !is_connected () ) {
    return;
  }
  _point = now ();
  _point += _duration;
  _server_private->client_insert_point ( _server_entry, _point );
}

template < class CLK >
void
Client< CLK >::start_periodical_at_point ()
{
  if ( !is_connected () ) {
    return;
  }
  _server_private->client_insert_period ( _server_entry, _point, _duration );
}

template < class CLK >
void
Client< CLK >::start_periodical_at ( const Time_Point & time_point_n )
{
  if ( !is_connected () ) {
    return;
  }
  _point = time_point_n;
  _server_private->client_insert_period ( _server_entry, _point, _duration );
}

template < class CLK >
void
Client< CLK >::start_periodical_next ()
{
  if ( !is_connected () ) {
    return;
  }
  _point += _duration;
  _server_private->client_insert_period ( _server_entry, _point, _duration );
}

template < class CLK >
void
Client< CLK >::start_periodical_now ( const Time_Point & time_now_n )
{
  if ( !is_connected () ) {
    return;
  }
  _point = time_now_n;
  _point += _duration;
  _server_private->client_insert_period ( _server_entry, _point, _duration );
}

template < class CLK >
void
Client< CLK >::start_periodical_now ()
{
  if ( !is_connected () ) {
    return;
  }
  _point = now ();
  _point += _duration;
  _server_private->client_insert_period ( _server_entry, _point, _duration );
}

template < class CLK >
void
Client< CLK >::stop ()
{
  _server_private->client_remove ( _server_entry );
}

template < class CLK >
void
Client< CLK >::set_callback ( std::function< void() > callback_n )
{
  _callback_notifier = std::move ( callback_n );
  if ( is_connected () ) {
    _server_private->client_set_callback ( _server_entry, _callback_notifier );
  }
}

} // namespace sev::event::timer_server

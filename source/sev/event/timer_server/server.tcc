/// libsev: C++ library for threads, math strings and trees.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/timer_server/detail/private.hpp>
#include <sev/event/timer_server/server.hpp>

namespace sev::event::timer_server
{

template < class CLK >
Server< CLK >::Server ( sev::thread::Tracker * thread_tracker_n )
: _private ( std::make_unique< Private > ( thread_tracker_n ) )
{
}

template < class CLK >
Server< CLK >::~Server ()
{
}

template < class CLK >
bool
Server< CLK >::start ()
{
  return _private->start ();
}

template < class CLK >
void
Server< CLK >::stop ()
{
  _private->stop ();
}

template < class CLK >
bool
Server< CLK >::is_running () const
{
  return _private->is_running ();
}

} // namespace sev::event::timer_server

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/event.hpp>
#include <sev/event/list.hpp>
#include <cstddef>

namespace sev::event
{

// -- Forward declaration
class Pool_Tracker;

/// @brief Event pool
///
class Pool_Base
{
  public:
  // -- Types

  static constexpr std::size_t capacity_default = 8;

  // -- Construction

  Pool_Base ( Pool_Tracker & tracker_n );

  Pool_Base ( const Pool_Base & pool_n ) = delete;

  Pool_Base ( Pool_Base && pool_n ) = delete;

  virtual ~Pool_Base ();

  // -- Setup

  /// @brief Clears all instances
  virtual void
  clear () = 0;

  // -- Tracker

  Pool_Tracker &
  tracker ()
  {
    return _tracker;
  }

  const Pool_Tracker &
  tracker () const
  {
    return _tracker;
  }

  // -- Statistics

  /// @return Number of items available in the pool
  ///
  std::size_t
  size () const
  {
    return _size;
  }

  /// @return Number of items that were allocated and constructed
  ///
  std::size_t
  size_owned () const
  {
    return _size_owned;
  }

  /// @brief Returns true if ( size() == 0 )
  bool
  is_empty () const
  {
    return _list.is_empty ();
  }

  /// @brief Returns true if ( size() == capacity() )
  bool
  is_full () const
  {
    return ( _size == _capacity );
  }

  /// @brief Returns true if ( size() == size_owned() )
  bool
  all_home () const
  {
    return ( _size == _size_owned );
  }

  /// @brief Returns ( size_owned() - size() )
  std::size_t
  size_outside () const
  {
    return ( _size_owned - _size );
  }

  /// @brief Returns ( size_owned() == capacity() )
  bool
  size_owned_eq_capacity () const
  {
    return ( _size_owned == _capacity );
  }

  /// @brief Returns ( size_owned() >= capacity() )
  bool
  size_owned_ge_capacity () const
  {
    return ( _size_owned >= _capacity );
  }

  // -- Capacity

  /// @return Maximum number of events the pool can keep available
  ///
  std::size_t
  capacity () const
  {
    return _capacity;
  }

  /// @brief Sets the capacity
  ///
  /// @return True on success
  virtual void
  set_capacity ( std::size_t capacity_n ) = 0;

  void
  ensure_minimum_capacity ( std::size_t capacity_n );

  void
  ensure_maximum_capacity ( std::size_t capacity_n );

  /// @brief Sets the capacity to 0
  void
  clear_capacity ();

  // -- Release

  virtual void
  casted_release_virtual ( Event * event_n ) = 0;

  // -- Reset release

  virtual void
  casted_reset_release_virtual ( Event * event_n ) = 0;

  protected:
  // -- Utility
  void
  setup_event_pool ( Event * event_n )
  {
    event_n->_pool = this;
  }

  protected:
  // -- Attributes
  sev::event::List _list;
  std::size_t _size = 0;
  std::size_t _size_owned = 0;
  std::size_t _capacity = 0;
  Pool_Tracker & _tracker;
};

} // namespace sev::event

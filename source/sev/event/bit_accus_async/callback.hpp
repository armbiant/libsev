/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>
#include <functional>
#include <mutex>
#include <utility>

namespace sev::event::bit_accus_async
{

/// @brief Notifies a callback std::function on events
///
class Callback
{
  public:
  // -- Types

  using Int_Type = std::uint_fast32_t;
  using Function_Type = std::function< void () >;

  // -- Construction

  Callback ()
  : _callback ( [] () {} )
  {
  }

  Callback ( Function_Type callback_n )
  : _callback ( std::move ( callback_n ) )
  {
  }

  ~Callback () = default;

  // -- Callback

  /// @brief The callback
  ///
  const Function_Type &
  callback () const
  {
    return _callback;
  }

  /// @brief Sets the callback
  ///
  /// @arg callback_n The callback
  /// @arg call_on_demand_n Selects whether the callback can be called
  ///      immediately by this method
  void
  set_callback ( Function_Type callback_n, bool call_on_demand_n = true );

  void
  clear_callback ();

  // -- Bit set/get interface

  void
  set ( Int_Type bits_n );

  /// @brief Read but don't clear the socket or the bits.
  ///
  Int_Type
  get ();

  /// @brief Fetch and clear the bits and the socket.
  ///
  Int_Type
  fetch_and_clear ();

  private:
  // -- Attributes
  std::mutex _mutex;
  Int_Type _bits = 0;
  Function_Type _callback;
};

} // namespace sev::event::bit_accus_async

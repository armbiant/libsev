/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/bit_accus_async/socket.hpp>
#include <sys/eventfd.h>
#include <poll.h>
#include <unistd.h>
#include <array>
#include <cstdint>

namespace sev::event::bit_accus_async
{

Socket::Socket ()
: _bits ( 0 )
{
  int retval = eventfd ( 0, EFD_NONBLOCK | EFD_SEMAPHORE );
  if ( retval >= 0 ) {
    _socket_fd.fill ( retval );
  }
}

Socket::~Socket ()
{
  fetch_and_clear ();
  if ( _socket_fd[ 0 ] == _socket_fd[ 1 ] ) {
    if ( _socket_fd[ 0 ] >= 0 ) {
      close ( _socket_fd[ 0 ] );
    }
  } else {
    for ( int fdi : _socket_fd ) {
      if ( fdi >= 0 ) {
        close ( fdi );
      }
    }
  }
}

short
Socket::socket_events () const
{
  return ( POLLIN | POLLPRI );
}

void
Socket::set ( Int_Type bits_n )
{
  if ( bits_n == 0 ) {
    return;
  }
  if ( _bits.fetch_or ( bits_n ) == 0 ) {
    // Add 1 to the semaphore
    std::int64_t const value = 1;
    static_assert ( sizeof ( value ) == 8,
                    "eventfd requires an 8 byte integer" );
    write ( _socket_fd[ 0 ], &value, sizeof ( value ) );
  }
}

Socket::Int_Type
Socket::fetch_and_clear ()
{
  Int_Type res = _bits.exchange ( 0 );
  if ( res != 0 ) {
    // Read 1 from the semaphore
    std::int64_t value = 0;
    static_assert ( sizeof ( value ) == 8,
                    "eventfd requires an 8 byte integer" );
    read ( _socket_fd[ 1 ], &value, sizeof ( value ) );
  }
  return res;
}

} // namespace sev::event::bit_accus_async

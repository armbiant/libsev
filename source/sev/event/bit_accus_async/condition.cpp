/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/bit_accus_async/condition.hpp>

namespace sev::event::bit_accus_async
{

Condition::Condition () = default;

Condition::~Condition () = default;

void
Condition::set ( Int_Type bits_n )
{
  std::size_t waiters = 0;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _bits |= bits_n;
    waiters = _waiters;
  }
  // Notify waiters
  if ( waiters != 0 ) {
    _cond.notify_all ();
  }
}

Condition::Int_Type
Condition::get ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  return _bits;
}

Condition::Int_Type
Condition::fetch_and_clear ()
{
  Int_Type res = 0;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _bits;
    _bits = 0;
  }
  return res;
}

Condition::Int_Type
Condition::fetch_and_clear_wait ()
{
  Int_Type res = 0;
  {
    std::unique_lock< std::mutex > lock ( _mutex );
    while ( true ) {
      if ( _bits != 0 ) {
        res = _bits;
        _bits = 0;
        break;
      }
      ++_waiters;
      _cond.wait ( lock );
      --_waiters;
    }
  }
  return res;
}

Condition::Events_Steady
Condition::fetch_and_clear_events (
    const std::chrono::steady_clock::time_point & tp_n )
{
  Events_Steady res;
  res.bits = fetch_and_clear ();
  res.now = std::chrono::steady_clock::now ();
  res.timeout = ( res.now >= tp_n );
  return res;
}

Condition::Events_System
Condition::fetch_and_clear_events (
    const std::chrono::system_clock::time_point & tp_n )
{
  Events_System res;
  res.bits = fetch_and_clear ();
  res.now = std::chrono::system_clock::now ();
  res.timeout = ( res.now >= tp_n );
  return res;
}

Condition::Events_Steady
Condition::fetch_and_clear_events_wait (
    const std::chrono::steady_clock::time_point & tp_n )
{
  return fetch_and_clear_events_wait_< std::chrono::steady_clock > ( tp_n );
}

Condition::Events_System
Condition::fetch_and_clear_events_wait (
    const std::chrono::system_clock::time_point & tp_n )
{
  return fetch_and_clear_events_wait_< std::chrono::system_clock > ( tp_n );
}

template < typename Clock >
inline Condition::Events< Clock >
Condition::fetch_and_clear_events_wait_ (
    typename Clock::time_point const & tp_n )
{
  Condition::Events< Clock > res;

  // Wait for events or timeout
  {
    std::unique_lock< std::mutex > lock ( _mutex );
    while ( true ) {
      // Leave when event bits were found
      if ( _bits != 0 ) {
        res.bits = _bits;
        _bits = 0;
        break;
      }

      // Wait for notification
      ++_waiters;
      auto wait_res = _cond.wait_until ( lock, tp_n );
      --_waiters;

      // Leave on timeout
      if ( wait_res == std::cv_status::timeout ) {
        break;
      }
    }
  }

  // Probe timeout
  res.now = Clock::now ();
  res.timeout = ( res.now >= tp_n );
  return res;
}

} // namespace sev::event::bit_accus_async

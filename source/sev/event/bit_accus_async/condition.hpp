/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <cstdint>
#include <mutex>

namespace sev::event::bit_accus_async
{

/// @brief Supports waiting at a std::condition_variable
///
class Condition
{
  public:
  // -- Types

  using Int_Type = std::uint_fast32_t;

  template < typename Clock >
  struct Events
  {
    Int_Type bits = 0;
    bool timeout = false;
    // @brief Current time. Only valid if timeout is true.
    typename Clock::time_point now;
  };

  using Events_Steady = Events< std::chrono::steady_clock >;
  using Events_System = Events< std::chrono::system_clock >;

  // -- Construction

  Condition ();

  ~Condition ();

  // -- Bit set/get interface

  void
  set ( Int_Type bits_n );

  /// @brief Reads the events bits but doesn't clear them.  Returns immediately.
  Int_Type
  get ();

  /// @brief Fetches and clears the events bits.  Returns immediately.
  Int_Type
  fetch_and_clear ();

  /// @brief Fetches bits or waits until bits are available.
  ///
  /// @return Fetched bits.
  Int_Type
  fetch_and_clear_wait ();

  /// @brief Fetch bits immediately or wait until bits appear
  ///
  Int_Type
  fetch_and_clear_wait ( bool wait_n )
  {
    return wait_n ? fetch_and_clear_wait () : fetch_and_clear ();
  }

  /// @brief Fetch bits and timeout if the clock time exceeded @a tp_n
  ///
  /// @return All fetched bits, a timeout flag and optionally the current time.
  Events_Steady
  fetch_and_clear_events ( const std::chrono::steady_clock::time_point & tp_n );

  /// @brief Fetch bits and timeout if the clock time exceeded @a tp_n
  ///
  /// @return All fetched bits, a timeout flag and optionally the current time.
  Events_System
  fetch_and_clear_events ( const std::chrono::system_clock::time_point & tp_n );

  /// @brief Fetch bits or wait until the clock time exceeds @a tp_n
  ///
  /// @return All fetched bits, a timeout flag and optionally the current time.
  Events_Steady
  fetch_and_clear_events_wait (
      const std::chrono::steady_clock::time_point & tp_n );

  /// @brief Fetch bits or wait until the clock time exceeds @a tp_n
  ///
  /// @return All fetched bits, a timeout flag and optionally the current time.
  Events_System
  fetch_and_clear_events_wait (
      const std::chrono::system_clock::time_point & tp_n );

  /// @brief Fetch bits imediately or wait until bits or a timeout appear
  ///
  Events_Steady
  fetch_and_clear_events_wait (
      const std::chrono::steady_clock::time_point & tp_n, bool wait_n )
  {
    return wait_n ? fetch_and_clear_events_wait ( tp_n )
                  : fetch_and_clear_events ( tp_n );
  }

  /// @brief Fetch bits imediately or wait until bits or a timeout appear
  ///
  Events_System
  fetch_and_clear_events_wait (
      const std::chrono::system_clock::time_point & tp_n, bool wait_n )
  {
    return wait_n ? fetch_and_clear_events_wait ( tp_n )
                  : fetch_and_clear_events ( tp_n );
  }

  private:
  // -- Utility

  /// @brief Private implementation template
  template < typename Clock >
  Events< Clock >
  fetch_and_clear_events_wait_ ( const typename Clock::time_point & tp_n );

  private:
  // -- Attributes
  mutable std::mutex _mutex;
  Int_Type _bits = 0;
  std::condition_variable _cond;
  std::size_t _waiters = 0;
};

} // namespace sev::event::bit_accus_async

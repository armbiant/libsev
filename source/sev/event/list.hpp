/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/event.hpp>
#include <sev/mem/list/se/list.hpp>

namespace sev::event
{

using List = sev::mem::list::se::List< Event >;

} // namespace sev::event

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/list.hpp>
#include <sev/event/queue/connection.hpp>
#include <cstddef>
#include <functional>
#include <type_traits>

// -- Forward declaration
namespace sev::event
{
class Event;
}
namespace sev::event::queue
{
template < std::size_t N >
class Reference_N;
}

namespace sev::event::queue
{

/// @brief Push connection to a  sev::event::Queue_N < N > with local event list
///
template < std::size_t N >
class Connection_Out_N : protected Connection_N< N >
{
  public:
  /// @brief Interface to one of the connection event lists
  ///
  class Channel
  {
    public:
    // -- Single event processing

    bool
    is_empty () const
    {
      return _list.is_empty ();
    }

    void
    push ( sev::event::Event * event_n )
    {
      _list.push_back ( event_n );
      _con.pushed ();
    }

    private:
    // -- Friends
    friend class Connection_Out_N< N >;

    // -- Constructors

    Channel ( Connection_Out_N< N > & con_n, sev::event::List & list_n )
    : _con ( con_n )
    , _list ( list_n )
    {
    }

    private:
    Connection_Out_N< N > & _con;
    sev::event::List & _list;
  };

  // -- Constructors

  Connection_Out_N ();

  Connection_Out_N ( const Connection_Out_N< N > & connection_n ) = delete;

  Connection_Out_N ( Connection_Out_N< N > && connection_n ) = delete;

  ~Connection_Out_N ();

  // -- Assignment operators

  Connection_Out_N< N > &
  operator= ( const Connection_Out_N< N > & connection_n ) = delete;

  Connection_Out_N< N > &
  operator= ( Connection_Out_N< N > && connection_n ) = delete;

  // -- Status

  using Connection_N< N >::is_empty;

  // -- General

  /// @brief Disconnects and clears the push notifiers
  void
  clear ();

  // -- Push notifier

  /// @brief Gets notified on the first push to a local channel()
  ///
  std::function< void () > const &
  push_notifier () const
  {
    return _push_notifier;
  }

  void
  set_push_notifier ( std::function< void () > const & notifier_n,
                      bool call_on_demand_n = true );

  void
  clear_push_notifier ();

  // -- Queue connection

  using Connection_N< N >::is_connected;
  using Connection_N< N >::queue;

  /// @brief Connects to a given queue
  ///
  /// @return True on success
  bool
  connect ( Queue_N< N > * queue_n );

  void
  disconnect ();

  // -- Queue feeding

  /// @brief Non blocking queue feeding
  ///
  /// @return True if any event was fed to the queue
  bool
  feed_into_queue ()
  {
    if ( !is_connected () || _is_empty ) {
      return false;
    }
    _is_empty = true;
    private_feed_into_queue ();
    return true;
  }

  // -- Channel interface

  /// @brief Returns the list interface
  template < typename = std::enable_if< ( N == 1 ) > >
  Channel
  channel ()
  {
    return Channel ( *this, Connection_N< N >::list_ref ( 0 ) );
  }

  /// @brief Returns a list interface
  Channel
  channel ( std::size_t index_n )
  {
    return Channel ( *this, Connection_N< N >::list_ref ( index_n ) );
  }

  // -- Single list single event processing

  template < typename = std::enable_if< ( N == 1 ) > >
  void
  push ( sev::event::Event * event_n )
  {
    Connection_N< N >::list_ref ( 0 ).push_back ( event_n );
    pushed ();
  }

  private:
  // -- Friends
  friend class Channel;

  // -- Utility

  using Connection_N< N >::queue_ref;

  void
  pushed ()
  {
    // Notify callback if this was the first push
    if ( _is_empty ) {
      _is_empty = false;
      _push_notifier ();
    }
  }

  void
  private_feed_into_queue ();

  private:
  bool _is_empty = true;
  std::function< void () > _push_notifier;
};

// -- Types
using Connection_Out = Connection_Out_N< 1 >;
using Connection_Out_1 = Connection_Out_N< 1 >;
using Connection_Out_2 = Connection_Out_N< 2 >;
using Connection_Out_3 = Connection_Out_N< 3 >;
using Connection_Out_4 = Connection_Out_N< 4 >;

} // namespace sev::event::queue

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "connection_in.hpp"
#include <sev/event/queue/connection.hpp>
#include <sev/event/queue/queue.hpp>

namespace sev::event::queue
{

template < std::size_t N >
Connection_In_N< N >::Connection_In_N ()
: _incoming_notifier ( [] () {} )
{
}

template < std::size_t N >
Connection_In_N< N >::~Connection_In_N ()
{
  disconnect ();
}

template < std::size_t N >
void
Connection_In_N< N >::clear ()
{
  disconnect ();
  _incoming_notifier = [] () {};
}

template < std::size_t N >
void
Connection_In_N< N >::set_incoming_notifier (
    std::function< void () > const & notifier_n )
{
  _incoming_notifier = notifier_n;
  if ( is_connected () ) {
    queue ()->set_push_notifier ( _incoming_notifier );
  }
}

template < std::size_t N >
void
Connection_In_N< N >::clear_incoming_notifier ()
{
  if ( is_connected () ) {
    queue ()->clear_push_notifier ();
  }
  _incoming_notifier = [] () {};
}

template < std::size_t N >
bool
Connection_In_N< N >::connect ( Reference_N< N > const & queue_n,
                                bool call_on_demand_n )
{
  disconnect ();
  if ( queue_n.is_valid () ) {
    queue_ref ().reset ( queue_n );
    queue ()->set_push_notifier ( _incoming_notifier, call_on_demand_n );
    return true;
  }
  return false;
}

template < std::size_t N >
void
Connection_In_N< N >::disconnect ()
{
  if ( is_connected () ) {
    queue ()->clear_push_notifier ();
    queue_ref ().clear ();
  }
}

template < std::size_t N >
bool
Connection_In_N< N >::read_from_queue ()
{
  if ( is_connected () ) {
    return queue ()->try_pop_lists ( Connection_N< N >::lists_ref () );
  }
  return false;
}

// -- Instantiation
template class Connection_In_N< 1 >;
template class Connection_In_N< 2 >;
template class Connection_In_N< 3 >;
template class Connection_In_N< 4 >;

} // namespace sev::event::queue

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/assert.hpp>
#include <sev/event/queue/connection.hpp>

namespace sev::event::queue
{

template < std::size_t N >
Connection_N< N >::Connection_N ()
{
}

template < std::size_t N >
Connection_N< N >::~Connection_N ()
{
  DEBUG_ASSERT ( is_empty () );
}

// -- Instantiation
template class Connection_N< 1 >;
template class Connection_N< 2 >;
template class Connection_N< 3 >;
template class Connection_N< 4 >;

} // namespace sev::event::queue

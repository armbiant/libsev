/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "connection_out.hpp"
#include <sev/event/queue/connection.hpp>
#include <sev/event/queue/queue.hpp>

namespace sev::event::queue
{

template < std::size_t N >
Connection_Out_N< N >::Connection_Out_N ()
: _push_notifier ( [] () {} )
{
}

template < std::size_t N >
Connection_Out_N< N >::~Connection_Out_N ()
{
}

template < std::size_t N >
void
Connection_Out_N< N >::clear ()
{
  disconnect ();
  _push_notifier = [] () {};
}

template < std::size_t N >
void
Connection_Out_N< N >::set_push_notifier (
    std::function< void () > const & notifier_n, bool call_on_demand_n )
{
  _push_notifier = notifier_n;
  if ( call_on_demand_n && !_is_empty ) {
    _push_notifier ();
  }
}

template < std::size_t N >
void
Connection_Out_N< N >::clear_push_notifier ()
{
  _push_notifier = [] () {};
}

template < std::size_t N >
bool
Connection_Out_N< N >::connect ( Queue_N< N > * queue_n )
{
  disconnect ();
  if ( queue_n == nullptr ) {
    return false;
  }
  queue_ref ().reset ( queue_n );
  return true;
}

template < std::size_t N >
void
Connection_Out_N< N >::disconnect ()
{
  if ( is_connected () ) {
    queue_ref ().clear ();
  }
}

template < std::size_t N >
void
Connection_Out_N< N >::private_feed_into_queue ()
{
  queue ()->push_lists ( Connection_N< N >::lists_ref () );
}

// -- Instantiation
template class Connection_Out_N< 1 >;
template class Connection_Out_N< 2 >;
template class Connection_Out_N< 3 >;
template class Connection_Out_N< 4 >;

} // namespace sev::event::queue

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/list.hpp>
#include <sev/event/queue/reference.hpp>
#include <sev/mem/list/se/list.hpp>
#include <array>
#include <cstddef>

namespace sev::event::queue
{

/// @brief Base class for a connection to a  sev::event::Queue_N < N > with
///        a local event list
///
template < std::size_t N >
class Connection_N
{
  public:
  // -- Types

  typedef std::array< sev::event::List, N > Lists;

  public:
  // -- Construcctors

  Connection_N ();

  Connection_N ( const Connection_N< N > & connection_n ) = delete;

  Connection_N ( Connection_N< N > && connection_n ) = delete;

  ~Connection_N ();

  // -- Lists

  bool
  is_empty () const;

  const sev::event::List &
  list ( std::size_t index_n ) const
  {
    return _lists[ index_n ];
  }

  // -- Queue

  const Reference_N< N > &
  queue () const
  {
    return _queue;
  }

  bool
  is_connected () const
  {
    return _queue.is_valid ();
  }

  // -- Operators

  Connection_N< N > &
  operator= ( const Connection_N< N > & connection_n ) = delete;

  Connection_N< N > &
  operator= ( Connection_N< N > && connection_n ) = delete;

  protected:
  sev::event::List &
  list_ref ( std::size_t index_n )
  {
    return _lists[ index_n ];
  }

  Lists &
  lists_ref ()
  {
    return _lists;
  }

  Reference_N< N > &
  queue_ref ()
  {
    return _queue;
  }

  private:
  Lists _lists;
  Reference_N< N > _queue;
};

template < std::size_t N >
inline bool
Connection_N< N >::is_empty () const
{
  for ( const auto & list : _lists ) {
    if ( !list.is_empty () ) {
      return false;
    }
  }
  return true;
}

template <>
inline bool
Connection_N< 1 >::is_empty () const
{
  return _lists[ 0 ].is_empty ();
}

} // namespace sev::event::queue

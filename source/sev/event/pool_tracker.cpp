/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "pool_tracker.hpp"
#include <sev/assert.hpp>
#include <sev/event/event.hpp>
#include <sev/event/pool_base.hpp>
#include <algorithm>

namespace sev::event
{

Pool_Tracker::Pool_Tracker () = default;

Pool_Tracker::~Pool_Tracker ()
{
  DEBUG_ASSERT ( _pool_register.empty () );
}

bool
Pool_Tracker::all_home () const
{
  for ( const auto * pool : _pool_register ) {
    if ( !pool->all_home () ) {
      return false;
    }
  }
  return true;
}

bool
Pool_Tracker::all_empty () const
{
  for ( const auto * pool : _pool_register ) {
    if ( !pool->is_empty () ) {
      return false;
    }
  }
  return true;
}

bool
Pool_Tracker::any_empty () const
{
  for ( const auto * pool : _pool_register ) {
    if ( pool->is_empty () ) {
      return true;
    }
  }
  return false;
}

bool
Pool_Tracker::all_not_empty () const
{
  for ( const auto * pool : _pool_register ) {
    if ( pool->is_empty () ) {
      return false;
    }
  }
  return true;
}

bool
Pool_Tracker::any_not_empty () const
{
  for ( const auto * pool : _pool_register ) {
    if ( !pool->is_empty () ) {
      return true;
    }
  }
  return false;
}

std::size_t
Pool_Tracker::size_outside () const
{
  std::size_t res = 0;
  for ( const auto * pool : _pool_register ) {
    res += pool->size_outside ();
  }
  return res;
}

void
Pool_Tracker::clear_all_instances () const
{
  for ( auto * pool : _pool_register ) {
    pool->clear ();
  }
}

void
Pool_Tracker::set_all_capacities ( std::size_t capacity_n ) const
{
  for ( auto * pool : _pool_register ) {
    pool->set_capacity ( capacity_n );
  }
}

void
Pool_Tracker::clear_all_capacities () const
{
  for ( auto * pool : _pool_register ) {
    pool->clear_capacity ();
  }
}

bool
Pool_Tracker::owned ( Event * event_n ) const
{
  for ( auto * pool : _pool_register ) {
    if ( event_n->pool () == pool ) {
      return true;
    }
  }
  return false;
}

void
Pool_Tracker::pool_register ( Pool_Base * pool_n )
{
  // Detect doubles in debug mode
  _pool_register.emplace_back ( pool_n );
}

void
Pool_Tracker::pool_unregister ( Pool_Base * pool_n )
{
  auto it =
      std::find ( _pool_register.begin (), _pool_register.end (), pool_n );
  if ( it != _pool_register.end () ) {
    _pool_register.erase ( it );
  }
}

} // namespace sev::event

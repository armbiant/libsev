/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <tuple>

namespace sev::event
{

/// @brief Event pool tracker with pools tuple and interface
///
template < typename... Args >
class Tracker_Pools
{
  public:
  // -- Construction

  Tracker_Pools ()
  : _pools ( ( static_cast< void > ( sizeof ( Args ) ), _tracker )... )
  {
  }

  // -- Tracker

  Pool_Tracker &
  tracker ()
  {
    return _tracker;
  }

  const Pool_Tracker &
  tracker () const
  {
    return _tracker;
  }

  // -- Event type pool accessors

  template < typename T >
  auto &
  pool ()
  {
    return std::get< sev::event::Pool< T > > ( _pools );
  }

  template < typename T >
  const auto &
  pool () const
  {
    return std::get< const sev::event::Pool< T > > ( _pools );
  }

  // -- Index pool accessors

  template < std::size_t N >
  auto &
  pool_n ()
  {
    return std::get< N > ( _pools );
  }

  template < std::size_t N >
  const auto &
  pool_n () const
  {
    return std::get< N > ( _pools );
  }

  private:
  // -- Pools
  Pool_Tracker _tracker;
  std::tuple< sev::event::Pool< Args >... > _pools;
};

} // namespace sev::event

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/flags.hpp>
#include <cstdint>
#include <functional>

namespace sev::event
{

/// @brief Abstract bit accumulator class
///
template < typename IT >
class Bit_Accu
{
  public:
  // -- Types

  using Int_Type = IT;
  using Flags = sev::mem::Flags< IT >;
  static constexpr Int_Type F_None = Flags::F_None;
  static constexpr Int_Type F_All = Flags::F_All;

  using Function_Type = std::function< void () >;

  // -- Construction

  Bit_Accu ();

  Bit_Accu ( Int_Type flags_n );

  Bit_Accu ( Function_Type callback_n );

  Bit_Accu ( Int_Type flags_n,
             Function_Type callback_n,
             bool call_on_demand_n = true );

  ~Bit_Accu ();

  // -- Accessor

  Int_Type &
  flags ()
  {
    return _flags.flags ();
  }

  const Int_Type &
  flags () const
  {
    return _flags.flags ();
  }

  // -- Fill

  void
  clear ()
  {
    _flags.clear ();
  }

  // -- Test

  /// @return True if all flags are low
  bool
  is_empty () const
  {
    return _flags.is_empty ();
  }

  /// @return True if any of the given flags is set
  bool
  test_any ( Int_Type flags_n ) const
  {
    return _flags.test_any ( flags_n );
  }

  /// @return True if all of the given flags are set
  bool
  test_all ( Int_Type flags_n ) const
  {
    return _flags.test_all ( flags_n );
  }

  /// @return True if the flag registers are equal
  bool
  matches ( Int_Type flags_n ) const
  {
    return _flags.matches ( flags_n );
  }

  // -- Get

  /// @return The flags.
  const Int_Type &
  get () const
  {
    return _flags.flags ();
  }

  /// @brief Internal bits are cleared and their value before is returned.
  Int_Type
  fetch_and_clear ()
  {
    return _flags.fetch_and_clear ();
  }

  // -- Set

  /// @brief Sets the given flags high with callback() notification on demand.
  ///
  /// Notifies callback() if the bit accumulator was empty before.
  void
  set ( Int_Type flags_n )
  {
    const Int_Type before = _flags.flags ();
    _flags.set ( flags_n );
    if ( ( before == 0 ) && _callback ) {
      _callback ();
    }
  }

  /// @brief Sets the given flags high without callback() notification.
  void
  set_silent ( Int_Type flags_n )
  {
    _flags.set ( flags_n );
  }

  /// @return Sets the given flags low
  void
  unset ( Int_Type flags_n )
  {
    _flags.unset ( flags_n );
  }

  /// @return Sets the given flags high or low.  If bits are set and the
  ///         register was empty, callback() is notified.
  void
  set ( Int_Type flags_n, bool on_n )
  {
    if ( on_n ) {
      set ( flags_n );
    } else {
      unset ( flags_n );
    }
  }

  /// @return Sets the given flags high or low without callback() notification.
  void
  set_silent ( Int_Type flags_n, bool on_n )
  {
    if ( on_n ) {
      set_silent ( flags_n );
    } else {
      unset ( flags_n );
    }
  }

  // -- Test and set

  /// @brief Tests if any of the given flags is high and sets all high
  ///        otherwise.  The callback() is triggered when the register
  ///        was empty.
  /// @return True if any of the given flags were high.
  bool
  test_any_set ( Int_Type flags_n )
  {
    if ( !test_any ( flags_n ) ) {
      set ( flags_n );
      return false;
    }
    return true;
  }

  /// @brief Tests if any of the given flags is high and sets all high
  ///        otherwise.  The callback() is not triggered.
  /// @return True if any of the given flags were high.
  bool
  test_any_set_silent ( Int_Type flags_n )
  {
    if ( !test_any ( flags_n ) ) {
      set_silent ( flags_n );
      return false;
    }
    return true;
  }

  /// @brief Tests if any of the given flags is high and sets all low then.
  /// @return True if any of the given flags were high.
  bool
  test_any_unset ( Int_Type flags_n )
  {
    if ( test_any ( flags_n ) ) {
      unset ( flags_n );
      return true;
    }
    return false;
  }

  // -- Utility

  /// @brief notify the callback if the register isn't empty
  void
  notify_not_empty () const
  {
    if ( !is_empty () && _callback ) {
      _callback ();
    }
  }

  // -- Callback

  /// @brief The callback
  ///
  const Function_Type &
  callback () const
  {
    return _callback;
  }

  /// @brief Sets the callback
  ///
  /// @arg callback_n The callback
  /// @arg call_on_demand_n Selects whether the callback can be called
  ///      immediately by this method
  void
  set_callback ( Function_Type callback_n, bool call_on_demand_n = true );

  void
  clear_callback ();

  private:
  // -- Attributes
  Flags _flags;
  Function_Type _callback;
};

template < typename IT >
Bit_Accu< IT >::Bit_Accu () = default;

template < typename IT >
Bit_Accu< IT >::Bit_Accu ( Int_Type flags_n )
: _flags ( flags_n )
{
}

template < typename IT >
Bit_Accu< IT >::Bit_Accu ( Function_Type callback_n )
: _callback ( std::move ( callback_n ) )
{
}

template < typename IT >
Bit_Accu< IT >::Bit_Accu ( Int_Type flags_n,
                           Function_Type callback_n,
                           bool call_on_demand_n )
: _flags ( flags_n )
, _callback ( std::move ( callback_n ) )
{
  if ( call_on_demand_n ) {
    notify_not_empty ();
  }
}

template < typename IT >
Bit_Accu< IT >::~Bit_Accu () = default;

template < typename IT >
void
Bit_Accu< IT >::set_callback ( Function_Type callback_n, bool call_on_demand_n )
{
  _callback = std::move ( callback_n );
  if ( _callback && !is_empty () && call_on_demand_n ) {
    _callback ();
  }
}

template < typename IT >
void
Bit_Accu< IT >::clear_callback ()
{
  _callback = Function_Type ();
}

// -- Types
using Bit_Accu_Fast8 = Bit_Accu< std::uint_fast8_t >;
using Bit_Accu_Fast32 = Bit_Accu< std::uint_fast32_t >;
using Bit_Accu_Fast64 = Bit_Accu< std::uint_fast64_t >;

} // namespace sev::event

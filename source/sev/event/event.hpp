/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/list/se/item.hpp>
#include <cstdint>

namespace sev::event
{

// -- Forward declaration
class Pool_Base;

/// @brief Event base class with type id and linked list support
///
class Event : public sev::mem::list::se::Item
{
  public:
  friend class sev::event::Pool_Base;

  // -- Types

  using Type_Int = std::uint_fast32_t;

  // -- Construction

  /// @brief Sets the event type
  Event ( Type_Int type_n = 0 )
  : _type ( type_n )
  {
  }

  ~Event () = default;

  // -- Setup

  /// @brief Reset default method (does nothing)
  void
  reset ()
  {
  }

  // -- Accessors

  /// @brief The event type
  const Type_Int &
  type () const
  {
    return _type;
  }

  // -- Owning pool

  /// @brief The event pool that owns this event
  Pool_Base *
  pool () const
  {
    return _pool;
  }

  private:
  // -- Attributes
  Type_Int _type = 0;
  Pool_Base * _pool = nullptr;
};

} // namespace sev::event

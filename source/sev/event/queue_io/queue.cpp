/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/queue_io/link.hpp>
#include <sev/event/queue_io/queue.hpp>

namespace sev::event::queue_io
{

// -- Types
using Mutex_Guard = std::lock_guard< std::mutex >;
using Spinlock_Guard = std::lock_guard< sev::thread::Spinlock >;

// -- Queue_N

template < std::size_t N >
Queue_N< N >::Queue_N ()
{
  for ( auto & item : _push_notifier ) {
    item = [] () {};
  }
}

template < std::size_t N >
Queue_N< N >::~Queue_N ()
{
}

template < std::size_t N >
Link_N< N >
Queue_N< N >::link_a ()
{
  return Link_N< N > ( this,
                       &Queue_N< N >::set_push_notifier< 0 >,
                       &Queue_N< N >::feed_ro< 0 >,
                       &Queue_N< N >::feed_wo< 0 >,
                       &Queue_N< N >::feed_rw< 0 > );
}

template < std::size_t N >
Link_N< N >
Queue_N< N >::link_b ()
{
  return Link_N< N > ( this,
                       &Queue_N< N >::set_push_notifier< 1 >,
                       &Queue_N< N >::feed_ro< 1 >,
                       &Queue_N< N >::feed_wo< 1 >,
                       &Queue_N< N >::feed_rw< 1 > );
}

template < std::size_t N >
bool
Queue_N< N >::lists_empty_inline ( Lists & lists_n )
{
  for ( const auto & list : lists_n ) {
    if ( !list.is_empty () ) {
      return false;
    }
  }
  return true;
}

template < std::size_t N >
template < std::size_t INDEX >
bool
Queue_N< N >::lists_empty ()
{
  Mutex_Guard lock ( _lists_mutex );
  return lists_empty_inline ( _lists[ INDEX ] );
}

template < std::size_t N >
template < std::size_t INDEX >
void
Queue_N< N >::set_push_notifier ( Queue_N< N > & queue_n,
                                  std::function< void () > const & notifier_n,
                                  bool call_on_demand_n )
{
  Spinlock_Guard lock ( queue_n._push_notifier_mutex[ INDEX ] );
  queue_n._push_notifier[ INDEX ] = notifier_n;
  if ( call_on_demand_n && !queue_n.lists_empty< other_index ( INDEX ) > () ) {
    queue_n._push_notifier[ INDEX ]();
  }
}

template < std::size_t N >
template < std::size_t INDEX >
void
Queue_N< N >::feed_ro ( Queue_N< N > & queue_n, Lists & lists_n )
{
  {
    using Iter = typename Lists::iterator;
    Mutex_Guard lock ( queue_n._lists_mutex );

    auto & local_lists = queue_n._lists[ other_index ( INDEX ) ];
    Iter sic = local_lists.begin ();
    Iter const sie = local_lists.end ();
    Iter dic = lists_n.begin ();
    for ( ; sic != sie; ++sic, ++dic ) {
      dic->splice_back ( *sic );
    }
  }
}

template < std::size_t N >
template < std::size_t INDEX >
void
Queue_N< N >::feed_wo ( Queue_N< N > & queue_n, Lists & lists_n )
{
  bool src_not_empty = false;
  bool dst_not_empty = false;
  {
    using Iter = typename Lists::iterator;
    Mutex_Guard lock ( queue_n._lists_mutex );

    auto & local_lists = queue_n._lists[ INDEX ];
    Iter dic = local_lists.begin ();
    Iter const die = local_lists.end ();
    Iter sic = lists_n.begin ();
    for ( ; dic != die; ++dic, ++sic ) {
      dst_not_empty = ( dst_not_empty || !dic->is_empty () );
      if ( sic->is_empty () ) {
        continue;
      }
      src_not_empty = true;
      dic->splice_back_not_empty ( *sic );
    }
  }

  if ( src_not_empty && !dst_not_empty ) {
    // Link b listens to pushes on list a
    constexpr auto OINDEX = other_index ( INDEX );
    Spinlock_Guard lock ( queue_n._push_notifier_mutex[ OINDEX ] );
    queue_n._push_notifier[ OINDEX ]();
  }
}

template < std::size_t N >
template < std::size_t INDEX >
void
Queue_N< N >::feed_rw ( Queue_N< N > & queue_n, Lists & in_n, Lists & out_n )
{
  constexpr auto OINDEX = other_index ( INDEX );
  bool src_not_empty ( false );
  {
    using Iter = typename Lists::iterator;
    Mutex_Guard lock ( queue_n._lists_mutex );
    {
      auto & out_lists = queue_n._lists[ INDEX ];
      Iter odc = out_lists.begin ();
      Iter const ode = out_lists.end ();
      Iter osc = out_n.begin ();
      for ( ; odc != ode; ++odc, ++osc ) {
        if ( osc->is_empty () ) {
          continue;
        }
        src_not_empty = true;
        odc->splice_back_not_empty ( *osc );
      }
    }
    {
      auto & in_lists = queue_n._lists[ OINDEX ];
      Iter isc = in_lists.begin ();
      Iter const ise = in_lists.end ();
      Iter idc = in_n.begin ();
      for ( ; isc != ise; ++isc, ++idc ) {
        idc->splice_back ( *isc );
      }
    }
  }
  if ( src_not_empty ) {
    // Link b listens to pushes on list a
    Spinlock_Guard lock ( queue_n._push_notifier_mutex[ OINDEX ] );
    queue_n._push_notifier[ OINDEX ]();
  }
}

// -- Instantiation
template class Queue_N< 1 >;
template class Queue_N< 2 >;

} // namespace sev::event::queue_io

/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/list.hpp>
#include <sev/event/pool_base.hpp>
#include <sev/event/queue_io/link.hpp>
#include <array>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <type_traits>

// -- Forward declarationt
namespace sev::event
{
class Event;
} // namespace sev::event
namespace sev::event::queue_io
{
template < std::size_t N >
class Reference_N;
} // namespace sev::event::queue_io

namespace sev::event::queue_io
{

/// @brief Base class for a connection to a queue
///
class Connection_Base
{
  public:
  // -- Types

  class Channel_In
  {
    private:
    // -- Construction
    friend class Connection_Base;

    constexpr Channel_In ( List & list_in_n,
                           List & list_out_n,
                           Connection_Base & con_n )
    : _in ( list_in_n )
    , _out ( list_out_n )
    , _con ( con_n )
    {
    }

    public:
    // -- State

    /// @return true if there are events available
    bool
    available () const
    {
      return !_in.is_empty ();
    }

    // -- Front event

    Event *
    front () const
    {
      return _in.front ();
    }

    /// @brief Take one elements from the incoming list and move it
    ///        to the outgoing list.  Requires available().
    void
    accept_one_not_empty ()
    {
      _out.push_back ( _in.pop_front_not_empty () );
      _con.pushed ();
    }

    /// @brief Take one elements from the incoming list and move it
    ///        to the outgoing list.
    void
    accept_one ()
    {
      if ( available () ) {
        accept_one_not_empty ();
      }
    }

    /// @return true if an event was processed
    template < class Function >
    bool
    process_one ( Function && f_n )
    {
      if ( _in.is_empty () ) {
        return false;
      }
      f_n ( _in.front () );
      accept_one_not_empty ();
      return true;
    }

    // -- All events

    /// @brief Take all elements from the incoming list and move them
    ///        to the outgoing list.  Requires available().
    void
    accept_all_not_empty ()
    {
      _out.splice_back_not_empty ( _in );
      _con.pushed ();
    }

    /// @brief Take all elements from the incoming list and move them
    ///        to the outgoing list.
    void
    accept_all ()
    {
      if ( available () ) {
        accept_all_not_empty ();
      }
    }

    /// @return true if at least one event was processed
    template < class Function >
    bool
    process_all ( Function && f_n )
    {
      if ( _in.is_empty () ) {
        return false;
      }
      for ( Event & event : _in ) {
        f_n ( event );
      }
      accept_all_not_empty ();
      return true;
    }

    /// @return true if at least one event was processed
    template < class ET, class Function, class... Args >
    bool
    process_all_casted ( Function && f_n, Args &&... args )
    {
      if ( _in.is_empty () ) {
        return false;
      }
      for ( Event & event : _in ) {
        f_n ( static_cast< ET & > ( event ) );
      }
      accept_all_not_empty ();
      return true;
    }

    private:
    // -- Utility

    private:
    List & _in;
    List & _out;
    Connection_Base & _con;
  };

  class Channel_Out
  {
    private:
    // -- Construction
    friend class Connection_Base;

    constexpr Channel_Out ( List & list_in_n,
                            List & list_out_n,
                            Connection_Base & con_n )
    : _in ( list_in_n )
    , _out ( list_out_n )
    , _con ( con_n )
    {
    }

    public:
    // -- Outgoing

    void
    push ( Event * event_n )
    {
      _out.push_back ( event_n );
      _con.pushed ();
    }

    /// @brief Are there outgoing events?
    bool
    outgoing () const
    {
      return !_out.is_empty ();
    }

    // -- Returned event release

    /// @brief Are there outgoing events?
    bool
    returning () const
    {
      return !_in.is_empty ();
    }

    /// @return true if at least one event was returned
    bool
    release_all ()
    {
      if ( _in.is_empty () ) {
        return false;
      }
      while ( !_in.is_empty () ) {
        auto * event = _in.pop_front_not_empty ();
        event->pool ()->casted_release_virtual ( event );
      }
      return true;
    }

    /// @return true if at least one event was returned
    template < class Function >
    bool
    release_all ( Function && f_n )
    {
      if ( _in.is_empty () ) {
        return false;
      }
      while ( !_in.is_empty () ) {
        f_n ( _in.pop_front_not_empty () );
      }
      return true;
    }

    /// @return true if at least one event was returned
    template < class ET, class Function >
    bool
    release_all_casted ( Function && f_n )
    {
      if ( _in.is_empty () ) {
        return false;
      }
      while ( !_in.is_empty () ) {
        f_n ( static_cast< ET * > ( _in.pop_front_not_empty () ) );
      }
      return true;
    }

    /// @return true if at least one event was returned
    bool
    release_all_with_reset ()
    {
      if ( _in.is_empty () ) {
        return false;
      }
      while ( !_in.is_empty () ) {
        auto * event = _in.pop_front_not_empty ();
        event->pool ()->casted_reset_release_virtual ( event );
      }
      return true;
    }

    private:
    List & _in;
    List & _out;
    Connection_Base & _con;
  };

  public:
  // -- Construction

  Connection_Base ();

  ~Connection_Base ();

  // -- Accessors

  /// @brief Gets notified on the first push to a local list()
  std::function< void () > const &
  push_notifier () const
  {
    return _push_notifier;
  }

  /// @brief Gets notified on the first remote push to the shared queue
  std::function< void () > const &
  incoming_notifier () const
  {
    return _incoming_notifier;
  }

  protected:
  // -- Utility

  Channel_In
  make_in ( List & in_n, List & out_n )
  {
    return Channel_In ( in_n, out_n, *this );
  }

  Channel_Out
  make_out ( List & in_n, List & out_n )
  {
    return Channel_Out ( in_n, out_n, *this );
  }

  /// @brief Notifies the callback if this was the first push
  void
  pushed ()
  {
    if ( !_pushed ) {
      _pushed = true;
      _push_notifier ();
    }
  }

  protected:
  bool _pushed = false;
  std::function< void () > _push_notifier;
  std::function< void () > _incoming_notifier;
};

/// @brief Connection to a queue with local event storage list
///
template < std::size_t N >
class Connection_N : public Connection_Base
{
  public:
  // -- Construction

  Connection_N ();

  Connection_N ( const Connection_N< N > & connection_n ) = delete;

  Connection_N ( Connection_N< N > && connection_n ) = delete;

  ~Connection_N ();

  // -- Assignment operators

  Connection_N< N > &
  operator= ( const Connection_N< N > & connection_n ) = delete;

  Connection_N< N > &
  operator= ( Connection_N< N > && connection_n ) = delete;

  // -- Queue link

  /// @brief Connects to a queue
  ///
  /// @arg call_on_demand_n If true incoming_notifier()
  ///      is called if the queue is not empty.
  void
  connect ( Link_N< N > const & link_n, bool call_on_demand_n = true );

  void
  disconnect ();

  bool
  is_connected () const
  {
    return _link.is_valid ();
  }

  const Link_N< N > &
  link () const
  {
    return _link;
  }

  const Reference_N< N > &
  queue () const
  {
    return _link.queue ();
  }

  // -- Lists in / out

  Channel_In
  in ( std::size_t index_n = 0 )
  {
    return make_in ( _lists_in[ index_n ], _lists_out[ ( N / 2 ) + index_n ] );
  }

  Channel_Out
  out ( std::size_t index_n = 0 )
  {
    return make_out ( _lists_in[ ( N / 2 ) + index_n ], _lists_out[ index_n ] );
  }

  void
  push ( std::size_t index_n, Event * event_n )
  {
    _lists_out[ index_n ].push_back ( event_n );
    pushed ();
  }

  void
  push ( Event * event_n )
  {
    push ( 0, event_n );
  }

  // -- Lists common

  bool
  all_empty () const;

  // -- Event push notifier

  void
  set_push_notifier ( std::function< void () > const & notifier_n,
                      bool call_on_demand_n = true );

  void
  clear_push_notifier ();

  // -- Incoming event notifier

  void
  set_incoming_notifier ( std::function< void () > const & notifier_n,
                          bool call_on_demand_n = true );

  void
  clear_incoming_notifier ();

  // -- Queue feeding

  /// @brief Non blocking queue feeding
  ///
  void
  feed_queue ();

  /// @brief Non blocking queue feeding
  ///
  void
  feed_queue ( std::uint_fast32_t & bits_n,
               std::uint_fast32_t incoming_bits_n,
               std::uint_fast32_t outgoing_bits_n );

  private:
  std::array< List, N > _lists_in;
  std::array< List, N > _lists_out;
  Link_N< N > _link;
};

// -- Types
using Connection = Connection_N< 1 >;
using Connection_1 = Connection_N< 1 >;
using Connection_2 = Connection_N< 2 >;

} // namespace sev::event::queue_io

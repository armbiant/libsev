/// libsev: C++ library for threads, math strings and trees.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/timer_queue/client.hpp>
#include <sev/event/timer_queue/queue.hpp>

namespace sev::event::timer_queue
{

template < class CLK >
Client< CLK >::Client ()
: _callback ( []() {} )
, _run_state ( Client_Run_State::IDLE )
{
}

template < class CLK >
Client< CLK >::Client ( Queue * queue_n )
: _callback ( []() {} )
, _run_state ( Client_Run_State::IDLE )
{
  connect ( queue_n );
}

template < class CLK >
Client< CLK >::Client ( Queue * queue_n, std::function< void() > callback_n )
: _callback ( std::move ( callback_n ) )
, _run_state ( Client_Run_State::IDLE )
{
  connect ( queue_n );
}

template < class CLK >
Client< CLK >::~Client ()
{
  disconnect ();
}

template < class CLK >
bool
Client< CLK >::connect ( Queue * queue_n )
{
  if ( _queue != queue_n ) {
    disconnect ();

    if ( queue_n != nullptr ) {
      queue_n->client_register ();
      _queue = queue_n;
    }
  }
  return is_connected ();
}

template < class CLK >
void
Client< CLK >::disconnect ()
{
  if ( is_connected () ) {
    stop ();
    _queue->client_unregister ();
    _queue = nullptr;
  }
}

template < class CLK >
void
Client< CLK >::set_callback ( std::function< void() > callback_n )
{
  stop ();
  _callback = std::move ( callback_n );
}

template < class CLK >
void
Client< CLK >::clear_callback ()
{
  set_callback ( []() {} );
}

template < class CLK >
void
Client< CLK >::set_point ( const Time_Point & time_point_n )
{
  stop ();
  _point = time_point_n;
}

template < class CLK >
void
Client< CLK >::adjust_point ( const Duration & duration_n )
{
  stop ();
  _point += duration_n;
}

template < class CLK >
void
Client< CLK >::set_duration ( const Duration & duration_n )
{
  stop ();
  _duration = duration_n;
}

template < class CLK >
void
Client< CLK >::start_single_point ()
{
  if ( is_connected () ) {
    stop ();
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_point ( const Time_Point & time_point_n )
{
  if ( is_connected () ) {
    stop ();
    _point = time_point_n;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_point_now ()
{
  if ( is_connected () ) {
    stop ();
    _point = _queue->now ();
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period ( const Time_Point & time_point_n )
{
  if ( is_connected () ) {
    stop ();
    _point = time_point_n;
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_next ()
{
  if ( is_connected () ) {
    stop ();
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_next ( const Duration & duration_n )
{
  if ( is_connected () ) {
    stop ();
    _duration = duration_n;
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_now ()
{
  if ( is_connected () ) {
    stop ();
    _point = _queue->now ();
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_single_period_now ( const Duration & duration_n )
{
  if ( is_connected () ) {
    stop ();
    _duration = duration_n;
    _point = _queue->now ();
    _point += _duration;
    _run_state = Client_Run_State::SINGLE;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_at_point ()
{
  if ( is_connected () ) {
    stop ();
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_at ( const Time_Point & time_point_n )
{
  if ( is_connected () ) {
    stop ();
    _point = time_point_n;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_next ()
{
  if ( is_connected () ) {
    stop ();
    _point += _duration;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_now ( const Time_Point & time_now_n )
{
  if ( is_connected () ) {
    stop ();
    _point = time_now_n;
    _point += _duration;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::start_periodical_now ()
{
  if ( is_connected () ) {
    stop ();
    _point = _queue->now ();
    _point += _duration;
    _run_state = Client_Run_State::PERIOD;
    _queue->client_start ( this );
  }
}

template < class CLK >
void
Client< CLK >::stop ()
{
  if ( is_running () ) {
    _run_state = Client_Run_State::IDLE;
    _queue->client_stop ( this );
  }
}

} // namespace sev::event::timer_queue

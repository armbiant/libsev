/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/timer_queue/client.hpp>
#include <chrono>

namespace sev::event::timer_queue::chrono
{

// -- Types
typedef Client< std::chrono::steady_clock > Client_Steady;
typedef Client< std::chrono::system_clock > Client_System;
typedef Client< std::chrono::high_resolution_clock > Client_High_Res;

} // namespace sev::event::timer_queue::chrono

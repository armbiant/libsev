/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/timer_queue/queue.hpp>
#include <sev/event/timer_queue/queue.tcc>
#include <chrono>

namespace sev::event::timer_queue
{

// -- Instantiation
template class Queue< std::chrono::steady_clock >;
template class Queue< std::chrono::system_clock >;

} // namespace sev::event::timer_queue

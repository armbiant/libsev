# About

libsev is a C++ utility library for math, strings, threads and events.


## Copying

libsev is distributed under the terms in the LICENSE-libsev.txt text file that
comes with this package.


## Installation

libsev uses the CMake build system.

For a system wide installation call:

```
mkdir build-libsev
cd build-libsev
cmake <libsev_git_dir>
make -j5
sudo make install
```
